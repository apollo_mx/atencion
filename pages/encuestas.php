<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Encuestas</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="clearfix">&nbsp;</div><div class="clearfix">&nbsp;</div>
   <!--                          <h1 class="page-header" style="color: #AC182D;">Gestionar Catálogos</h1> -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">

                        <div class="col-lg-9">
                            <!-- <form id="cform1"> -->
                                <form id="cform1">
                                    <table id="table_mails" class="table table-striped table-bordered" style="width:100%"></table>
                                </form>
                            <!-- </form> -->
                               

<!--                                 <form id="cform1">
                                    <table  class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Mail</th>
                                            <th>Contrato</th>                   
                                        </tr>
                                        </thead>
                                        <tbody id="list-envios">
                                        </tbody>
                                    </table>
                                </form> -->

                        </div>
                        <div class="col-lg-3">
                            <div class="form-check">
                                <div class="form-check checkbox-teal mb-2">
                                  <input type="checkbox" class="form-check-input" id="checkAll">
                                  <label class="form-check-label" for="checkAll">Seleccionar Todos</label>
                                </div>
                            </div>
                            <div class="col-md-3" style="text-align: right;">
                                <select class="custom-select custom-select-lg mb-3" id="an">
                                  <option value="0" selected>Año</option>
                                  <option value="2016-01-15"> 2016</option>
                                  <option value="2017-01-15"> 2017</option>
                                  <option value="2018-01-15"> 2018</option>
                                  <option value="2019-01-15"> 2019</option>                                 
                                  <option value="2020-01-15"> 2020</option>
                                  <option value="2021-01-15"> 2021</option>
                                  <option value="2022-01-15"> 2022</option>
                                  <option value="2023-01-15"> 2023</option>                                 
                                  <option value="2024-01-15"> 2024</option>
                                  <option value="2025-01-15"> 2025</option>                              
<!--                                   <option value="2022-01-15"> 2022</option>
                                  <option value="2023-01-15"> 2023</option> -->                                                                                                                                                                          
                                </select>                               
                            </div>
                            <div class="col-md-3" style="text-align: right;">
                                <select class="custom-select custom-select-lg mb-3" id="trimestre">
                                  <option value="0" selected>Trimestre</option>
                                  <option value="1T"> 1T</option>
                                  <option value="2T"> 2T</option>
                                  <option value="3T"> 3T</option>
                                  <option value="4T"> 4T</option>                                  
                                </select>                               
                            </div><br>
                            <div class="col-md-3" style="text-align: right;">
                                <select class="custom-select custom-select-lg mb-3" id="estatusEnc">
                                  <option value="NP" selected> Estatus</option>
                                  <option value="NA"> NA</option>
                                </select>                               
                            </div><br>                            

                              
                            <div class="clearfix"></div><br>
                            <div class="col-md-6"><div class="float-right text-right"><button type="button" class="btn btn-primary" onclick="enviarEnc();">Enviar Encuesta</button></div></div>

                            <div class="clearfix">&nbsp;</div>
                            <div id="estatusmail"></div>

                        </div>
                        

                        

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->


        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>


    <script>
    

    </script>

        <script type="text/javascript">
        	$(document).ready(function(){
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });

        	});
            
            $("#checkAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            function loadClientes(){    //Cargar catalogo de clientes
                 
                var table = $('#table_mails').dataTable({
                   dom: 'Bfrtip',
                    bProcessing: true,
                    "scrollY": 440,
                    destroy: true,
                    "scrollX": true,
                    "autoWidth": true,
                     retrieve: true,
                    // scrollCollapse: true,
                    "paging": false,
                    // "ordering": false,
                    "bInfo": false,
                    "language": {
                        "search": "Buscar:"
                    },
                    ajax: {
                        "url":"./_php/listarcontrEncuesta.php"
                    },
                    // columnDefs: [
                    //     { responsivePriority: 1, targets: 1 },
                    //     { responsivePriority: 10001, targets: 1 },                                            
                    // ],
                    columns: [
                        // {
                        //     data: 'opcion',
                        //     defaultContent: '',
                        //     className: 'select-checkbox',
                        //     orderable: false
                        // },
                       { data: 'opcion',title: 'Enviar' },
                        { data: 'num_contrato',title: 'Contrato' },
                        { data: 'correo',title: 'Correo' },
                        { data: 'nom_representante',title: 'Representante' },                           
                        { data: 'nom_division',title: 'Division' },                    
                        // // { data: 'desContrato',title: 'Descripción' },                                                
                        // { data: 'nom_ubicacion',title: 'Ubicacion' },                                                      
                      ],
                    order: [1, 'asc'],
                    select: {
                        style: 'os',
                        selector: 'td:first-child'
                    },
                    buttons: [
                                {
                                    text: "Add",
                                    action: function (e, dt, node, config) {
                                        AddNewRIKDialog(e);
                                    }
                                },
                                {
                                    text: "Edit",
                                    action: function () { alert("Edit RIK"); }
                                },
                                {
                                    text: "Remove",
                                    action: function () { alert("remove RIK"); }
                                }
                            ],          
                    scrollY:        '50vh',
                    scrollCollapse: true,
                    paging:         false
                });   
                table.DataTable().ajax.reload();
            }
         
            loadClientes();

                function enviarEnc(){
                     //console.log($(this));
                    var selected = [];// initialize array

                    
                    $('#cform1 input[type=checkbox]').each(function(key,val) {
                        if ($(this).is(":checked")) {
                            listado = {"idContrato":$(this).attr('id'),"correo":$(this).val()};                       
                            selected.push(listado);
                        }
                    });

                    var trimestre = $("#trimestre").val();
                    var an = $("#an").val();
                    var estatusEnc = $("#estatusEnc").val();

                    if (selected.length ==0 || trimestre ==0 || an == 0) {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: "<label>Selecciona un trimestre y al menos un registro para enviar su encuesta</label>",
                            callback: function(){
                            }
                        });   
                        //  console.log("No haz seleeccionado nignun dato");
                    }else{
                        //console.log(trimestre);
                       // console.log(an);
                       // console.log(estatusEnc);
                       // console.log(selected);
                       
                        $.ajax({
                            type: 'POST',
                            url: './_php/crearEncabezadoEnc.php',
                            dataType: 'json',
                            data: {selected:selected, trimestre:trimestre,an:an,estatusEnc:estatusEnc},
                             complete: function (xhr, textStatus) {
                                //called when complete
                            },
                            success: function(data) {
                                //console.log(data);
                                
                                var enviado = "";
                                // var error = "";
                                //enviado +='<span>'+value+'</span><br>';
                                
                                $.each(data['data'], function( key, value ) {
                                    enviado +='<span>'+value+'</span><br>';
                                });
                                
                                // $.each(data['error'], function( key, value ) {
                                //     error += '<span>'+value+'</span><br>';
                                // });

                                $("#estatusmail").html('<div class="alert alert-success">'+enviado+'</div>');
                                //<div class="alert alert-danger"><h5>Correos fallidos</h5>'+error+'</div>
                                  
                            }
                        });//fin ajax  


                    }                    
                };


        </script>

    </body>
</html>