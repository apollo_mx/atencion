<?php 

error_reporting(0);
//https://www.youtube.com/watch?v=JVnDHYS_3Ag

    // session_start();  
    // if (empty($_SESSION['user'])) {
    //    header("location: ./logout/");
    // } 
    include "./mcript.php";
    
    $id_respuesta = $desencriptar($_GET['seg']);
    $codEmpleadoResponde = $desencriptar($_GET['codCol']);
    $folio_seguimiento = $desencriptar($_GET['folSeg']);    
    

    require_once('./_php/db.class.php');
    $db = DataBase::connect();    


    //Seleccionar todos los datos relacionadps a la respuesta de un cuestionario y un contrato
    $db->setQuery("select r.id as id_respuesta, c.num_contrato,c.descripcion,cd.nombre as division,r.anio_trimestre,r.trimestre, r.estatus, crep.nombre,crep.correo,crep.puesto from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_representantes crep
on c.id_representante = crep.id 
inner join cat_division cd 
on cd.id = c.id_division where r.id =".$id_respuesta);
    $rowe = $db->loadObject();


    $date = date_create($rowe->anio_trimestre); 
    $anFormat = date_format($date,"Y");

    //Seleccionar las respuestas de peticiones que se realizaron
    $db->setQuery("SELECT respuesta3, respuesta6, respuesta9,respuesta12,respuesta15 FROM respuestas where id =".$id_respuesta); 
	$listPeticiones = $db->loadObject();

    //Obtiene el numero de seccion del seguimiento a utilizar
    $db->setQuery("select num_pregunta from detalle_seguimiento where folio_seguimiento = ".$folio_seguimiento." and cod_empleado =".$codEmpleadoResponde); 
    $listPet = $db->loadObjectList();

    

    foreach ($listPet as $key => $value) {
        if ($value->num_pregunta == 3) {
            $respuesta3 = "respuesta3";
        }
        if ($value->num_pregunta == 6) {
            $respuesta6 = "respuesta6";
        }        
        if ($value->num_pregunta == 9) {
            $respuesta9 = "respuesta9";
        }        
        if ($value->num_pregunta == 12) {
            $respuesta12 = "respuesta12";
        }                
        if ($value->num_pregunta == 15) {
            $respuesta15 = "respuesta15";
        }                
    }

    $listPeticiones2 = array("respuesta3" => $respuesta3,"respuesta6" =>$respuesta6,"respuesta9"=>$respuesta9,"respuesta12"=>$respuesta12,"respuesta15" =>$respuesta15);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Seguimiento</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->
        <link href="../js/dist/css/jquery.dm-uploader.min.css" rel="stylesheet">
        <link href="../css/styles.css" rel="stylesheet">


        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="../css/jquery.chosen.css" rel="stylesheet" type="text/css">
  
        <style type="text/css">
           .titulo{
            color: #fff;
            background-color: #cf203d;
            text-align: center;
            font-size: 20px !important;
           }
           label{
            font-size: 13px;
            color: #969696;
           }
           .subtit{
            font-weight: 700;
            color: #002e5b;
            font-size: 14px;
           }
            .subtit1 {
                font-weight: 700;
                color: #000;
/*                font-size: 14px;*/
                text-align: right;
                display: block;
            }           
           .checkbox, .radio {
                margin-top: 5px;
                margin-bottom: 5px;
                padding-left: 20px;
            }

            blockquote {
        
                border-left-color: #f0ad4e;
            }            
            .recived_msg{
                background: #ebebeb none repeat scroll 0 0;
                border-radius: 3px;
                color: #646464;
                font-size: 14px;
                margin: 0;
                padding: 5px 10px 5px 12px;
                width: 100%;
                display: block;
            }
            .time_date{
                color: #747474;
                display: block;
                font-size: 10px;
                margin: 8px 0 0;
                text-align: right;                
            }
            .incoming_msg_img{
                width: 35px;
            }
            .autor{
                font-size: 12px;
                line-height: 1.5;
                color: var(--color-text-primary);
                font-weight: 600;
                white-space: nowrap;                
            }

       </style>
    </head>
    <body class="bg-light">

        
	    <main role="main">

			<div class="container">
            <div id="loading" class="col-md-6" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
				<br><br>

                <div class="col-md-10 col-md-offset-1">
                    

                        <div class="col-md-12">
	                        <table class="table table-striped table-bordered">
	                        	<tbody>
	                        		<tr>
	                        			<td>
                                            <div class="col-md-6" style="font-weight: 700; color: #002e5b; font-size: 14px; text-align: left;">
                                                <img src="/atencion/img/logo.png" style="width: 160px; height: px;" alt="APOLLO">
                                            </div>
                                            <div class="col-md-6" style="font-weight: 700; color: #002e5b; font-size: 14px; text-align: right;">
                                            <?php 
                                                //Verificar Si ya se inició un seguimiento
                                                $db->setQuery("SELECT * FROM seguimiento where id_respuesta =".$id_respuesta); 
                                                $seguimiento_general = $db->loadObject();
                                            ?>
                                            <h1><small>Folio:</small><small id="folioSeguimiento"><?php echo $seguimiento_general->folio; ?></small></h1>

                                          
                                                                                

					                        </div>  
	                        			</td>	                        			
	                        		</tr>
	                        	</tbody>
	                        </table>
						</div>					
						<hr>	                        
                        <div class="col-md-12">
	                        <table class="table table-striped table-bordered">
	                        	<tbody>
	                        		<tr>
	                        			<th>Contrato</th>
	                        			<td><?php echo $rowe->num_contrato; ?></td>
	                        			<th>Representante</th>
	                        			<td><?php echo ($rowe->nombre); ?></td>	  
	                        		</tr>
	                        		<tr>
	                        			<th>Descripcion</th>
	                        			<td><?php echo utf8_encode($rowe->descripcion); ?></td>
	                        			<th>Correo</th>
	                        			<td><?php echo utf8_encode($rowe->correo); ?></td>		                        			                        			
	                        		</tr>
	                        		<tr>
	                        			<th>División</th>
	                        			<td><?php echo utf8_encode($rowe->division); ?></td>	
	                        			<th>Puesto</th>
	                        			<td><?php echo $rowe->puesto; ?></td>		                        			                        			
	                        		</tr>	                        		
	                        		<tr>
	                        			<th>Año y Trimestre</th>
	                        			<td><?php echo $anFormat .",Trimestre". $rowe->trimestre; ?></td>	                        			                        
                                        <th>Estatus</th>
                                        <td>
                                            <?php if ($seguimiento_general == 1) { 
                                                $color_ms ='';
                                                if($seguimiento_general->estatus == "EN PROCESO"){
                                                    $color_ms ='label-info';
                                                }
                                                if($seguimiento_general->estatus == "CANCELADO"){
                                                    $color_ms ='label-danger';
                                                }
                                                if($seguimiento_general->estatus == "CERRADO"){
                                                    $color_ms ='label-success';
                                                }                                                                              
                                                echo '<span class="label '.$color_ms.'">'.$seguimiento_general->estatus.'</span>'; 
                                            } ?>
                                            <?php if ($seguimiento_general < 1) { echo '<span class="label label-default">NUEVO</span>'; } ?>

                                        </td>                                                                       

	                        		</tr>			                        		                        		
	                        	</tbody>
	                        </table>	                        
						</div>
                        <?php 
                            if ($seguimiento_general == 1){
                                
                            }else{
                        ?>
<!--                         se carga cuando aun no hay una asignacion -->
						<div class="col-md-12">   
							<table class="table table-striped table-bordered">
	                        	<thead>
	                        		<tr>
	                        			<th>Sección</th>
	                        			<th style="text-align: center;"><span>Solicitud y/o Petición</span></th>
                                        <th>Asignar a:</th>
                                        <th>Cancelar un Seguimiento</th>
	                        		</tr>
	                        	</thead>
	                        	<tbody>
                                    <?php if ($respuesta3) { ?>                                    
	                        		<tr>
	                        			<td>Servicio</td>
	                        			<td><?php echo $respuesta3; ?></td>
                                        <td>
                                            <div class="form-group">
                                                <label id="empl3">Colaborador</label>                
                                                <select id="clnt3" name="clnt3" data-placeholder="Selecciona un Cliente" multiple class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>

                                            </div>
                                        </td>
                                        <td>
                                            <input type="button" id="canc3" class="btn btn-danger" value="Cancelar">
                                        </td>
	                        		</tr>
	                        		<?php } ?>
                                    <?php if ($respuesta6) { ?>
                                    <tr>
	                        			<td>Equipos e instalaciones</td>
	                        			<td><?php echo $respuesta6; ?></td>
                                        <td>
                                            <div class="form-group">  
                                                <label id="empl6">Colaborador</label>               
                                                <select id="clnt6" name="clnt6" multiple data-placeholder="Selecciona un Cliente" class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                          
                                        </td>
	                        		    <td>
                                            <input type="button" id="canc6" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>	                        	
                                    <?php } ?>
                                    <?php if ($respuesta9) { ?>                                                                          
	                        		<tr>
	                        			<td>Factor humano</td>
	                        			<td><?php echo $respuesta9; ?></td>
                                        <td>
                                            <div class="form-group"> 
                                                <label id="empl9">Colaborador</label>                
                                                <select id="clnt9" name="clnt9" multiple data-placeholder="Selecciona un Cliente" class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                          
                                        </td>
                                        <td>
                                            <input type="button" id="canc9" class="btn btn-danger" value="Cancelar">
                                        </td>
	                        		</tr>                                    
                                    <?php } ?>                                     	                        	
                                    <?php if ($respuesta12) { ?>	                        		
                                    <tr>
	                        			<td>Gestión</td>
	                        			<td><?php echo $respuesta12; ?></td>
                                        <td>
                                            <div class="form-group">
                                                <label id="empl12">Colaborador</label>                
                                                <select id="clnt12" name="clnt12" data-placeholder="Selecciona un Cliente" multiple class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                          
                                        </td>
	                        		    <td>
                                            <input type="button" id="canc12" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>
                                    <?php } ?>                                      
	                        		<?php if ($respuesta15) { ?>
                                    <tr>
	                        			<td>Imagen</td>
	                        			<td><?php echo $respuesta15; ?></td>
                                        <td>
                                            <div class="form-group">
                                                <label id="empl15">Colaborador</label>                 
                                                <select id="clnt15" name="clnt15" data-placeholder="Selecciona un Cliente" multiple class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                             
                                        </td>
	                        		    <td>
                                            <input type="button" id="canc15" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>
                                    <?php } ?>                                     	                        						                        		                        		
	                        	</tbody>
	                        </table>
						</div>

                        
                        <?php } ?>

						
                        <?php if ($seguimiento_general == 1):   //Seccion que se muestra cuando ya está generado un folio
                             $db->setQuery("SELECT * FROM atencion.detalle_seguimiento where folio_seguimiento =".$seguimiento_general->folio); 
                            $seguimiento_detalle = $db->loadObjectlist();
                            //print_r($seguimiento_detalle);                            # code...
                            include './_php/cise_get_UserId.php'; 
                        ?>

                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div style="text-align: center;">
                                        Detalles
                                    </div>
                                </div>
                                <div class="panel-body">
                               
                                <div class="row">
        
                                    <ul class="nav nav-tabs">
                                    <?php function getSeccion($secc){
                                        switch ($secc) {
                                            case 'respuesta3':
                                                return 'Servicio';
                                                break;
                                            case 'respuesta6':
                                                return 'Equipos e instalaciones';
                                                break; 
                                            case 'respuesta9':
                                                return 'Factor humano';
                                                break;
                                            case 'respuesta12':
                                                return 'Gestión';
                                                break;       
                                            case 'respuesta15':
                                                return 'Imagen';
                                                break;
                                            default:
                                                return 'No';
                                                break;
                                        }
                                    } ?>                             
                                    <?php foreach ($listPeticiones2 as $key => $val_peticiones): ?>

                                        <?php if ($val_peticiones): ?>
                                            <li><a data-toggle="pill" href="#<?php echo $key; ?>" onClick="que_preg(<?php echo "'".$key."'"; ?>);"><?php echo getSeccion($key); ?></a></li>
                                        
                                        <?php endif ?>

                                    <?php endforeach ?>
                                  <?php $page = "front"; ?>

                                  </ul>
                                  <div class="tab-content">
                                    <div id="respuesta3" class="tab-pane fade">
                                     <?php include './_php/serv_tab.php'; ?>
                                    </div>
                                    <div id="respuesta6" class="tab-pane fade">
                                      <?php include './_php/equi_e_ins_tab.php'; ?>
                                    </div>
                                    <div id="respuesta9" class="tab-pane fade">
                                      <?php include './_php/fac_hum_tab.php'; ?>
                                    </div>
                                    <div id="respuesta12" class="tab-pane fade">
                                      <?php include './_php/gest_tab.php'; ?>
                                    </div>
                                    <div id="respuesta15" class="tab-pane fade">
                                      <?php include './_php/imag_tab.php'; ?>
                                    </div>                                    
                                  </div>
                                </div>

                                </div>
                            </div>
                        </div>                                  
                        <?php endif ?>
											
                </div>

			</div>	
            <br>
	    </main>
<!-- File item template -->
    <script type="text/html" id="files-template">
      <li class="media">
        <div class="media-body mb-1">
          <p class="mb-2">
            <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
          </p>
          <div class="progress mb-2">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
              role="progressbar"
              style="width: 0%" 
              aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
            </div>
          </div>
          <hr class="mt-1 mb-1" />
        </div>
      </li>
    </script>

    <!-- Debug item template -->
    <script type="text/html" id="debug-template" class="debug-template">
      <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
    </script>
    </body>
<!-- jQuery -->
    <script src="../js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../js/metisMenu.min.js"></script>
    <script src="../js/dataTables/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../js/startmin.js"></script>
    <script src="../js/bootbox.min.js"></script>
    <script src="../js/chosen.jquery.min.js"></script>

    <script src="../js/dist/js/jquery.dm-uploader.min.js"></script>
    <script src="../js/demo-ui.js"></script>
    <script src="../js/demo-config.js"></script>

    <script type="text/javascript">
	    var num_pr = 0;
        //console.log("forma global " + num_pr);

        function que_preg(pregunta){
            num_pr = pregunta;
            //console.log("dentro de funcion click tab" + num_pr);
        }
        $(document).ready(function(){
  
           
            // Estatus 1 asignado
            // Estatus 2 cerrado
            // Estatus 3 cancelado
            

            $('#loading').hide(); //initially hide the loading icon

            $(document).ajaxStart(function(){
                $('#loading').show();
                //console.log('shown');
              });
            $(document).ajaxStop(function(){
                $('#loading').hide();
                //console.log('hidden');
            });


            
    	});
    
    function sendNota(note,num_preg,id_detalle){   //INserta una nota de texto

        var nota = $('#'+note).val();
        var dataNota = {folio: $('#folioSeguimiento').text(), num_preg: num_preg,nota:null, codEmpleado:'<?php echo $codEmpleadoResponde; ?>', id_detalle:id_detalle};
        if ($.trim(nota).length >=10) {
            dataNota.nota = nota; 

            $.ajax({
                type: 'POST',
                url: './_php/insertNota_texto_colaborador.php',
                dataType: 'json',
                data: {dataNota:dataNota},
                 complete: function (xhr, textStatus) {
                    //called when complete
                    //console.log(textStatus);
                },
                success: function(data) {
                    // console.log(data);
                    bootbox.alert({
                        size: "small",
                        title: "Alerta",
                        message: "<label>"+data['data']+"</label>",
                        callback: function(){
                            location.reload(1);
                        }
                    });                                   
                }
            });//fin ajax   

        }else{
            bootbox.alert({
                size: "small",
                title: "Alerta",
                message: "<label>Campo Nota, Requiere mínimo 10 caracteres.</label>",
                callback: function(){
                }
            });    
        }
    }

    function sendFile(rutaImg){   //INserta un archivo
        // console.log("dentro de funcion insertar archivo" + num_pr);
        var dataFile = {folio: $('#folioSeguimiento').text(), num_preg: num_pr,urlFile:null, codEmpleado:'<?php echo $codEmpleadoResponde; ?>'};
        dataFile.urlFile = rutaImg; 

        $.ajax({
            type: 'POST',
            url: './_php/insertNota_archivo.php',
            dataType: 'json',
            data: {dataFile:dataFile},
             complete: function (xhr, textStatus) {
                //called when complete
                //console.log(textStatus);
            },
            success: function(data) {
                // console.log(data);
                bootbox.alert({
                    size: "small",
                    title: "Alerta",
                    message: "<label>"+data['data']+"</label>",
                    callback: function(){
                        location.reload(1);
                    }
                });                                   
            }
        });//fin ajax   

    }

    function validarAsignacion(idDetalle){
        //console.log(idDetalle);
        // var dataseg = {folio: $('#folioSeguimiento').text(), num_preg: num_pr, codEmpleado:'<?php echo $codEmpleadoResponde; ?>'};
        bootbox.confirm({ 
            size: "small",
            title: 'Enviar a Validación',
            message: "¿Deseas continuar?",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-secondary'
                }
            },
            callback: function(result){ 
            /* result is a boolean; true = OK, false = Cancel*/ 
                if (result === true) {
                    //var datastring = $("#formAcliente").serializeArray(); 

                    $.ajax({
                        type: 'POST',
                        url: './_php/validarCierreAsignacion.php',
                        dataType: 'json',
                        data: {idDetalle:idDetalle},
                         complete: function (xhr, textStatus) {
                            //called when complete
                            //console.log(textStatus);
                        },
                        success: function(data) {
                            // console.log(data)
                            bootbox.alert({
                                size: "small",
                                title: "Alerta",
                                message: "<label>"+data['data']+"</label>",
                                callback: function(){
                                    location.reload(1);
                                }
                            });                                   
                        }
                    });//fin ajax                         
                }
            }
            
        });
    }

    function solicitarReasignacion(idDetalle){
        bootbox.confirm({ 
            size: "small",
            title: 'Solicitar Reasignación',
            message: "¿Deseas continuar?",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-secondary'
                }
            },
            callback: function(result){ 
            /* result is a boolean; true = OK, false = Cancel*/ 
                if (result === true) {
                    //var datastring = $("#formAcliente").serializeArray(); 

                    $.ajax({
                        type: 'POST',
                        url: './_php/soliciaReasignacion.php',
                        dataType: 'json',
                        data: {idDetalle:idDetalle},
                         complete: function (xhr, textStatus) {
                            //called when complete
                            console.log(textStatus);
                        },
                        success: function(data) {
                            //console.log(data);
                            bootbox.alert({
                                size: "small",
                                title: "Alerta",
                                message: "<label>"+data['data']+"</label>",
                                callback: function(){
                                    location.reload(1);
                                }
                            });                                   
                        }
                    });//fin ajax                         
                }
            }
            
        });
    }

    </script>
</html>
