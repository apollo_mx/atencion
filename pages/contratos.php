<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Catalogo de Ubicaciones</title>        

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet">        

        
       
        <!--  <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet"> -->

        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../css/jquery.chosen.css" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

            div.container { max-width: 1200px }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                                                    <div class="clearfix">&nbsp;</div>                        <div class="clearfix">&nbsp;</div>
   <!--                          <h1 class="page-header" style="color: #AC182D;">Gestionar Catálogos</h1> -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="panel-body">
                                <div class="float-left text-left"><button type="button" class="btn btn-success  btn-sm" data-toggle="modal" data-target="#myModal">Agregar Contrato</button></div>
                                <!-- Button trigger modal -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel">Agregar Contrato</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form role="form" id="formNcliente">
                                                    <div class="form-group">
                                                        <label>* Numero de Contrato</label>
                                                        <input id="ncontr" class="form-control" placeholder="Numero de Contrato">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>* Descripción</label>
                                                        <input id="desc" class="form-control" placeholder="Descripción">
                                                    </div>
                                                    
       
                                                    <div class="form-group">
                                                        <label> *Cliente</label>                   
                                                        <select id="clnt" name="clnt" data-placeholder="Selecciona un Cliente" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>                        
                                    
                                                    <div class="form-group">
                                                        <label>* Ubicación</label>
                                                       <select id="ubicaci" name="ubicaci" data-placeholder="Selecciona una Ubicación" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label>* Representante</label>
                                                        <select id="representante" name="representante" data-placeholder="Selecciona un representante" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>* División</label>
                                                        <select id="division" name="division" data-placeholder="Selecciona una división" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>                                                                                                                                    
                                                <!--     <input type="hidden" name="idSistem" id="idSistem"> -->
                                                   
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="float-right text-right"><button type="button" class="btn btn-primary" onclick="nuevoContrato();">Nuevo Contrato</button></div>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                            </div>
                           
                            <div class="panel-body">
                       <!--          <div class="float-left text-left"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal1">Actualizar Contrato</button></div> -->
                                <!-- Button trigger modal -->
                                <!-- Modal -->
                                <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel1">Actualizar Contrato</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form role="form" id="formUcliente">
                                                    <div class="form-group">
                                                        <label>* Numero de Contrato</label>
                                                        <input type="hidden" id="up_id">                                                        
                                                        <input id="up_ncontr" class="form-control" placeholder="Numero de Contrato">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>* Descripción</label>
                                                        <input id="up_desc" class="form-control" placeholder="Descripción">
                                                    </div>
                                                    
       
                                                    <div class="form-group">
                                                        <label> *Cliente</label>                   
                                                        <select id="up_clnt" name="up_clnt" data-placeholder="Selecciona un Cliente" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>                        
                                    
                                                    <div class="form-group">
                                                        <label>* Ubicación</label>
                                                       <select id="up_ubicaci" name="up_ubicaci" data-placeholder="Selecciona una Ubicación" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label>* Representante</label>
                                                        <select id="up_representante" name="up_representante" data-placeholder="Selecciona un representante" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>* División</label>
                                                        <select id="up_division" name="up_division" data-placeholder="Selecciona una división" class="chosen-select" tabindex="-1" style="width: 568px; display: none;">
                                                        </select>
                                                    </div>                                                                                                                                      
                                                <!--     <input type="hidden" name="idSistem" id="idSistem"> -->
                                                   
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="float-right text-right"><button type="button" class="btn btn-primary" onclick="actualizContrato();">Actualizar Contrato</button></div>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                            </div>
                           <table id="table_catalogos" class="display nowrap" style="width:100%"></table>
                        </div>

                       

                        <div class="clearfix">&nbsp;</div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
<!--         <script src="../js/jquery.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
        

<!--         <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script> -->
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>
        <script src="../js/chosen.jquery.min.js"></script>

    <script>
    

    </script>

        <script type="text/javascript">
        	$(document).ready(function(){
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });


                $('#clnt').load('./_php/cboClientes.php', function() {
                    $(this).chosen();
                });

                $('#ubicaci').load('./_php/cboUbicacion.php', function() {
                    $(this).chosen();
                });

                $('#representante').load('./_php/cboRepresentante.php', function() {
                    $(this).chosen();
                });

                $('#division').load('./_php/cboDivision.php', function() {
                    $(this).chosen();
                });

                
        	});
            

            function actualizContrato(){
                
                var idc = $('#up_id').val();
                var upcon = $('#up_ncontr').val();
                var updesc = $('#up_desc').val();
                var upcl = $('#up_clnt').val();
                var upuboc = $('#up_ubicaci').val();
                var uprepr = $('#up_representante').val();
                var updiv = $('#up_division').val();                

                if (idc == "" || upcon== "" || updesc == "" || upcl == "" || upuboc == "" || uprepr == "" || updiv == "") {
                    bootbox.alert({
                        size: "small",
                        title: "Alerta",
                        message: '<label class="alert alert-danger">Los campos con <b>*<b> no pueden estár vacios.</label>',
                        callback: function(){}
                    }); 
                }else{
                    bootbox.confirm({ 
                        size: "small",
                        title: 'Actualizar Contrato',
                        message: "¿Deseas continuar?",
                        buttons: {
                            confirm: {
                                label: 'Continuar',
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-secondary'
                            }
                        },
                        callback: function(result){ 
                        /* result is a boolean; true = OK, false = Cancel*/ 
                            if (result === true) {
                               // console.log("enviar datos");
                                $.ajax({
                                    type: 'POST',
                                    url: './_php/updContrato.php',
                                    dataType: 'json',
                                    data: {idc:idc,upcon:upcon,updesc:updesc,upcl:upcl, upuboc:upuboc,uprepr:uprepr,updiv:updiv},
                                     complete: function (xhr, textStatus) {
                                        //called when complete
                                    },
                                    success: function(data) {
                                        bootbox.alert({
                                            size: "small",
                                            title: "Alerta",
                                            message: "<label>"+data['data']+"</label>",
                                            callback: function(){
                                                //loadClientes();
                                                $('#formNcliente').trigger("reset");
                                                // setTimeout(function () { location.reload(1); }, 3000);
                                            }
                                        });                                    
                                    }
                                });//fin ajax                             
                            }
                        }
                        
                    });
                }
            }

            function loadClientes(){    //Cargar catalogo de clientes
                 
                var table = $('#table_catalogos').dataTable({
                    responsive: true,
                    "language": {
                        "search": "Buscar:"
                    },
                    ajax: {
                        "url":"./_php/listarContratos.php"
                    },
                    columnDefs: [
                        { responsivePriority: 1, targets: 0 },
                        { responsivePriority: 10001, targets: 1 },                                            
                        { responsivePriority: 2, targets: -2 }
                    ],
                    columns: [
                        { data: 'num_contrato',title: 'Contrato' },
                        { data: 'desContrato',title: 'Descripción' },
                        { data: 'nom_representante',title: 'Representante' },
                        { data: 'nom_division',title: 'División' },                                                                          
                        { data: 'nom_cliente',title: 'Cliente' },
                        { data: 'nom_ubicacion',title: 'Ubicacion' },                                                
                        { data: 'opcion',title: 'Opción' },                                                      
                      ]
                });   
                table.DataTable().ajax.reload();
            }


            $('#myModal1, #myModal').on('hidden.bs.modal', function () {
                // put your default event here
                //console.log("se cierra modal actualizar");
                location.reload(1);
            });
            
            function disaContrato(id){  //Desactivar Contrato
                bootbox.confirm({ 
                        size: "small",
                        title: 'Desactivar Contrato',
                        message: "¿Deseas continuar?",
                        buttons: {
                            confirm: {
                                label: 'Continuar',
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-secondary'
                            }
                        },
                        callback: function(result){ 
                        /* result is a boolean; true = OK, false = Cancel*/ 
                            if (result === true) {
                                //var datastring = $("#formAcliente").serializeArray(); 
                                $.ajax({
                                    type: 'POST',
                                    url: './_php/desactivarContrato.php',
                                    dataType: 'json',
                                    data: {id:id},
                                    complete: function (xhr, textStatus) {
                                        //called when complete
                                        setTimeout(function () { location.reload(1); }, 3000);
                                    },
                                    success: function(data) {
                                        bootbox.alert({
                                            size: "small",
                                            title: "Alerta",
                                            message: "<label>"+data['data']+"</label>",
                                            callback: function(){
                                                //loadClientes();
                                                //$('#formNcliente').trigger("reset");
                                                // setTimeout(function () { location.reload(1); }, 3000);
                                            }
                                        });                                    
                                    }
                                });//fin ajax                             
                            }
                        }
                        
                    });
            }

            function getContrato(id,num_contrato,desContrato,num_cliente,nom_ubicacion,nom_representante, nom_division){ //LLenar formulario para actualizar clientes
               

               $('#myModal1').modal('show');
               
                $('#up_id').val(id);
                $('#up_ncontr').val(num_contrato);
                $('#up_desc').val(desContrato);
                
                $('#up_clnt').load('./_php/cboClientesid.php?param1='+num_cliente, function() {
                    $(this).chosen();               
                });

                $('#up_ubicaci').load('./_php/cboUbicacionid.php?param1='+nom_ubicacion, function() {
                    $(this).chosen();               
                });

                $('#up_representante').load('./_php/cboRepresentanteid.php?param1='+nom_representante, function() {
                    $(this).chosen();               
                });

                $('#up_division').load('./_php/cboDivisionid.php?param1='+nom_division, function() {
                    $(this).chosen();
                });


            }

            function nuevoContrato(){    //funcion para enviar los clientes 

                var ncontr = $('#ncontr').val();
                var desc = $('#desc').val();
                var clnt = $('#clnt').val();
                var ubicaci = $('#ubicaci').val();
                var representante = $('#representante').val();
                var division = $('#division').val();


                if (ncontr == "" || desc =="" || clnt =="" || ubicaci =="" || representante =="" || division =="") {
                    bootbox.alert({
                        size: "small",
                        title: "Alerta",
                        message: '<label class="alert alert-danger">Los campos con <b>*<b> no pueden estár vacios.</label>',
                        callback: function(){}
                    });       
                }else{
                    //console.log(ncontr + ' ' + desc + ' ' + clnt + ' ' + ubicaci + ' ' + representante);
                    bootbox.confirm({ 
                        size: "small",
                        title: 'Crear Contrato',
                        message: "¿Deseas continuar?",
                        buttons: {
                            confirm: {
                                label: 'Continuar',
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-secondary'
                            }
                        },
                        callback: function(result){ 
                        /* result is a boolean; true = OK, false = Cancel*/ 
                            if (result === true) {
                                //var datastring = $("#formAcliente").serializeArray(); 
                                $.ajax({
                                    type: 'POST',
                                    url: './_php/insertContrato.php',
                                    dataType: 'json',
                                    data: {ncontr:ncontr,desc:desc,clnt:clnt,ubicaci:ubicaci, representante:representante,division:division},
                                     complete: function (xhr, textStatus) {
                                        //called when complete
                                    },
                                    success: function(data) {
                                        bootbox.alert({
                                            size: "small",
                                            title: "Alerta",
                                            message: "<label>"+data['data']+"</label>",
                                            callback: function(){
                                                //loadClientes();
                                                $('#formUcliente').trigger("reset");
                                                // setTimeout(function () { location.reload(1); }, 3000);
                                            }
                                        });                                    
                                    }
                                });//fin ajax                             
                            }
                        }
                        
                    });
                }
            }

            loadClientes();

            $('.tooltip-demo').tooltip({
                selector: "[data-toggle=tooltip]",
                container: "body"
            })

        </script>

    </body>
</html>