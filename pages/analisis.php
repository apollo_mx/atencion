<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Analisis</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
        <!--         <link href="../css/morris.css" rel="stylesheet"> -->
        <link rel='stylesheet' href='https://pierresh.github.io/morris.js/css/morris.css' crossorigin='anonymous'>

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper" style="min-height: 312px;">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">


                            <div class="col-md-12 page-header" style="text-align: left;"> 
	                            
	                            <div class="form-group">
<!-- 	                                <label>* Año </label> -->
									<select class="custom-select custom-select-lg mb-3" id="an">
									  <option value="0" selected>Año</option>
									  <option value="2016-01-15"> 2016</option>
									  <option value="2017-01-15"> 2017</option>
									  <option value="2018-01-15"> 2018</option>
									  <option value="2019-01-15"> 2019</option>                                 
									  <option value="2020-01-15"> 2020</option>
									  <option value="2021-01-15"> 2021</option>
                                      <option value="2022-01-15"> 2022</option>
                                      <option value="2023-01-15"> 2023</option>                                                                                                                              
									</select>
	                            </div>
                            </div>
                            <h2 class="text-center">Analisis</h2>
<!--                             <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExportLine();"><span>PDF</span></button> -->
                        </div>
                        <!-- /.col-lg-12 -->

                        <div class="col-lg-6">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                    Estatus
                                   <!-- Gerencias Compañías: Año <strong id="title_an"></strong> Trimestre <strong id="title_div"></strong> -->
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
									<div class="col-lg-12">
										<div id="aditi"></div>
									</div>   
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>

                        <div class="col-lg-6">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                Divisiones
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div id="divisiondoDonut"></div>
                                    </div>   
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        <!-- /.panel -->
                        </div>

                        <div class="col-lg-6">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                Tópicos
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div id="topic"></div>
                                    </div>   
                                    <div id="legend" class="col-md-12"></div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        <!-- /.panel -->
                        </div>

                    </div>
                    
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->


        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
        <!-- Custom Theme JavaScript -->

        <!-- Morris Charts JavaScript -->
        <!-- <script src="../js/raphael.min.js"></script> -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js' crossorigin='anonymous'></script>
        <script src='https://pierresh.github.io/morris.js/js/morris.js' crossorigin='anonymous'></script>


        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>

        <script src="../js/dist/html2pdf.bundle.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });
                
                // $('#division').load('./_php/cboDivision.php', function() {
                //     $(this).chosen();
                // });


                

 
            }); ///fin jquery

            
        	$("#an").on("change",function() {
                //$(this).val();
                $("#aditi,#divisiondoDonut,#topic,#legend").empty();
                //var anio = $("#an").val();

                var anio = $(this).val();
               	
                //$("#title_div").text($("#trim option:selected").text());
               	//$("#title_an").text($("#an option:selected").text());               	
               	
                //console.log(anio);
				// console.log(anio);
                $.ajax({
                    type: "GET",
                    url: "./_php/get_analisis.php",
                    dataType: "json",
                    data: {anio:anio},
                    complete: function (xhr, textStatus) {
                        //called when complete
                        //console.log(textStatus);
                    },
                    success: function(data) {

                        // console.log(data.nivel_a_pet);
                        // console.log(data.nivel_a_division);
                        // console.log(data.nivel_a_topicos);

                        Morris.Bar({ //DIBUJAR TABLA
                            element: 'aditi',
                            data: data['nivel_a_pet'],
                            xkey: 'estatus',
                            ykeys: ['tot'],
                            labels: ['#'],
                            barColors: function(row, series, type) {
                                if (type != 'bar') {
                                  return;
                                }
                                if (row.label == "CANCELADO") {
                                    return '#d9534f';
                                }if(row.label == "EN PROCESO"){
                                    return '#5bc0de';  
                                }if (row.label == "CERRADO") {
                                    return '#5cb85c';
                                }
                            },                          
                            xLabelAngle: '40',
                            resize: true,
                            //hideHover: "always",
                            //horizontal: true,
                            grid: true,
                            gridTextSize: 12,
                            dataLabelsSize:12,
                        });

                        Morris.Bar({ //DIBUJAR TABLA
                            element: 'divisiondoDonut',
                            data: data['nivel_a_division'],
                            xkey: 'nombre',
                            ykeys: ['tot'],
                            labels: ['#'],                          
                            xLabelAngle: '40',
                            resize: true,
                            //hideHover: "always",
                            //horizontal: true,
                            grid: true,
                            gridTextSize: 10,
                            dataLabelsSize:10,                        
                        });

                        // Morris.Donut({ //DIBUJAR TABLA
                        //     element: 'topic',
                        //     data: data['nivel_a_topicos'],
                        //     xkey: 'label',
                        //     ykeys: ['value'],
                        //     resize: true,
                        //     showPercentage:true,                      
                        // });

                        var donut = Morris.Donut({
                            element: 'topic',
                            data   : data['nivel_a_topicos'],
                            showPercentage:true,
                        });  
                        
                        donut.options.data.forEach(function(label, i){
                            var legendItem = $('<span></span>').text( label['label']).prepend('&nbsp;<i>&nbsp;&nbsp;&nbsp;</i>');
                            legendItem.find('i').css('backgroundColor', donut.options.colors[i]);
                            $('#legend').append(legendItem)
                        })                        
                                      

                 
                    }
                });//fin //FIN DIBUJAR TABLA DIRECCION INDUSTRIAL  					                         
              

            });
               
        </script>

    </body>
</html>