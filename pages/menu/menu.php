<?php

	$menu ="";
	$cc ="";
	$ac ="";
	$sc ="";
	$cerrC ="";
	$gu = "";
	$osh= "";
	$uni= "";
	$ds= "";
	$ps= "";
	$dps= "";
	$hist ="";
	$pgsServ="";

	function showMenu($idNav, $rol){
		if ($idNav == 'encli') {
			$encli = 'active';
		}else{
			$encli = 'ninguno';
		}

		if ($rol == 1) {
		//es un monitorista
		echo $menu = '
			          <li>
                        <a href="./_php/logout/" class=""><i class="fa fa-sign-out fa-fw"></i> Salir del Sistema</a>
                      </li> 
                      <li>
                        <a href="index.php" class=""><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                      </li>
                       <li class="">
	                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Indicadores<span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 200px;">
	                            <li>
	                                <a href="participacion.php">Participacion</a>
	                            </li> 
	                            <li>
	                                <a href="resultados-encuestas.php">Centros</a>
	                            </li> 
	                            <li>
	                                <a href="gerencias-globales.php">Divisiones Global</a>
	                            </li>
	                            <li>
	                                <a href="gerencias-companias.php">Divisiones Trimestre</a>
	                            </li>
	                            <li>
	                                <a href="satisfaccion-qa.php">Satisfacción QA</a>
	                            </li>	                            	                                     
	                        </ul>
	                  </li>                     
                      <li>
                        <a href="contratos.php" class=""><i class="fa fa-folder-open-o fa-fw"></i> Proyecto</a>
                      </li>
                      <li>
                        <a href="encuestas.php" class=""><i class="fa fa-file-text fa-fw"></i> Encuestas</a>
                      </li>                      
	                  <li class="">
	                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Gestionar Catálogos<span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
	                            <li>
	                                <a href="cclientes.php">Clientes</a>
	                            </li>
	                            <li>
	                                <a href="crepresentantes.php">Representantes</a>
	                            </li>
	                            <li>
	                                <a href="cubicaciones.php">Ubicaciones</a>
	                            </li>
	                            <li>
	                                <a href="cdivision.php">División</a>
	                            </li>	                            
	                        </ul>
	                  </li>';
		}
		if($rol == 2){
		echo $menu = '<li class=""><a href="#"><i class="fa fa-edit fa-fw"></i> Encuestas<span class="fa arrow"></span></a><ul class="nav nav-second-level collapse" aria-expanded="false"><li class="'.$encli.'"><a href="encuesta-clima.php">Administrar Encuestas</a></li></ul></li>
				<li><a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Catalogos
		<span class="fa arrow"></span></a><ul class="nav nav-second-level collapse" aria-expanded="false"><li class="'.$encli.'"><a href="reporte-clima.php">Administrar Catalogos</a></li></ul></li><li><a href="./_php/logout/" class="no-submenu"><i class="fa fa-sign-out fa-fw"></i>Salir del Sistema
	</a></li>';
		}
		if($rol == 3){
			echo $menu = '<li>
                        <a href="./_php/logout/" class=""><i class="fa fa-sign-out fa-fw"></i> Salir del Sistema</a>
                      </li> 
                      <li>
                        <a href="index.php" class=""><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                      </li>
	 										<li class="">
		                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Catálogos<span class="fa arrow"></span></a>
		                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 40;">
		                            <li>
		                                <a href="cdivision.php">Divisiones</a>
		                            </li>
		                            <li>
		                                <a href="cubicaciones.php">Ubicaciones</a>
		                            </li>		                            
		                            <li>
		                                <a href="cclientes.php">Clientes</a>
		                            </li>
		                            <li>
		                                <a href="crepresentantes.php">Representantes</a>
		                            </li>                            
		                        </ul>
		                  </li>
                      <li>
                        <a href="contratos.php" class=""><i class="fa fa-folder-open-o fa-fw"></i> Proyectos</a>
                      </li>
                      <li>
                        <a href="encuestas.php" class=""><i class="fa fa-file-text fa-fw"></i> Encuestar</a>
                      </li>
                      <li>
                        <a href="seguimiento.php" class=""><i class="fa fa-bookmark fa-fw"></i> Seguimiento</a>
                      </li>                                            
                      <li class="">
	                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Tableros<span class="fa arrow"></span></a>
	                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 240px;">
	                            <li>
	                                <a href="resultados-encuestas.php"> Resultados</a>
	                            </li>
	                            <li>
	                                <a href="satisfaccion-qa.php"> Satisfacción</a>
	                            </li>		                            
	                            <li>
	                                <a href="gerencias-globales.php"> Divisiones</a>
	                            </li>
	                            <li>
	                                <a href="gerencias-companias.php"> Trimestres</a>
	                            </li>
	                            <li>
	                                <a href="analisis.php">Peticiones</a>
	                            </li>
	                            <li>
	                                <a href="participacion.php"> Participación</a>
	                            </li>
	                        </ul>
	                  	</li>';
		}	
	}
//	<li class="'.$uni.'"><a href="gestionarUnidades.php" class="no-submenu"><span class="awe-lock"></span> Gestionar Unidades</a></li>
?>