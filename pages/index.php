<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>¡Bienvenido!</title>        

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <!--         <link href="../css/morris.css" rel="stylesheet"> -->
        <link rel='stylesheet' href='https://pierresh.github.io/morris.js/css/morris.css' crossorigin='anonymous'>

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php  echo utf8_encode($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

                </div>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header" style="color: #337ab7;">¡Bienvenido! </h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-yellow">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-map-marker fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge" id="ubica"></div>
                                                    <div>Divisiones</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-red">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-suitcase fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge" id="contra"></div>
                                                    <div>Contratos</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-comments fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge" id="clien"></div>
                                                    <div>Clientes</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-green">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-group fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge" id="repre"></div>
                                                    <div>Representantes</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <!-- /.col-lg-12 -->
                        <div class="col-lg-6">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                Estatus peticiones
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div id="estadoDonut"></div>
                                    </div>   
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        <!-- /.panel -->
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                Peticiones por división
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div id="divisiondoDonut"></div>
                                    </div>   
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        <!-- /.panel -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <!-- <script src="../js/raphael.min.js"></script> -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js' crossorigin='anonymous'></script>
        <script src='https://pierresh.github.io/morris.js/js/morris.js' crossorigin='anonymous'></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#loading').hide(); //initially hide the loading icon

            $(document).ajaxStart(function(){
                $('#loading').show();
                //console.log('shown');
              });
            $(document).ajaxStop(function(){
                $('#loading').hide();
                //console.log('hidden');
            });
            
            $.ajax({
                type: 'POST',
                url: './_php/dashboard.php',
                dataType: 'json',
                 complete: function (xhr, textStatus) {
                    //called when complete
                },
                success: function(data) {
                    // console.log(data);
                    $("#clien").html(data.tot_clientes);
                    $("#repre").html(data.tot_representantes);
                    $("#ubica").html(data.tot_ubicaciones); 
                    $("#contra").html(data.tot_contratos);
                }
            });//fin ajax         


            $.ajax({
                type: "GET",
                url: "./_php/get_general_seg.php",
                dataType: "json",
                complete: function (xhr, textStatus) {
                    //called when complete
                    // console.log(textStatus);
                },
                success: function(data) {
                    //console.log(data['data']);
                    Morris.Bar({ //DIBUJAR TABLA
                        element: 'estadoDonut',
                        data: data['data'],
                        xkey: 'y',
                        ykeys: ['a'],
                        labels: ['#'],
                        barColors: function(row, series, type) {
                            if (type != 'bar') {
                              return;
                            }
                            if (row.label == "CANCELADO") {
                                return '#d9534f';
                            }if(row.label == "EN PROCESO"){
                                return '#5bc0de';  
                            }if (row.label == "CERRADO") {
                                return '#5cb85c';
                            }
                        },                          
                        xLabelAngle: '40',
                        resize: true,
                        //hideHover: "always",
                        //horizontal: true,
                        grid: true,
                        gridTextSize: 10,
                        dataLabelsSize:10,                        
                    });

                    Morris.Bar({ //DIBUJAR TABLA
                        element: 'divisiondoDonut',
                        data: data['dataa'],
                        xkey: 'y',
                        ykeys: ['a'],
                        labels: ['#'],                          
                        xLabelAngle: '40',
                        resize: true,
                        //hideHover: "always",
                        //horizontal: true,
                        grid: true,
                        gridTextSize: 10,
                        dataLabelsSize:10,                        
                    });
                    // Morris.Donut({ //DIBUJAR TABLA
                    //     element: 'divisiondoDonut',
                    //     data: data['dataa'],
                    //     resize: true
                    // });

                }
            });//fin //FIN DIBUJAR TABLA DIRECCION INDUSTRIAL           


        });

        </script>
    </body>
</html>
