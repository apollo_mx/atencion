<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Resultados</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
        <!--         <link href="../css/morris.css" rel="stylesheet"> -->
        <link rel='stylesheet' href='https://pierresh.github.io/morris.js/css/morris.css' crossorigin='anonymous'>


        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>         

                      <div class="row">
                        <div class="col-lg-12">

                            
                           
                                 <br><br>
                                <div class="col-md-12" style="text-align: left;">
                                    <select class="custom-select custom-select-lg mb-3" id="an">
                                      <option value="0" selected>Año</option>
                                      <option value="2016-01-15"> 2016</option>
                                      <option value="2017-01-15"> 2017</option>
                                      <option value="2018-01-15"> 2018</option>
                                      <option value="2019-01-15"> 2019</option>                                 
                                      <option value="2020-01-15"> 2020</option>
                                      <option value="2021-01-15"> 2021</option>
                                                                                                                                                                        
                                    </select>                               
                                </div>
                                <br><br>
                                <div class="col-md-12" style="text-align: left;">
                                    <select class="custom-select custom-select-lg mb-3" id="trimestre">
                                      <option value="0" selected>Trimestre</option>
                                      <option value="1T"> 1T</option>
                                      <option value="2T"> 2T</option>
                                      <option value="3T"> 3T</option>
                                      <option value="4T"> 4T</option>                                  
                                    </select>                               
                                </div>
                          

<!--                                 <h2 class="text-center">Resultado de Encuestas</h2> -->
                            
<!--                             <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExportLine();"><span>PDF</span></button> -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <br><br><br>                        
                            <table id="table_t20" class="display nowrap table" style="width:100%"></table>
                        </div>
                    </div>


                    <div class="row">
                    
                        <div class="col-md-12" id="totenc">
                            <div class="dt-buttons"> 
                                <br>
                                <button class="dt-button buttons-pdf buttons-html5 exp" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExportLine();"><span>PDF</span></button>
                            </div>
                            <div class="col-md-6">
                                <h2 style="text-align: center;"><small>Total de encuestas por división</small></h2>
                                <div id="total-encuestas"></div>
                            </div>
                            <div class="col-md-6">                            
                                <h2 style="text-align: center;"><small>Total de encuestas respondidas por división</small></h2>
                                <div id="total-encuestas2"></div>
                            </div>
                        </div>
                    </div> 
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->


           <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
<!--         <script src="../js/raphael.min.js"></script> -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js' crossorigin='anonymous'></script>

        <script src='https://pierresh.github.io/morris.js/js/morris.js' crossorigin='anonymous'></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>


        <!-- DataTables JavaScript -->
<!--         <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script> -->

        <script src="../js/scriptdown/jquery.dataTables.min.js"></script>
        <script src="../js/scriptdown/dataTables.buttons.min.js"></script>
        <script src="../js/scriptdown/buttons.flash.min.js"></script>
        <script src="../js/scriptdown/jszip.min.js"></script>
        <script src="../js/scriptdown/pdfmake.min.js"></script>
        <script src="../js/scriptdown/vfs_fonts.js"></script>
        <script src="../js/scriptdown/buttons.html5.min.js"></script>
        <script src="../js/scriptdown/buttons.print.min.js"></script>

        <script src="../js/dist/html2pdf.bundle.min.js"></script>
        

        <script type="text/javascript">
            $(document).ready(function(){
                // $("").hide();
                $('#loading,#trimestre,#totenc').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });
            });



            $("#an").on("change",function() {
                //$(this)val();
                //console.log($(this).val());
                if ($(this).val() == 0) {
                    $("#trimestre").val(0);
                    $("#trimestre").fadeOut();                    
                }else{
                    $("#trimestre").fadeIn();
                }
            });

            $("#trimestre").on("change",function() {
                var an = $("#an").val();
                var trimestre = $(this).val();
                if (trimestre != 0) { 
                    loadt20(an,trimestre);    
                    $("#totenc").fadeIn();              
                }
            });



            function loadt20(an,trim){    //Cargar catalogo de clientes

                var table = $('#table_t20').dataTable({
                    dom: 'Bfrtip',
                    destroy: true,
                    buttons: [
                        'excel'
                        //'excel', 'pdf'
                    ],
                    bProcessing: true,
                    "scrollY": 300,
                    "scrollX": true,
                    // scrollCollapse: true,
                    paging: false,
                    responsive: false,
                    "language": {
                        "search": "Filtro:"
                    },
                    "type": "GET",
                    ajax: {
                        "url":"./_php/listarT20.php?an="+an+"&trim="+trim
                    },
                    // columnDefs: [
                    //     { responsivePriority: 1, targets: 1 },
                    //     { responsivePriority: 10001, targets: 1 },                                            
                    // ],
                    columns: [
                        // { data: 'id_respuesta',title: 'Id' },
                        { data: 'division',title: 'División' }, 
                        { data: 'descripcion',title: 'Proyecto' }, 
                        { data: 'respuesta1',title: 'Resultados y Desempeño del producto y la prestación del Servicio' }, 
                        { data: 'respuesta2',title: 'Atención en aspectos de calidad, medio ambiente y seguridad' }, 
                        { data: 'respuesta3',title: 'Comentario: SERVICIO' }, 
                        { data: 'respuesta4',title: 'Estado y Mantenimiento de Equipo e Instalaciones' },
                        { data: 'respuesta5',title: 'Tecnología disponible en Equipo de Monitoreo, Dosificación, Control, etc' }, 
                        { data: 'respuesta6',title: 'Comentario: II. EQUIPOS E INSTALACIONES' }, 
                        { data: 'respuesta7',title: 'Formación y Experiencia del personal' }, 
                        { data: 'respuesta8',title: 'Competencia y Habilidades en la prestación del Servicio' },
                        { data: 'respuesta9',title: 'Comentario: FACTOR HUMANO' }, 
                        { data: 'respuesta10',title: 'Organización y Ejecución en la prestación del Servicio' }, 
                        { data: 'respuesta11',title: 'Medición, Control, Análisis y Mejora en la prestación del Servicio' }, 
                        { data: 'respuesta12',title: 'Comentario: GESTIÓN' },
                        { data: 'respuesta13',title: 'Infraestructura y Recursos disponibles' }, 
                        { data: 'respuesta14',title: 'Comunicación y Servicio al Cliente' }, 
                        { data: 'respuesta15',title: 'Comentario: IMAGEN' },
                        { data: 'estatus',title: 'Estatus' },
                        { data: 'promedio',title: 'Promedio' },                                                               
                      ],
                    columnDefs: [
                        { width: '20%', targets: 2 },
                        { width: '20%', targets: 3 },
                        { width: '20%', targets: 4 },
                        { width: '20%', targets: 5 },
                        { width: '20%', targets: 6 },
                        { width: '20%', targets: 7 },
                        { width: '20%', targets: 8 },
                        { width: '20%', targets: 9 },                                                                       
                    ],
                    // order: [1, 'asc'],
                    // select: {
                    //     style: 'os',
                    //     selector: 'td:first-child'
                    // },         
                    // scrollY:        '70vh',
                    // scrollCollapse: true,
                    // paging:         false
                }); 

                $.ajax({
                    type: 'GET',
                    url: './_php/listarT20.php',
                    dataType: 'json',
                    data: {an:an,trim:trim},
                    complete: function (xhr, textStatus) {
                    },
                    success: function(data) {
                        //console.log(data.allquest);
                        Morris.Bar({ //grafica lineal
                            element: 'total-encuestas',
                            data: data.allquest,
                            xkey: 'titulo',
                            ykeys: ['value'],
                            labels: ['#encuestas'],
                            pointSize: 4,
                            fillOpacity: 0.2,
                            xLabelAngle: '40',
                            horizontal: true,                            
                            resize: true
                        });

                        Morris.Bar({ //grafica lineal
                            element: 'total-encuestas2',
                            data: data.allquest2,
                            xkey: 'titulo',
                            ykeys: ['value'],
                            labels: ['#encuestas'],
                            pointSize: 4,
                            fillOpacity: 0.2,
                            horizontal: true,
                            xLabelAngle: '40',
                            resize: true,
                            barColors: ["#7CB07C"],
                        });

                    }
                });//fin ajax
                $("#total-encuestas").empty();
                $("#total-encuestas2").empty();

            }
        
            function pdfExportLine(){
                //console.log(nameId);
                var element = document.getElementById('totenc');
                var opt = {
                  margin:       0.1,
                  filename:     'reporte-encuesta',
                  image:        { type: 'jpeg', quality: 0.98 },
                  html2canvas:  { scale: 1 },
                  jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
                };
                html2pdf(element, opt);
            }
            
        </script>

    </body>
</html>