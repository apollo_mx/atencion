<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 

if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
if ($_SESSION['user']['id_rol'] !="2") {
  header("location: ./login.php");
}
 $idCuest = $_GET['idCuest'];
//session_destroy();


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Reporte | Encuesta de Clima Organizacional</title>        

        <!-- Bootstrap Core CSS -->

        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">


        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
     

   <!-- <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet"> -->
<!--         <link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet">         -->
    

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

    </head>
   <body>
 <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
              
                
            </nav>

            <div id="wrapper">
            <!-- <div id="loading" class="col-md-4" style="text-align: center;"> 
              <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> 
            </div> -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header" style="color: #AC182D; margin: 55px 0px 20px;">Reporte | Encuestas por usuario </h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">
                        <!-- /.col-lg-6 -->
                      <div class="col-lg-12">
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  Detalle de encuesta por usuario
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">

<!--                                 <input type="button" onclick="tableToExcel('usuario-encuesta', 'W3C Example Table')" value="Export to Excel"> -->
                                <?php
                  
                                  require_once('./_php/db.class.php');
                                  
                                  $db = DataBase::connect();// inicio obtener las preguntas de los cuestionarios
                                    $db->setQuery("select id_usuario, fecha_inicio, fecha_termino, u.nombre as nom_user, e.nombre from estatus_encuestas 
                                    inner join usuarios u
                                    on u.numero_empleado = estatus_encuestas.id_usuario
                                    inner join empresa e
                                    on e.id = u.id_empresa
                                    where estatus_encuestas.id_cuestionario = ".$idCuest." order by u.nombre;"); //and id_usuario = 11497
                                    $rows = $db->loadObjectList();
                                    $reg ="";
                                    if($rows){

                                    echo "<table class='display nowrap table' style='width:100%' id='usuario-encuesta'>";
                                    echo "<thead><tr><th>Usuario</th><th>P1</th><th>P2</th><th>P3</th><th>P4</th><th>P5</th><th>P6</th><th>P7</th><th>P8</th><th>P9</th><th>P10</th><th>P11</th><th>P12</th><th>P13</th><th>P14</th><th>P15</th><th>P16</th><th>P17</th><th>P18</th><th>P19</th><th>P20</th><th>P21</th><th>P22</th><th>P23</th><th>P24</th><th>P25</th><th>P26</th><th>P27</th><th>P28</th><th>P29</th><th>P30</th><th>P31</th><th>P32</th><th>P33</th><th>P34</th><th>P35</th><th>P36</th><th>P37</th><th>P38</th><th>P39</th><th>P40</th><th>P41</th><th>P42</th><th>P43</th><th>P44</th><th>P45</th><th>P46</th><th>P47</th><th>P48</th><th>P49</th><th>P50</th><th>P51</th><th>P52</th></tr></thead>";
                                    echo "<tbody>";
                                    foreach($rows as $registro){
                                      $db1 = DataBase::connect();
                                      $db1->setQuery("select id as idPreg from preguntas where id_cuestionario =".$idCuest); //".$registro->id_usuario ." and r.id_pregunta =1;"
                                      $rows1 =$db1->loadObjectList();
                                      echo "<tr><td>".$registro->nom_user."</td>";
                                      
                                      $i=1;
                                      foreach ($rows1 as $preguntas) { //Obtener id de todas las preguntas de un cuestionario
                                      // $preg="";
                                        //$preg.=$preguntas->idPreg.',';
                                          $db2 = DataBase::connect();
                                          $db2->setQuery("select r.id_pregunta,o.nombre, o.valor from respuestas r inner join opciones o on r.id_opcion = o.id where r.id_usuario = ".utf8_encode($registro->id_usuario)." and r.id_pregunta =".$preguntas->idPreg);
                                          $rows2 =$db2->loadObject();
                                          

                                          //echo $rows2->nombre;  
                                          if ($rows2) {
                                            echo "<td>".$rows2->nombre."</td>";
                                          }else{
                                            echo "<td>Sin respuestas</td>";
                                          } 
                                          //echo $preg;
                                        $i++;
                                      } 
                                      //print_r($preg);
                                      echo "</tr>";
                                    }
                                    }
                                      echo "<tbody>";
                                      echo "</table>";
                                      

                                  //$jsondata['aaData'] = $str2;

                                  //echo json_encode($str);

                                  
                                    //echo json_encode($jsondata);

                                ?>

                                      </div>
                                      <!-- /.panel-body -->
                                  </div>
                                  <!-- /.panel -->
                              </div>
                              <!-- /.col-lg-6 -->                                                                 
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
<!--         <script src="../js/raphael.min.js"></script> -->
<!--     <script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js' crossorigin='anonymous'></script>

        <script src='https://pierresh.github.io/morris.js/js/morris.js' crossorigin='anonymous'></script>
 -->
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>


        <!-- DataTables JavaScript -->
<!--         <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script> -->

        <script src="../js/scriptdown/jquery.dataTables.min.js"></script>
        <script src="../js/scriptdown/dataTables.buttons.min.js"></script>
        <script src="../js/scriptdown/buttons.flash.min.js"></script>
        <script src="../js/scriptdown/jszip.min.js"></script>
        <script src="../js/scriptdown/pdfmake.min.js"></script>
        <script src="../js/scriptdown/vfs_fonts.js"></script>
        <script src="../js/scriptdown/buttons.html5.min.js"></script>
        <script src="../js/scriptdown/buttons.print.min.js"></script>

        <script src="../js/dist/html2pdf.bundle.min.js"></script>
                                                               
<!--         https://code.jquery.com/jquery-3.5.1.js
        https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js
        https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js
        https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js
        https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
        https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js
        https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js
        https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js

        https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js
 -->
      <script type="text/javascript">
          $(document).ready(function(){
                $("#usuario-encuesta").DataTable({
                    dom: "Bfrtip",
                    buttons: ["excel"],
                    bProcessing: true,
                    "bInfo" : false,
                    "scrollY": 300,
                    "scrollX": true,
                    paging: false,
                    responsive: true,
                    "language": {
                        "search": "Filtro:",
                        "zeroRecords": " "                        
                    },
                });
          }); 

        </script>
    </body>
</html>