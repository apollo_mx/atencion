<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Divisiones</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
        <!--         <link href="../css/morris.css" rel="stylesheet"> -->
        <link rel='stylesheet' href='https://pierresh.github.io/morris.js/css/morris.css' crossorigin='anonymous'>

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper" style="min-height: 312px;">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="col-md-12 page-header">
                                <select class="custom-select custom-select-lg mb-3" id="an" style="text-align: left;">
                                  <option value="0" selected>Año</option>
                                  <option value="2016-01-15"> 2016</option>
                                  <option value="2017-01-15"> 2017</option>
                                  <option value="2018-01-15"> 2018</option>
                                  <option value="2019-01-15"> 2019</option>                                 
                                  <option value="2020-01-15"> 2020</option>
                                  <option value="2021-01-15"> 2021</option>
                                </select>                               
                            </div>
<!--                             <h2 class="text-center">Gerencias Divisiones</h2> -->
<!--                             <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExportLine();"><span>PDF</span></button> -->
                        </div>
                        <!-- /.col-lg-12 -->

                                <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel1">Centros</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Año</th>
                                                            <th>División</th>
                                                            <th>Descripción / Proyecto</th>
                                                            <th>Promedio</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="listadoReg" style="font-size: 12px;">
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
<!--                                             <div class="modal-footer">
                                                
                                            </div> -->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->

                                

                        <div class="col-lg-12">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                    Dirección Industrial
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        
                                        <table class="display nowrap table" style="width:100%">
                                        <thead id="encabezado">
                                        </thead>
                                        <tbody id="cuerpo"> </tbody>
                                        </table>
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Aditivos</div>
                                            <div id="aditi"></div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">I. Ind</div>
                                            <div id="iind"></div>
                                        </div> 
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Refinación</div>
                                            <div id="refinacion"></div>
                                        </div>                                                                                
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">T. Aguas</div>
                                            <div id="taguas"></div>
                                        </div> 
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">I. Privada</div>
                                            <div id="iprivada"></div>
                                        </div>                                                                            
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                    Dirección Ductos
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        
                                        <table class="display nowrap table" style="width:100%">
                                        <thead id="ductos-encabezado">
                                        </thead>
                                        <tbody id="ductos-cuerpo"> </tbody>
                                        </table>
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Ductos</div>
                                            <div id="ductos"></div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Duba</div>
                                            <div id="duva"></div>
                                        </div> 
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Trazadores</div>
                                            <div id="trazadora"></div>
                                        </div>                                     
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-green">
                                <div class="panel-heading" style="text-align: center;">
                                    Dirección Petróleo
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        
                                        <table class="display nowrap table" style="width:100%">
                                        <thead id="petroleo-encabezado">
                                        </thead>
                                        <tbody id="petroleo-cuerpo"> </tbody>
                                        </table>
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">PEP Sur</div>
                                            <div id="p_sur"></div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">P. Nte</div>
                                            <div id="p_norte"></div>
                                        </div> 
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Produc. de Pozos</div>
                                            <div id="prod_pozos"></div>
                                        </div>                                                                                
                                        <div class="col-lg-12">
                                            <div class="panel panel-success" style="text-align: center;">Remediaciones</div>
                                            <div id="remediaciones"></div>
                                        </div>                                                
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>


                    </div>
                    
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->


        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
        <!-- Custom Theme JavaScript -->

        <!-- Morris Charts JavaScript -->
        <!-- <script src="../js/raphael.min.js"></script> -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js' crossorigin='anonymous'></script>
        <script src='https://pierresh.github.io/morris.js/js/morris.js' crossorigin='anonymous'></script>


        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>

        <script src="../js/dist/html2pdf.bundle.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });
 
            }); ///fin jquery

            
        	$("#an").on("change",function() {
                //$(this).val();
                var anio = $(this).val();
                if (anio != 0) {
           
                    $("#encabezado,#cuerpo").empty();//INICIO DIBUJAR TABLA DIRECCION INDUSTRIAL
					$.ajax({
	                    type: "GET",
	                    url: "./_php/get_g_global_industrial.php",
                    	dataType: "json",
	                    data: {anio:anio},
	                    complete: function (xhr, textStatus) {
	                        //called when complete
	                        //console.log(textStatus);
	                    },
	                    success: function(data) {
	                        //$("#lapso").text(data.combinados_global['lapso']);
	                    	var opt_header ="";
	                    	var opt_aditivos ="";	                    	
	                    	var opt_refinacion ="";
	                    	var opt_iind ="";
	                    	var opt_iprivada ="";
	                    	var opt_taguas = "";
                            var showI = "";
	                    	//console.log(data['encabezado']);
		                    $.each(data['encabezado'], function( key, val ) {
	                            //console.log(val);
	                            opt_header +='<th>'+val.anio+'</th>';                               
	                        });
                        	$.each(data['body0'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_aditivos +='<td>'+val.promedio+'%</td>';
                            });
                        	$.each(data['body1'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_iind +='<td>'+val.promedio+'%</td>';
	                        });	                        
                        	$.each(data['body2'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_refinacion +='<td>'+val.promedio+'%</td>';
	                        });
                        	$.each(data['body3'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_taguas +='<td>'+val.promedio+'%</td>';
	                        });	                        
                        	$.each(data['body4'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_iprivada +='<td>'+val.promedio+'%</td>';
	                        });	     	                                           
                        	
	                        $("#encabezado").append('<tr><th>División</th>'+opt_header+'</tr>');
	                        $("#cuerpo").append('<tr><td>Aditivos</td>'+opt_aditivos+'</tr>');	                        
	                       	$("#cuerpo").append('<tr><td>I. Ind.</td>'+opt_iind+'</tr>');	                        
	                       	$("#cuerpo").append('<tr><td>Refinación</td>'+opt_refinacion+'</tr>');
	                       	$("#cuerpo").append('<tr><td>T. Aguas</td>'+opt_taguas+'</tr>');	                       	
	                       	$("#cuerpo").append('<tr><td>I. Privada</td>'+opt_iprivada+'</tr>');
                            // $("#dir-industrial").append("graficas");
                            $('#aditi,#iind,#refinacion,#taguas,#iprivada').empty();
                            // iind
                            // refinacion
                            // taguas
                            // iprivada
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'aditi',
                                data: data['body0'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });

                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'iind',
                                data: data['body1'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'refinacion',
                                data: data['body2'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });  
                                                                                  
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'taguas',
                                data: data['body3'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });

                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'iprivada',
                                data: data['body4'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });                            
	                    }
	                });//fin //FIN DIBUJAR TABLA DIRECCION INDUSTRIAL

					$("#ductos-encabezado, #ductos-cuerpo").empty();//INICIO DIBUJAR TABLA DIRECCION DUCTOS
					$.ajax({
	                    type: "GET",
	                    url: "./_php/get_g_global_ductos.php",
                    	dataType: "json",
	                    data: {anio:anio},
	                    complete: function (xhr, textStatus) {
	                        //called when complete
	                        //console.log(textStatus);
	                    },
	                    success: function(data) {
	                        //$("#lapso").text(data.combinados_global['lapso']);
	                    	var opt_header ="";
	                    	var opt_ductos ="";	                    	
	                    	var opt_duva ="";
	                    	var opt_trazadores ="";
                            var showI = "";
	                    	//console.log(data['encabezado']);
		                    $.each(data['encabezado'], function( key, val ) {
	                            //console.log(val);
	                            opt_header +='<th>'+val.anio+'</th>';
	                        });
	                        
                        	$.each(data['body0'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_ductos +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });
                        	$.each(data['body1'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            opt_duva +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });	                
	                        //console.log(data['body1']);        
                        	$.each(data['body2'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }
	                            //console.log(val);
	                            opt_trazadores +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });	     	                                           
                        	
	                        $("#ductos-encabezado").append('<tr><th>División</th>'+opt_header+'</tr>');
	                        $("#ductos-cuerpo").append('<tr><td>Ductos</td>'+opt_ductos+'</tr>');	                        
	                       	$("#ductos-cuerpo").append('<tr><td>Duba</td>'+opt_duva+'</tr>');	                        
	                       	$("#ductos-cuerpo").append('<tr><td>Trazadores</td>'+opt_trazadores+'</tr>');
                            $('#ductos,#duva,#trazadora').empty();
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'ductos',
                                data: data['body0'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });  
                                                                                  
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'duva',
                                data: data['body1'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });

                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'trazadora',
                                data: data['body2'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            }); 
	                    }
	                });//fin //FIN DIBUJAR TABLA DIRECCION DUCTOS

					$("#petroleo-encabezado, #petroleo-cuerpo").empty();//INICIO DIBUJAR TABLA DIRECCION DUCTOS
					//ductos-encabezado  ductos-cuerpo
					$.ajax({
	                    type: "GET",
	                    url: "./_php/get_g_global_petroleo.php",
                    	dataType: "json",
	                    data: {anio:anio},
	                    complete: function (xhr, textStatus) {
	                        //called when complete
	                        //console.log(textStatus);
	                    },
	                    success: function(data) {
	                        //$("#lapso").text(data.combinados_global['lapso']);
	                    	var opt_header ="";
	                    	var opt_pepsur ="";	                    	
	                    	var opt_pnte ="";
	                    	var opt_prpozos ="";
							var opt_remediaciones ="";
                            var showI = "";                 	

	                    	//console.log(data['encabezado']);
		                    $.each(data['encabezado'], function( key, val ) {
	                            opt_header +='<th>'+val.anio+'</th>';
	                        });
	                        
                        	$.each(data['body0'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }                                
	                            //console.log(val);
	                            opt_pepsur +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });

                        	$.each(data['body1'], function( key, val ) {
                                console.log(showI)
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }                                
	                            //console.log(val);
	                            opt_pnte +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });	 

                        	$.each(data['body2'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }                                
	                            //console.log(val);
	                            opt_prpozos +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });	

                        	$.each(data['body3'], function( key, val ) {
                                if (val.number_ittem >1) {
                                    showI = '<span class="badge badge-info" style="cursor: pointer;" onclick="showReg(\''+val.an+'\','+val.trim+','+val.div+');">'+val.number_ittem+'</span>';
                                }                                
	                            //console.log(val);
	                            opt_remediaciones +='<td>'+val.promedio+'% '+showI+'</td>';
                                showI = "";
	                        });		                             	                                           
                        	
	                        $("#petroleo-encabezado").append('<tr><th>División</th>'+opt_header+'</tr>');
	                        $("#petroleo-cuerpo").append('<tr><td>PEP Sur</td>'+opt_pepsur+'</tr>');	                        
	                       	$("#petroleo-cuerpo").append('<tr><td>P. Nte</td>'+opt_pnte+'</tr>');	                        
	                       	$("#petroleo-cuerpo").append('<tr><td>Produc. de Pozos</td>'+opt_prpozos+'</tr>');
	                       	$("#petroleo-cuerpo").append('<tr><td>Remediaciones</td>'+opt_remediaciones+'</tr>');
                            $("#p_sur, #p_norte, #prod_pozos, #remediaciones").empty();// petroleo limpiar graficas
                            
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'p_sur',
                                data: data['body0'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });  
                                                                                  
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'p_norte',
                                data: data['body1'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });

                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'prod_pozos',
                                data: data['body2'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            }); 
                            
                            Morris.Bar({ //DIBUJAR TABLA
                                element: 'remediaciones',
                                data: data['body3'],
                                xkey: 'anio',
                                ykeys: ['promedio'],
                                labels: ['%'],
                                barColors: function(row, series, type) {
                                    if (type != 'bar') {
                                      return;
                                    }
                                    if (row.y >=80) {
                                        return '#00b050';
                                    }if(row.y >=71 && row.y <=79){
                                        return '#ffc000';  
                                    }if (row.y >=10 && row.y <=70) {
                                        return '#ff0000';
                                    }else{
                                        return '#337ab7';
                                    }
                                },                           
                                xLabelAngle: '40',
                                resize: true,
                                //hideHover: "always",
                                //horizontal: true,
                                grid: true,
                                gridTextSize: 10,
                                dataLabelsSize:10,
                            });                             
	                    }
	                });//fin //FIN DIBUJAR TABLA DIRECCION DUCTOS

	                //get_g_global_petroleo                                
                }


            });
                function showReg(an,trim,div){
                    // console.log(an);
                    // console.log(trim);
                    // console.log(div);
                    
                    $('#myModal1').modal('show');
                    $.ajax({
                        type: "GET",
                        url: "./_php/get_g_global_detalleid.php",
                        dataType: "json",
                        data: {anio:an, trim:trim,div:div},
                        complete: function (xhr, textStatus) {
                            //called when complete
                            //console.log(textStatus);
                        },
                        success: function(data) {
                            //console.log(data);
                            $("#listadoReg").empty();        
                            var opt_reg = "";
                            $.each(data['registros'], function( key, val ) {
                                //console.log(val);
                                opt_reg +='<tr><td>'+val.anio+'</td><td>'+val.division+'</td><td>'+val.descrip+'</td><td>'+val.promedio+'%</td></tr>';
                            }); 
                    
                            $("#listadoReg").append(opt_reg);

                        }
                    });//fin //FIN DIBUJAR TABLA DIRECCION DUCTOS                  
                }
        </script>

    </body>
</html>