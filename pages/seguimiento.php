<?php 
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Seguimiento</title>        

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet">         -->

        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">        
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">
        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

            div.container { max-width: 1200px }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper">
            <div id="loading" class="col-md-6" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="clearfix">&nbsp;</div>
                            <h2 class="text-center">Seguimiento</h2>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="table_segui" class="table table-striped table-bordered table-hover dataTable no-footer" style="width:100%"></table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
<!--         <script src="../js/jquery.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
        

<!--         <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script> -->
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>

    <script>
    

    </script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });
                
            });
            
            function loadSeguimientos(){    //Cargar catalogo de clientes
                // $.ajax({
                //     type: 'GET',
                //     url: './_php/get_seguimientos_all.php',
                //     dataType: 'json',
                //      complete: function (xhr, textStatus) {
                //         //called when complete
                //         //console.log(xhr);
                //     },
                //     success: function(data) {
                //         console.log(data);
                        
                //         // var enviado = "";
                //         // // var error = "";
                //         // //enviado +='<span>'+value+'</span><br>';
                        
                //         // $.each(data['data'], function( key, value ) {
                //         //     enviado +='<span>'+value+'</span><br>';
                //         // });
                        
                //         // // $.each(data['error'], function( key, value ) {
                //         // //     error += '<span>'+value+'</span><br>';
                //         // // });

                //         // $("#estatusmail").html('<div class="alert alert-success">'+enviado+'</div>');
                //         //<div class="alert alert-danger"><h5>Correos fallidos</h5>'+error+'</div>
                          
                //     }
                // });//fin ajax 

                var table = $('#table_segui').dataTable({
                   dom: 'Bfrtip',
                    bProcessing: true,
                    "scrollY": 440,
                    destroy: true,
                    "scrollX": true,
                    "autoWidth": true,
                     retrieve: true,
                    // scrollCollapse: true,
                    "paging": false,
                    // "ordering": false,
                    "bInfo": false,
                    "language": {
                        "search": "Buscar:"
                    },
                    ajax: {
                        "url":"./_php/get_seguimientos_all.php"
                    },
                    // columnDefs: [
                    //     { responsivePriority: 1, targets: 1 },
                    //     { responsivePriority: 10001, targets: 1 },                                            
                    // ],
                    columns: [
                        // {
                        //     data: 'opcion',
                        //     defaultContent: '',
                        //     className: 'select-checkbox',
                        //     orderable: false
                        // },
                        { data: 'division',title: 'Division' },
                        { data: 'proyecto',title: 'Contrato' },
                        { data: 'cliente_nombre',title: 'Cliente' },
                        { data: 'anio',title: 'Año' },                         
                        { data: 'trimestre',title: 'Trimestre' },       
                        { data: 'ubicacion_nombre',title: 'Ubicacion' },                                               
                        { data: 'postdate',title: 'Días' },   
                        { data: 'folio',title: 'Folio' },                                                                                  
                        { data: 'estatus',title: 'Estatus' },                                                                               
                      ],
                    order: [1, 'asc'],
                    select: {
                        style: 'os',
                        selector: 'td:first-child'
                    },
                    buttons: [
                                {
                                    text: "Add",
                                    action: function (e, dt, node, config) {
                                        AddNewRIKDialog(e);
                                    }
                                },
                                {
                                    text: "Edit",
                                    action: function () { alert("Edit RIK"); }
                                },
                                {
                                    text: "Remove",
                                    action: function () { alert("remove RIK"); }
                                }
                            ],          
                    scrollY:        '50vh',
                    scrollCollapse: true,
                    paging:         false
                });   
                table.DataTable().ajax.reload();
            }

            loadSeguimientos();

            $('.tooltip-demo').tooltip({
                selector: "[data-toggle=tooltip]",
                container: "body"
            });

            function seguir(dato){
                alert(dato);
            }
            

        </script>

    </body>
</html>