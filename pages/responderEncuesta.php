<?php 

$tok = $_GET['tok'];
$tr = $_GET['tr'];
$anio = $_GET['an'];

include "./mcript.php";
//echo $tok;

$token = $desencriptar($tok);
$trim = $desencriptar($tr);
$an = $desencriptar($anio);


    require_once('./_php/db.class.php');
    $db = DataBase::connect();
    $db->setQuery("select r.id as id_respuesta, c.num_contrato,c.descripcion,cd.nombre as division,r.anio_trimestre,r.trimestre, r.estatus from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_division cd 
on cd.id = c.id_division where r.id_contrato = ".$token." and r.trimestre = ".$trim." and r.anio_trimestre = '".$an."';");
    $rowe = $db->loadObject();

    $date = date_create($an); 
    $anFormat = date_format($date,"Y");

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Encuestas</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
       <style type="text/css">
           .titulo{
            color: #fff;
            background-color: #7F7F7F;
            text-align: center;
            font-size: 20px !important;
           }
           label{
            font-size: 13px;
            color: #969696;
           }
           .subtit{
            font-weight: 700;
            color: #000;
            font-size: 14px;
           }
            .subtit1 {
                font-weight: 700;
                color: #000;
/*                font-size: 14px;*/
                text-align: right;
                display: block;
            }           
           .checkbox, .radio {
                margin-top: 5px;
                margin-bottom: 5px;
                padding-left: 20px;
            }

       </style>
    </head>
    <body class="bg-light">

        
	    <main role="main">

			<div class="container">
            <div id="loading" class="col-md-7" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>

				<br><br>
				    <?php if ($rowe->estatus == "NP") {
                    ?>

                <div class="col-md-10 col-md-offset-1">
                    <form role="form" id="f_encuesta2">
                        <div class="col-md-6" style="font-weight: 700; color: #002e5b; font-size: 14px; text-align: left;">
                            <img src="https://apollo.mx/wp-content/uploads/2020/10/logo.png" style="width: 160px; height: px;" alt="APOLLO">
                        </div>
                        
                        <div class="col-md-12" style="color: #002e5b; font-size: 13px; color: #969696;">
                            <label class="subtit1">Contrato: <?php echo $rowe->num_contrato; ?></label>
                            <label class="subtit1">Descripcion: <?php echo utf8_encode($rowe->descripcion); ?></label>
                            <label class="subtit1">División: <?php echo utf8_encode($rowe->division); ?></label>
                            <label class="subtit1">Año: <?php echo $anFormat; ?></label>
                            <label class="subtit1">Trimestre a evaluar: <?php echo $trim; ?></label>
                            <input type="hidden" id="id_respuesta" name="id_respuesta" value="<?php echo $rowe->id_respuesta; ?>">                          
<!--                             <br>                  
                            <span style="font-weight: 700; font-size: 14px; color:#002e5b;">Objetivo:</span> Identificar su  opinión respecto de nuestro servicio.   <br>
                            <span style="font-weight: 700; font-size: 14px; color:#002e5b;">Instrucción:</span> Favor de marcar la opción que corresponda con su opinión para cada aspecto, según la siguiente escala.<br> -->
                        </div>
                    </form>
                </div>
				<form role="form" id="f_encuesta">
					<div class="col-md-10 col-md-offset-1">
						<table class="table table-borderless" style="width:100%">
							<tbody>
                                <tr>
                                    <td colspan="6" class="titulo">Servicio</td>               
                                </tr>
								<tr>
									<td><label class="subtit">Resultados y desempeño del producto</label></td>
									<td><div class="radio"><label><input class="radio-button" type="radio" name="1" value="1">Totalmente insatisfecho</label></div></td>
									<td><div class="radio"><label><input class="radio-button" type="radio" name="1" value="2">Insatisfecho</label></div></td>
									<td><div class="radio"><label><input class="radio-button" type="radio" name="1" value="3">Indiferente</label></div></td>
									<td><div class="radio"><label><input class="radio-button" type="radio" name="1" value="4">Satisfecho</label></div></td>
									<td><div class="radio"><label><input class="radio-button" type="radio" name="1" value="5">Totalmente satisfecho</label></div></td>
								</tr>
                                <tr>
                                    <td><label class="subtit">Atención en aspectos de calidad, medio ambiente y seguridad</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="2" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="2" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="2" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="2" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="2" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Comentarios:</label></td>
                                    <td colspan="5">
                                        <div class="form-group">
                                            <textarea class="form-control" id="3" name="3" rows="3"></textarea>
                                        </div>
                                    </td>
                                </tr>      
                                <tr>
                                    <td colspan="6" class="titulo">Equipos e instalaciones</td>               
                                </tr>
                                <tr>
                                    <td><label class="subtit">Estado y mantenimiento de equipo e instalaciones</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="4" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="4" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="4" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="4" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="4" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Tecnología disponible en equipo monitoreo, dosificación, control, etc.</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="5" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="5" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="5" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="5" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="5" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Comentarios:</label></td>
                                    <td colspan="5">
                                        <div class="form-group">
                                            <textarea class="form-control" id="6" name="6" rows="3"></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="titulo">Factor humano</td>               
                                </tr>
                                <tr>
                                    <td><label class="subtit">Formación y experiencia del personal</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="7" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="7" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="7" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="7" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="7" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Competencia y habilidades en la prestación del servicio</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="8" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="8" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="8" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="8" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="8" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Comentarios:</label></td>
                                    <td colspan="5">
                                        <div class="form-group">
                                            <textarea class="form-control" id="9" name="9" rows="3"></textarea>
                                        </div>
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="6" class="titulo">Gestión</td>               
                                </tr>
                                <tr>
                                    <td><label class="subtit">Organización y ejecución en la prestación del servicio</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="10" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="10" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="10" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="10" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="10" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Medición, control, análisis y mejora en la prestación del servicio</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="11" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="11" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="11" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="11" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="11" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Comentarios:</label></td>
                                    <td colspan="5">
                                        <div class="form-group">
                                            <textarea class="form-control" id="12" name="12" rows="3"></textarea>
                                        </div>
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="6" class="titulo">Imagen</td>               
                                </tr>
                                <tr>
                                    <td><label class="subtit">Infraestructura y recursos disponibles</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="13" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="13" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="13" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="13" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="13" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Comunicación y servicio al cliente</label></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="14" value="1">Totalmente insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="14" value="2">Insatisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="14" value="3">Indiferente</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="14" value="4">Satisfecho</label></div></td>
                                    <td><div class="radio"><label><input class="radio-button" type="radio" name="14" value="5">Totalmente satisfecho</label></div></td>
                                </tr>
                                <tr>
                                    <td><label class="subtit">Comentarios:</label></td>
                                    <td colspan="5">
                                        <div class="form-group">
                                            <textarea class="form-control" id="15" name="15" rows="3"></textarea>
                                        </div>
                                    </td>
                                </tr>                                                                                                                              
							</tbody>
						</table>
                        <div class="col-md-6"></div>  
                        <div class="col-md-12" style="text-align: right;">
                            <input type="button" class="btn btn-primary" onclick="enviarEncuesta(<?php echo $token; ?>);" value="Enviar">
                        </div>  
                    </div>
				</form>
                
                <?php   
                    //si no se ha respondido la encuesta entonces muestra formulario 
                    }else{ 
                ?>
                <div class="col-md-10 col-md-offset-1">
                        <div class="col-md-6" style="font-weight: 700; color: #002e5b; font-size: 14px; text-align: left;">
                            <img src="https://apollo.mx/wp-content/uploads/2020/10/logo.png" style="width: 160px; height: px;" alt="APOLLO">
                        </div>
                        <div class="col-md-6" style="font-weight: 700; color: #000; font-size: 14px; text-align: right;">
                            Química Apollo S.A. de C.V.<br>Encuesta de Satisfacción del Cliente
                        </div>                        
                        <div class="col-md-12" style="color: #002e5b; font-size: 13px; color: #969696; text-align: center;">
                            <br>
<!--                                 <h4 style="letter-spacing: -.06em; line-height: 45px; padding: 10px 0 0; color: #005a87;">GRACIAS POR HABER RESPONDIDO LA ENCUESTA DE SATISFACCIÓN CORRESPONDIENTE A:</h4> -->
                                <h3>Contrato: <?php echo $rowe->num_contrato; ?></h3>
                                <h3>Descripción: <?php echo utf8_encode($rowe->descripcion); ?></h3>
                                <h3>División: <?php echo utf8_encode($rowe->division); ?></h3> 
                                <h3>Año: <?php echo $anFormat; ?></h3>                                                               
                                <h3>Trimestre: <?php echo $trim; ?><br></h3>
                        </div>
     

                        <div class="col-md-6"></div>
 
                    </div>
                <?php
                    }
                ?>

			</div>	
            <br>
	    </main>

    </body>
<!-- jQuery -->
    <script src="../js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../js/metisMenu.min.js"></script>
    <script src="../js/dataTables/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../js/startmin.js"></script>
    <script src="../js/bootbox.min.js"></script>

    <script type="text/javascript">
    	$(document).ready(function(){
            $('#loading').hide(); //initially hide the loading icon

            $(document).ajaxStart(function(){
                $('#loading').show();
                //console.log('shown');
              });
            $(document).ajaxStop(function(){
                $('#loading').hide();
                //console.log('hidden');
            });

            
            // $("#9").css('border-color','red');
            // $("#12").css('border-color','red');
            // $("#15").css('border-color','red');                      
    	});


        function enviarEncuesta(idContrato){
            
            var datos = $("#f_encuesta").serializeArray();
            var validacion = [];

            if (datos.length >= 15) {

                if (datos[0]['value'] <=3 || datos[1]['value'] <=3) {
                    if (datos[2]['value'] == "") {                           
                        bandera = 1;
                        $('#3').css("border-color","red");
                        $('#3').focus();
                    }else{
                        bandera = 0;
                    }
                }else if(datos[0]['value'] >=4 || datos[1]['value'] >=4){
                    bandera = 0;
                    $('#3').css("border-color","#ccc");
                }
                validacion.push(bandera);               
                
                if (datos[3]['value'] <=3 || datos[4]['value'] <=3) {
                    if (datos[5]['value'] == "") {                           
                        bandera = 1;
                        $('#6').css("border-color","red");
                        $('#6').focus();
                    }else{
                        bandera = 0;
                    }
                }else if(datos[3]['value'] >=4 || datos[4]['value'] >=4){
                    bandera = 0;
                    $('#6').css("border-color","#ccc");
                }
                validacion.push(bandera);                               
                
                if (datos[6]['value'] <=3 || datos[7]['value'] <=3) {
                    if (datos[8]['value'] == "") {                           
                        bandera = 1;
                        $('#9').css("border-color","red");
                        $('#9').focus();
                    }else{
                        bandera = 0;
                    }
                }else if(datos[6]['value'] >=4 || datos[7]['value'] >=4){
                    bandera = 0;
                    $('#9').css("border-color","#ccc");
                }
                validacion.push(bandera);  

                if (datos[9]['value'] <=3 || datos[10]['value'] <=3) {
                    if (datos[11]['value'] == "") {                           
                        bandera = 1;
                        $('#12').css("border-color","red");
                        $('#12').focus();
                    }else{
                        bandera = 0;
                    }
                }else if(datos[9]['value'] >=4 || datos[10]['value'] >=4){
                     bandera = 0;
                    $('#12').css("border-color","#ccc");                    
                }
                validacion.push(bandera);

                if (datos[12]['value'] <=3 || datos[13]['value'] <=3) {
                    if (datos[14]['value'] == "") {                           
                        bandera = 1;
                        $('#15').css("border-color","red");
                        $('#15').focus();
                    }else{
                        bandera = 0;
                    }
                }else if(datos[12]['value'] >=4 || datos[13]['value'] >=4){
                     bandera = 0;
                     bandera = 0;
                    $('#15').css("border-color","#ccc");                    
                }
                validacion.push(bandera);
                

                var indices = [];
                var element = 1;
                var idx = validacion.indexOf(element);
                while (idx != -1) {
                  indices.push(idx);
                  idx = validacion.indexOf(element, idx + 1);
                }

                if (indices.length == 0) {
                    bootbox.confirm({ //Confirmacion de enviar encuesta
                        size: "small",
                        title: 'Enviar Encuesta',
                        message: "¿Deseas continuar?",
                        buttons: {
                            confirm: {
                                label: 'Continuar',
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-secondary'
                            }
                        },
                        callback: function(result){ 
                        /* result is a boolean; true = OK, false = Cancel*/ 
                            if (result === true) {
                                //var datastring = $("#formAcliente").serializeArray();                                 
                                // var ncont = $("#ncont").val();
                                var id_respuesta = $("#id_respuesta").val();

                                // console.log(id_respuesta);
                                // console.log(datos);

                                window.scroll(0, 0);
                                $.ajax({
                                    type: 'POST',
                                    url: './_php/insertEncuesta.php',
                                    dataType: 'json',
                                    data: {id_respuesta:id_respuesta, datos:datos},
                                     complete: function (xhr, textStatus) {
                                        //called when complete
                                    },
                                    success: function(data) {
                                        //console.log(data);

                                        if (data['data'] != "") {
                                            setTimeout(function () { location.reload(1); }, 2000);
                                        }
                                        // bootbox.alert({
                                        //     size: "small",
                                        //     title: "Alerta",
                                        //     message: "<label>"+data['data']+"</label>",
                                        //     callback: function(){
                                        //         //loadClientes();
                                        //        // $('#formNcliente').trigger("reset");
                                        //         setTimeout(function () { location.reload(1); }, 2000);
                                        //     }
                                        // });       
                                    }
                                });//fin ajax                            
                            }
                        }
                        
                    });

                }else{
                    bootbox.alert({
                        size: "small",
                        title: "",
                        message: "<label>Si eligió una de las siguientes opciones: Indiferente, Insatisfecho o Totalmente insatisfecho, por favor escribir su comentario al respecto para completar la encuesta.</label>",
                        callback: function(){
                        }
                    });       
                }

                           
            }else{
                //console.log("elegir una respuesta de todos los campos opcionales");
                 bootbox.alert({
                    size: "small",
                    title: "Alerta",
                    message: "<label>Todos los campos de opción son requeridos</label>",
                    callback: function(){
                    }
                });       
            }
        }
        
    </script>
</html>
