<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 
if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Catalogo de Clientes</title>        

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
        	.card{display: flex;flex-direction: column;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid rgba(0,0,0,.125);border-radius: .25rem;min-height: 250px;}
        	.card-body{flex: 1 1 auto; min-height: 1px; padding: 1.25rem;}
        	.card-img-top{border-top-left-radius: calc(.25rem - 1px); border-top-right-radius: calc(.25rem - 1px);}
        	.card-text{color: #666; margin-bottom: 0;}
        	.control-label{font-size: 12px;font-weight: 300; text-align: center;}
        	.td-alinear{color:#545E6B; padding: 1px 0px 0px 35px;}
            .progress{height: 3px;}
            @media only screen and (max-device-width : 640px) {
            #loadimg{width: 340px;margin: 46% 0 0 -52%;}
            /* Styles */
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

<!--                 <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image"> -->
                </div>
            </nav>

            <div id="page-wrapper">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                                                    <div class="clearfix">&nbsp;</div>                        <div class="clearfix">&nbsp;</div>
   <!--                          <h1 class="page-header" style="color: #AC182D;">Gestionar Catálogos</h1> -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">

                        <div class="col-lg-8">
                           <table id="table_catalogos" class="table table-striped table-bordered" style="width:100%"></table>
                        </div>

                        <div class="col-lg-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Actualizar Cliente
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <form role="form" id="formAcliente">
                                        <div class="form-group">
                                            <label>* Id</label>
                                            <input id="idcl" class="form-control" placeholder="Id de Cliente">
                                        </div>
                                        <div class="form-group">
                                            <label>* Nombre</label>
                                            <input id="nomcl" class="form-control" placeholder="Nombre de Cliente">
                                        </div>
                                        <input type="hidden" name="idSistem" id="idSistem">
                                       <div class="float-right text-right"><button type="button" class="btn btn-primary" onclick="actualizaCliente();">Actualizar</button></div>
                                    </form>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->

                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    Agregar Nuevo Cliente al Catálogo
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <form role="form" id="formNcliente">
                                        <div class="form-group">
                                            <label>* Id</label>
                                            <input id="idcl_new" class="form-control" placeholder="Id de Cliente">
                                        </div>
                                        <div class="form-group">
                                            <label>* Nombre</label>
                                            <input id="nomcl_new" class="form-control" placeholder="Nombre de Cliente">
                                        </div>
                                         <div class="float-right text-right"><button type="button" class="btn btn-success" onclick="nuevoCliente();">Nuevo Cliente</button></div>
                                    </form>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>

    <script>
    

    </script>

        <script type="text/javascript">
        	$(document).ready(function(){
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                });

        	});
            
            function loadClientes(){    //Cargar catalogo de clientes
                 
                var table = $('#table_catalogos').dataTable({
                    dom: 'Bfrtip',
                    bProcessing: true,
                    "scrollY": 440,
                    destroy: true,
                    "scrollX": true,
                    "autoWidth": true,
                     retrieve: true,
                    // scrollCollapse: true,
                    "paging": false,
                    // "ordering": false,
                    "bInfo": false,
                    "language": {
                        "search": "Buscar:"
                    },
                    ajax: {
                        "url":"./_php/listarClientes.php"
                    },
                    columns: [
                        { data: 'identificador',title: 'Id' },
                        { data: 'nombre',title: 'Nombre' },
                        { data: 'opcion',title: 'Editar' }                       
                      ]
                });   
                table.DataTable().ajax.reload();
            }
            
            function getCiente(idCL,ident,nom){ //LLenar formulario para actualizar clientes
               //console.log(ident);
                $('#idSistem').val(idCL);
                $('#idcl').val(ident);
                $('#nomcl').val(nom);
                
            }

            function actualizaCliente(){    //funcion para enviar los clientes 
                var idSistem = $('#idSistem').val();
                if (idSistem == "") { //No se ha seleccionado ningun registro
                    bootbox.alert({
                        size: "small",
                        title: "Alerta",
                        message: "<label>Selecciona un registro a editar</label>",
                        callback: function(){
                            loadClientes(); 
                        }
                    });  
                }else{
                    var idcl_upd = $('#idcl').val();
                    var nomcl_upd = $('#nomcl').val();

                    if (idcl_upd == "" || nomcl_upd =="") {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: '<label class="alert alert-danger">Los siguientes campos no pueden estár vacios.<br>* Id <br>* Nombre</label>',
                            callback: function(){}
                        });       
                    }else{
                        bootbox.confirm({ 
                            size: "small",
                            title: 'Actualizar Cliente',
                            message: "¿Deseas continuar?",
                            buttons: {
                                confirm: {
                                    label: 'Continuar',
                                    className: 'btn-primary'
                                },
                                cancel: {
                                    label: 'Cancelar',
                                    className: 'btn-secondary'
                                }
                            },
                            callback: function(result){ 
                            /* result is a boolean; true = OK, false = Cancel*/ 
                                //console.log(result);
                                if (result === true) {
                                    //var datastring = $("#formAcliente").reset(); 
                                                
                                    $.ajax({
                                        type: 'POST',
                                        url: './_php/updatecatClientes.php',
                                        dataType: 'json',
                                        data: {idcl_upd:idcl_upd ,nomcl_upd:nomcl_upd, idSistem:idSistem },
                                         complete: function (xhr, textStatus) {
                                            //called when complete
                                        },
                                        success: function(data) {
                                            bootbox.alert({
                                                size: "small",
                                                title: "Alerta",
                                                message: "<label>"+data['data']+"</label>",
                                                callback: function(){
                                                    loadClientes(); 
                                                    document.getElementById("formAcliente").reset();
                                                }
                                            });                                    
                                        }
                                    });//fin ajax                             
                                }
                            }
                            
                        });
                    }
                }
 
            }

            function nuevoCliente(){    //funcion para enviar los clientes 

                var idcl_new = $('#idcl_new').val();
                var nomcl_new = $('#nomcl_new').val();

                if (idcl_new == "" || nomcl_new =="") {
                    bootbox.alert({
                        size: "small",
                        title: "Alerta",
                        message: '<label class="alert alert-danger">Los siguientes campos no pueden estár vacios.<br>* Id <br>* Nombre</label>',
                        callback: function(){}
                    });       
                }else{
                    bootbox.confirm({ 
                        size: "small",
                        title: 'Agregar Cliente a Catálogo',
                        message: "¿Deseas continuar?",
                        buttons: {
                            confirm: {
                                label: 'Continuar',
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-secondary'
                            }
                        },
                        callback: function(result){ 
                        /* result is a boolean; true = OK, false = Cancel*/ 
                            //console.log(result);
                            if (result === true) {
                                //var datastring = $("#formAcliente").serializeArray(); 
                                            
                                $.ajax({
                                    type: 'POST',
                                    url: './_php/insertcatClientes.php',
                                    dataType: 'json',
                                    data: {idcl_new:idcl_new ,nomcl_new:nomcl_new },
                                     complete: function (xhr, textStatus) {
                                        //called when complete
                                    },
                                    success: function(data) {
                                        bootbox.alert({
                                            size: "small",
                                            title: "Alerta",
                                            message: "<label>"+data['data']+"</label>",
                                            callback: function(){
                                                loadClientes();
                                                 document.getElementById("formNcliente").reset();
                                            }
                                        });                                    
                                    }
                                });//fin ajax                             
                            }
                        }
                        
                    });
                }
            }

            loadClientes();
        </script>

    </body>
</html>