<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

 	//echo phpinfo();

    $estatus = "";

    $db->setQuery("SELECT c.id as id_contrato, c.num_contrato, c.descripcion as desContrato, cc.nombre as nom_cliente, cc.id as num_cliente, cu.nombre as nom_ubicacion, cu.id as num_ubicacion, cr.id as num_representante, cr.nombre as nom_representante, cd.id as num_division, cd.nombre as nom_division,c.estatus FROM contratos c
LEFT JOIN cat_clientes cc on cc.id = c.id_cliente
LEFT JOIN cat_ubicaciones cu on cu.id = c.id_ubicacion
LEFT JOIN cat_representantes cr on cr.id = c.id_representante
LEFT JOIN cat_division cd on cd.id = c.id_division;"); 
    $rows = $db->loadObjectList();
    
    if($rows){
  		foreach ($rows as $row) {
          
        if ($row->estatus ==0) {
          $opc='<span class="badge badge-secondary">Inactivo</span>';
        }else{
          $opc='<div class="btn-group"> <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-gear"></i> <span class="caret"></span> </button> <ul class="dropdown-menu" role="menu"> <li><a style="cursor:pointer;" onClick="getContrato('.$row->id_contrato.',\''.$row->num_contrato.'\',\''.utf8_encode($row->desContrato).'\','.$row->num_cliente.',\''.$row->num_ubicacion.'\',\''.$row->num_representante.'\',\''.$row->num_division.'\');">Editar</a> </li> <li class="divider"></li> <li><a style="cursor:pointer;" onClick="disaContrato('.$row->id_contrato.');" href="#">Desactivar</a> </li> </ul> </div>';

        }
  			$arr[] = array('id' => $row->id_contrato, 'num_contrato' => utf8_encode($row->num_contrato), 'nom_division' =>$row->nom_division,'desContrato' => utf8_encode($row->desContrato), 'nom_cliente' => utf8_encode($row->nom_cliente), 'nom_ubicacion' => $row->nom_ubicacion,'nom_representante'=> $row->nom_representante,'opcion'=> $opc);


  		}
    }else{
        $arr[] =array('id_contrato' => '0', 'num_contrato' => '0', 'desContrato' =>'0', 'nom_cliente' => '0', 'nom_ubicacion' => '0','nom_representante' => '0');
    }

    $jsondata['data'] = $arr;
    echo json_encode($jsondata);


?>