<?php
	error_reporting(0);
	session_start();  
	if (empty($_SESSION['user'])) {
	   header("location: ./logout/");
	} 
 
  	$idEnc = $_POST['idEnc'];
  	$idSec = $_POST['idSec'];
   	require_once('./db.class.php');
 	
	
  	$db1 = DataBase::connect();	// inicio obtener las opciones de respuesta para cuestionarios
	$db1->setQuery("SELECT id, nombre, valor FROM opciones where id_cuestionario = ".$idEnc." order by id asc");
	$rows1 = $db1->loadObjectList();
	if($rows1){
		foreach ($rows1 as $row1) {			
			$opt[] = array('id_opcion' => $row1->id,'nom_opcion' => utf8_encode($row1->nombre), 'valor' => $row1->valor);
		} // fin obtener las opciones de respuesta para cuestionarios  	
  	}else{
  			$opt[] = array('id_opcion' => 0,'nom_opcion' => 'No hay opciones registradas', 'valor' => 0);
  	}


	$db = DataBase::connect();// inicio obtener las preguntas de los cuestionarios
    $db->setQuery("SELECT c.id as cuest_id, p.id as preg_id, s.id as seccion_id, s.nombre, p.pregunta as preg_nombre FROM cuestionarios c
		inner join preguntas p
		on c.id = p.id_cuestionario 
		inner join secciones s
		on p.id_seccion = s.id
		where c.id = ".$idEnc." and p.id_seccion = ".$idSec." order by p.id asc");
    $rows = $db->loadObjectList();
    if($rows){

		foreach ($rows as $row) {
			$d = DataBase::connect();	//ir por la respuesta de la pregunta si es que tiene algun guardada			
		    $d->setQuery("select id as id_respuesta, id_opcion as id_opcselect from respuestas r where id_cuestionario = ".$idEnc." and id_seccion = ".$idSec." and id_usuario = ".$_SESSION['user']['numero_empleado']." and id_pregunta =".$row->preg_id);
		    $ro = $d->loadObject();		    
			$arr[] = array('preg_id' => $row->preg_id,'preg_nombre' => utf8_encode($row->preg_nombre),'id_respuesta' => $ro->id_opcselect,'opciones' => $opt);
		}
    }else{
    		$arr[] = array('preg_id' => 0,'preg_nombre' => 'No hay preguntas registradas para este cuestionario','id_respuesta' => 0 ,'opciones' => $opt);
    }


    $db_respSec = DataBase::connect(); // inicio obtener respuestas por cuestionario y seccion de cada usuario
    $db_respSec->setQuery("select count(*) as numRespSecc from respuestas where id_cuestionario= ".$idEnc." and id_seccion = ".$idSec." and id_usuario =".$_SESSION['user']['numero_empleado']);
    $rowsRes = $db->loadObject();
	
	$item=sizeof($rows);
	$numeroResp=intval($rowsRes->numRespSecc);
	$porcent = intval((($numeroResp*100))/sizeof($rows));
	
	if ($porcent>=0 && $porcent<=33) {
		$barProgress ='<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: '.$porcent.'%"></div>';
	}elseif ($porcent>33 && $porcent<=99) {
		$barProgress ='<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: '.$porcent.'%"></div>';
	}elseif ($porcent ==100) {
		$barProgress ='<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: '.$porcent.'%"></div>';
	}
	

  	$jsondata['seccion'] = array('idSec' => $idSec,'numeroResp' => $numeroResp,'totalitem' => sizeof($rows));
  	$jsondata['opciones'] = $opt;
  	$jsondata['pregResp'] = $numeroResp ."/". $item; 
    $jsondata['porcentajeSecc'] = $barProgress;
    $jsondata['cuest_id'] = $rows[0]->cuest_id;
    $jsondata['data'] = $arr;
    echo json_encode($jsondata);


?>