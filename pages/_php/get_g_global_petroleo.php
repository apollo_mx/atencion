<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

    $an = $_GET['anio'];
 // $trim = $_GET['trim'];

////////////////////////////////////////////////////////////////////////////Obtener datos globales por 3 años atras

// 11	PEP. SUR
// 2	P.NORTE
// 12	PRODUC DE POZOS
// 6	REMEDIACIONES

    $anios = array('2013-01-15','2014-01-15','2015-01-15','2016-01-15','2017-01-15','2018-01-15','2019-01-15','2020-01-15','2021-01-15','2022-01-15','2023-01-15','2024-01-15','2025-01-15','2026-01-15','2027-01-15','2028-01-15','2029-01-15','2030-01-15');
    $indFin =array_search($an,$anios,true)+1;
    $indIni = $indFin-3;

	$nom_div = "";
	$id_div = "";	
    $id_11 = "PEP. SUR";
    $id_2 = "P.NORTE";
    $id_12 = "PRODUC DE POZOS";
    $id_6 = "REMEDIACIONES";

    foreach ($anios as $key => $value) {
     	//$glob_3[] = $key;
     	
     	if ($key >= $indIni && $key < $indFin) {
     		
     		for ($i=1; $i <=4 ; $i++) {
     			$tot_pepsur = 0;
     			$tot_pnte = 0;
     			$tot_prodpozos = 0;
     			$tot_remedioaciones = 0;
				

				$encabezado[] = array('anio'=>date("Y", strtotime($value)) .' T'.$i);//Crear encabezado
     		
     			//PEP. SUR 11
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (11) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$aditivosarr = $db->loadObjectlist();
				$totalProm =0;

				foreach ($aditivosarr as $aditivos) {
					$tot_pepsur +=$aditivos->respuesta1 + $aditivos->respuesta2 + $aditivos->respuesta4 + $aditivos->respuesta5 + $aditivos->respuesta7 +$aditivos->respuesta8 + $aditivos->respuesta10 + $aditivos->respuesta11 + $aditivos->respuesta13 + $aditivos->respuesta14;
	          		$tot_pepsur = $tot_pepsur/10;
					$totalProm += round(($tot_pepsur/5)*100);
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($aditivosarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyPepsur[] = array('an' =>$value, 'trim' => $i, 'div' => 11,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($aditivosarr));

				//P.NORTE. 2
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (2) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$iindarr = $db->loadObjectlist();
				$totalProm =0;

				foreach ($iindarr as $iind) {
					$tot_pnte +=$iind->respuesta1 + $iind->respuesta2 + $iind->respuesta4 + $iind->respuesta5 + $iind->respuesta7 +$iind->respuesta8 + $iind->respuesta10 + $iind->respuesta11 + $iind->respuesta13 + $iind->respuesta14;
	          		$tot_pnte = $tot_pnte/10;
					$totalProm += round(($tot_pnte/5)*100);	          		
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($iindarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyPnte[] = array('an' =>$value, 'trim' => $i, 'div' => 2,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($iindarr));
	     		
				//PRODUC DE POZOS 12
				$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (12) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$taguasarr = $db->loadObjectlist();
				$totalProm =0;

				foreach ($taguasarr as $taguas) {
					$tot_prodpozos +=$taguas->respuesta1 + $taguas->respuesta2 + $taguas->respuesta4 + $taguas->respuesta5 + $taguas->respuesta7 +$taguas->respuesta8 + $taguas->respuesta10 + $taguas->respuesta11 + $taguas->respuesta13 + $taguas->respuesta14;
		          	$tot_prodpozos = $tot_prodpozos/10;
		          	$totalProm += round(($tot_prodpozos/5)*100);				
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($taguasarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyPpozos[] = array('anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($taguasarr));

				//REMEDIACIONES 6
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (6) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$refinacionarr = $db->loadObjectlist();
				$totalProm =0;

				foreach ($refinacionarr as $refinacion) {
					$tot_remedioaciones +=$refinacion->respuesta1 + $refinacion->respuesta2 + $refinacion->respuesta4 + $refinacion->respuesta5 + $refinacion->respuesta7 +$refinacion->respuesta8 + $refinacion->respuesta10 + $refinacion->respuesta11 + $refinacion->respuesta13 + $refinacion->respuesta14;
		          	$tot_remedioaciones = $tot_remedioaciones/10;	
		          	$totalProm += round(($tot_remedioaciones/5)*100);		          				
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($refinacionarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyRemediaciones[] = array('an' =>$value, 'trim' => $i, 'div' => 6,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($refinacionarr));				
			}

     	}
    }

	$jsondata['encabezado'] = $encabezado;
	$jsondata['body0'] = $bodyPepsur;
	$jsondata['body1'] = $bodyPnte;
	$jsondata['body2'] = $bodyPpozos;
	$jsondata['body3'] = $bodyRemediaciones;	
	
	echo json_encode($jsondata);
	unset($an);

 ?>