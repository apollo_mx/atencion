<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

    $an = $_GET['anio'];
 // $trim = $_GET['trim'];

////////////////////////////////////////////////////////////////////////////Obtener datos globales por 3 años atras

    $anios = array('2013-01-15','2014-01-15','2015-01-15','2016-01-15','2017-01-15','2018-01-15','2019-01-15','2020-01-15','2021-01-15','2022-01-15','2023-01-15','2024-01-15','2025-01-15','2026-01-15','2027-01-15','2028-01-15','2029-01-15','2030-01-15');
    $indFin =array_search($an,$anios,true)+1;
    $indIni = $indFin-3;

	$nom_div = "";
	$id_div = "";	
	$id_8 = "ADITIVOS";	
	$id_9 = "I. IND";	
    $id_4 = "REFINACION";
	$id_10 = "T. AGUAS"; 
    $id_5 = "I. Privada";            
 


    foreach ($anios as $key => $value) {
     	//$glob_3[] = $key;
     	
     	if ($key >= $indIni && $key < $indFin) {
     		
     		for ($i=1; $i <=4 ; $i++) {
     			$tot_aditivos = 0;
     			$tot_iind = 0;
     			$tot_refinacion = 0;
     			$tot_iprivada = 0;
				$tot_iprivada = 0;     			
     			$tot_tAguas = 0;
				

				$encabezado[] = array('anio'=>date("Y", strtotime($value)) .' T'.$i);//Crear encabezado
     		
     			//Aditivos 8
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (8) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$aditii = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($aditii as $aditivos) {
					$tot_aditivos +=$aditivos->respuesta1 + $aditivos->respuesta2 + $aditivos->respuesta4 + $aditivos->respuesta5 + $aditivos->respuesta7 +$aditivos->respuesta8 + $aditivos->respuesta10 + $aditivos->respuesta11 + $aditivos->respuesta13 + $aditivos->respuesta14;
		          	$tot_aditivos = $tot_aditivos/10;
		          	$totalProm += round(($tot_aditivos/5)*100);
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($aditii));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyAditivos[] = array('an' =>$value, 'trim' => $i, 'div' => 8,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($aditii));
	     		//$bodyAditivos[] = array('promedio' => round(($tot_aditivos/5)*100,2));

				//I. Ind. 9
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (9) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$iindarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($iindarr as $iind) {
					$tot_iind +=$iind->respuesta1 + $iind->respuesta2 + $iind->respuesta4 + $iind->respuesta5 + $iind->respuesta7 +$iind->respuesta8 + $iind->respuesta10 + $iind->respuesta11 + $iind->respuesta13 + $iind->respuesta14;
	          		$tot_iind = $tot_iind/10;
	          		$totalProm += round(($tot_iind/5)*100);
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($iindarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyIind[] = array('an' =>$value, 'trim' => $i, 'div' => 9,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($iindarr));				
	     		//$bodyIind[] = array('promedio' => round(($tot_iind/5)*100));
				
				//T Aguas 10
				$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (10) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$taguasarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($taguasarr as $taguas) {
					$tot_tAguas +=$taguas->respuesta1 + $taguas->respuesta2 + $taguas->respuesta4 + $taguas->respuesta5 + $taguas->respuesta7 +$taguas->respuesta8 + $taguas->respuesta10 + $taguas->respuesta11 + $taguas->respuesta13 + $taguas->respuesta14;
		          	$tot_tAguas = $tot_tAguas/10;
		          	$totalProm += round(($tot_tAguas/5)*100);
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($taguasarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyTaguas[] = array('an' =>$value, 'trim' => $i, 'div' => 10,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($taguasarr));	
	     		//$bodyTaguas[] = array('promedio' => round(($tot_tAguas/5)*100));

				//REFINACION 4
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (4) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$refinacionarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($refinacionarr as $refinacion) {
					$tot_refinacion +=$refinacion->respuesta1 + $refinacion->respuesta2 + $refinacion->respuesta4 + $refinacion->respuesta5 + $refinacion->respuesta7 +$refinacion->respuesta8 + $refinacion->respuesta10 + $refinacion->respuesta11 + $refinacion->respuesta13 + $refinacion->respuesta14;
		          	$tot_refinacion = $tot_refinacion/10;
		          	$totalProm += round(($tot_refinacion/5)*100);		          	
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($refinacionarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyRefinacion[] = array('anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($refinacionarr));
	     		//$bodyRefinacion[] = array('promedio' => round(($tot_refinacion/5)*100));

				//I. Privada 5
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (5) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$iprivadaarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($iprivadaarr as $iprivada) {
					$tot_iprivada +=$iprivada->respuesta1 + $iprivada->respuesta2 + $iprivada->respuesta4 + $iprivada->respuesta5 + $iprivada->respuesta7 +$iprivada->respuesta8 + $iprivada->respuesta10 + $iprivada->respuesta11 + $iprivada->respuesta13 + $iprivada->respuesta14;
					$tot_iprivada = $tot_iprivada/10;
		          	$totalProm += round(($tot_iprivada/5)*100);						
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($iprivadaarr));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyIprivada[] = array('an' =>$value, 'trim' => $i, 'div' => 5,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($iprivadaarr));
	     		//$bodyIprivada[] = array('promedio' => round(($tot_iprivada/5)*100,2));
			}

     	}
    }

    
	$jsondata['encabezado'] = $encabezado;
	$jsondata['body0'] = $bodyAditivos;
	$jsondata['body1'] = $bodyIind;
	$jsondata['body2'] = $bodyRefinacion;
	$jsondata['body3'] = $bodyTaguas;	
	$jsondata['body4'] = $bodyIprivada;
	
	echo json_encode($jsondata);
	unset($an);

 ?>