
<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

  $db->setQuery("select respuesta15 as peticion from respuestas where id =".$id_respuesta); 
  $seg_15 = $db->loadObject();

  $db->setQuery("select * from history_seguimiento where folio = '".$seguimiento_general->folio."' and num_pregunta = 15 order by post desc;");
  $history_15 = $db->loadObjectlist();

  //ID de tab seguimiento que se está trabajando
  $db->setQuery("select id as id_detalle,estatus from detalle_seguimiento where folio_seguimiento = '".$folio_seguimiento."' and num_pregunta = 15 and cod_empleado = '".$codEmpleadoResponde."';");
  $reg = $db->loadObject();



?>
<div class="col-md-12">
  <div class="col-md-4">
    <h3>Comentario:</h3>
    <span><?php echo $seg_15->peticion; ?> </span>
  </div>                                    
  <div class="col-md-6">
    <h3>Colaboradores:</h3>

    <?php foreach ($seguimiento_detalle as $key => $value): ?> 
    <?php if ($value->num_pregunta == 15): ?>

      <div class="row">
        <?php echo getName($value->cod_empleado,$value->estatus); ?>
        <?php if ($page=="back" && $value->estatus == "validacion"): ?>
          <div class="col-lg-2">
            <div class="pull-right">
              <div class="btn-group open">
                  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                      Aprobar
                      <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                      <li><a href="#" onclick="aprobarAsignacion('cerrado',<?php echo $value->id; ?>)">SI</a>
                      </li>
                      <li><a href="#" onclick="aprobarAsignacion('en proceso',<?php echo $value->id; ?>)">NO</a>
                      </li>
                  </ul>
              </div>
            </div>  
          </div>
        <?php endif ?>
      </div>

    <?php endif ?>
    <?php endforeach ?>
  </div>
<?php if ($folio_seguimiento): ?>
  <?php if ($reg->estatus != "cerrado"): ?>
    <div class="col-md-1">
      <div class="btn-group">
        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-gear"></i> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="#" onclick="validarAsignacion(<?php echo $reg->id_detalle; ?>);"><i class="fa fa-check-circle fa-fw"></i> Enviar a Validación</a>
            </li>
            <li><a href="#" onclick="solicitarReasignacion(<?php echo $reg->id_detalle; ?>);"><i class="fa fa-bell-o fa-fw"></i>Solicitar Reasignación</a>
            </li>
        </ul>
      </div>
    </div>  
  <?php endif ?>
<?php endif ?>
  <!-- Historico detalle -->
  <div class="row">
    <div class="col-md-12">
      <?php foreach ($history_15 as $key => $value): ?>
          <div class="col-lg-2">
                        
            <span class="autor"><?php echo getNameabreb($value->cod_emp_responde); ?></span>
          </div>        
          <div class="col-md-10">
                
            <?php 
              if($value->url_file){
                //print $value->url_file;
                print '<a href="./backend/'.$value->url_file.'" download><img src="./img/doc.png" alt="descargar" width="70"></a>';
              }else{
                print '<p class="recived_msg"><small>'.$value->texto_seguimiento.'</small></p>';
              }
            ?>
            <span class="time_date"><?php print $value->post; ?></span>    
          </div>
          
      <?php endforeach ?>
    </div>
  </div>

  <!-- fin Historico detalle -->  
    <?php if ($reg->estatus != "cerrado"): ?>
      <?php if ($seguimiento_general->estatus!=="CERRADO"): ?>
      <div class="col-md-12"> <hr></div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="nota_imag_tab">Nota, requiere mínimo 10 caracteres.</label>
          <textarea class="form-control" id="nota_imag_tab" rows="3"></textarea>
        </div>

        <button type="button" class="btn btn-primary" onclick="sendNota('nota_imag_tab',15,<?php echo $reg->id_detalle; ?>);">Enviar</button>    
      </div>

      <div class="row">
        <div class="col-md-6 col-sm-12">
          
          <!-- Our markup, the important part here! -->
          <div id="drag-and-drop-zone-imag" class="dm-uploader p-5">
            <h3 class="mb-5 mt-5 text-muted">Arrastra y suelta archivos aquí</h3>

            <div class="btn btn-primary btn-block mb-5">
                <span>Abrir navegador de archivos</span>
                <input type="file" title='Click to add Files' />
            </div>
          </div><!-- /uploader -->

        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card h-100">
            <div class="card-header">
              Lista de archivos
            </div>

            <ul class="list-unstyled p-2 d-flex flex-column col" id="files-imag">
              <li class="text-muted text-center empty">No se cargaron archivos.</li>
            </ul>
          </div>
        </div>

      </div>
      <!-- /file list --> 
      <?php endif ?>     
    <?php endif ?>
      
</div>  