<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

 	//echo phpinfo();

    $estatus = "";

    $db->setQuery("SELECT * FROM cat_clientes where estatus = 1;"); 
    $rows = $db->loadObjectList();
    
    if($rows){
		foreach ($rows as $row) {

			  $arr[] = array('id' => $row->id, 'identificador' => $row->identificador,'nombre' => utf8_encode($row->nombre), 'opcion' =>'<button type="button" class="btn btn-outline btn-primary btn-sm" onClick="getCiente('.$row->id.',\''.$row->identificador.'\',\''.utf8_encode($row->nombre).'\');"><i class="fa fa-edit"></i></button>');
		}
    }else{
        $arr[] = array('id' => '0','identificador' => '0', 'nombre' => 'No hay clientes registrados');
    }

    $jsondata['data'] = $arr;
    echo json_encode($jsondata);


?>