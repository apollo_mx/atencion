<?php
	error_reporting(0);
	session_start();  
	if (empty($_SESSION['user'])) {
	   header("location: ./logout/");
	} 
 
  	$idQuiestion = $_POST['idQuiestion'];

   	require_once('./db.class.php');
 	
	
	$db = DataBase::connect();// inicio obtener las preguntas de los cuestionarios
    $db->setQuery("SELECT count(*) as numero, DATE_FORMAT(fecha_inicio, '%Y-%m-%d')as fecha_ini FROM estatus_encuestas where id_cuestionario =".$idQuiestion." group by (fecha_ini)");
    $rows = $db->loadObjectList();
    if($rows){
		foreach ($rows as $row) {
			$arr[] = array('fecha_inicio' => $row->fecha_ini,'value' => $row->numero);
		}
    }else{
			$arr[] = array('fecha_inicio' => '00-00-0000','value' => 0);
    }

	$db3 = DataBase::connect();// numero total de encuestas no finalizadas
    $db3->setQuery("SELECT count(*)as numero FROM estatus_encuestas where ".$idQuiestion." and fecha_termino is null");
    $rows3 = $db3->loadObjectList();
    if($rows3){
		foreach ($rows3 as $row) {
			$arr3[] = $row->numero;
		}
    }else{
			$arr3[] = 0;
    }

	$db2 = DataBase::connect();// numero total de encuestas finalizadas
    $db2->setQuery("SELECT count(*)as numero FROM estatus_encuestas where ".$idQuiestion." and fecha_termino is not null");
    $rows2 = $db2->loadObjectList();
    if($rows2){
		foreach ($rows2 as $row) {
			$arr2[] = $row->numero;
		}
    }else{
			$arr2[] = 0;
    }

	$db1 = DataBase::connect();// numero total de encuestas
    $db1->setQuery("SELECT count(*)as numero FROM estatus_encuestas where ".$idQuiestion);
    $rows1 = $db1->loadObjectList();
    if($rows1){
		foreach ($rows1 as $row) {
			$arr1[] = $row->numero;
		}
    }else{
			$arr1[] = 0;
    }


	$db1 = DataBase::connect();// numero total de encuestas
    $db1->setQuery("SELECT count(*)as numero FROM estatus_encuestas where ".$idQuiestion);
    $rows1 = $db1->loadObjectList();
    if($rows1){
		foreach ($rows1 as $row) {
			$arr1[] = $row->numero;
		}
    }else{
			$arr1[] = 0;
    }

	$db4 = DataBase::connect();// numero total de encuestas
    $db4->setQuery("SELECT count(e.id) as numero, e.id_cuestionario ,em.nombre FROM estatus_encuestas e 
		INNER JOIN usuarios u ON 
		e.id_usuario = u.numero_empleado
		INNER JOIN empresa em ON
		em.id = u.id_empresa WHERE e.id_cuestionario =".$idQuiestion." GROUP BY u.id_empresa order by numero desc");
    $rows4 = $db4->loadObjectList();
    if($rows4){
		foreach ($rows4 as $row) {
			$arr4[] = array('label' => utf8_encode($row->nombre),'value' => $row->numero);
		}
    }else{
			$arr4[] = array('label' => 0,'value' => 'NINGUNO');
    }    

	$jsondata['totales'] = $arr1;
	$jsondata['finalizados'] = $arr2;
	$jsondata['no_finalizados'] = $arr3;
    $jsondata['allquest'] = $arr;
	$jsondata['por_empresa'] = $arr4;    
    echo json_encode($jsondata);


?>