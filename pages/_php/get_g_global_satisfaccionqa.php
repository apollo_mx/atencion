<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

    $an = $_GET['anio'];
 	// $trim = $_GET['trim'];

////////////////////////////////////////////////////////////////////////////Obtener datos globales por 3 años atras

    $anios = array('2013-01-15','2014-01-15','2015-01-15','2016-01-15','2017-01-15','2018-01-15','2019-01-15','2020-01-15','2021-01-15','2022-01-15','2023-01-15','2024-01-15','2025-01-15','2026-01-15','2027-01-15','2028-01-15','2029-01-15','2030-01-15');
    $indFin =array_search($an,$anios,true)+1;
    $indIni = $indFin-3;        
 
	foreach ($anios as $key => $value) {
		unset($trimestres_anios);
		if ($key >= $indIni && $key < $indFin) {
			// $i = 4;
     		
			for ($i=1; $i <=4; $i++) {
     		    // unset($bodyAditivos);
     		    // unset($newAdit);

     		    $tot_aditivos = 0;
     			$tot_iind = 0;
     			$tot_refinacion = 0;
     			$tot_iprivada = 0;    			
     			$tot_tAguas = 0;
				$tot_ductos = 0;
				$tot_duba = 0;
				$tot_trazadora = 0;
				$tot_pepsur = 0;
				$tot_pnte = 0;
				$tot_prodpozos = 0;
				$tot_remedioaciones = 0;
	

				//DIRECCION INDUSTRIAL
     		
     			//Aditivos 8
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (8) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$aditii = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($aditii as $aditivos) {
					$tot_aditivos +=$aditivos->respuesta1 + $aditivos->respuesta2 + $aditivos->respuesta4 + $aditivos->respuesta5 + $aditivos->respuesta7 +$aditivos->respuesta8 + $aditivos->respuesta10 + $aditivos->respuesta11 + $aditivos->respuesta13 + $aditivos->respuesta14;
		          	$tot_aditivos = $tot_aditivos/10;
		          	$totalProm += round(($tot_aditivos/5)*100);
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($aditii));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'Aditivos','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($aditii));

				$newTot =0;
		     	if(sizeof($aditii)>=1) {	
		     		$newTot = ($tot/100) * sizeof($aditii);
		     		$trimestres_anios[] = array('trim' => $i, 'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($aditii));
		     	}
		     	$totalProm =0;
		     	$newTot =0;

				//I. Ind. 9
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (9) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$iindarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($iindarr as $iind) {
					$tot_iind +=$iind->respuesta1 + $iind->respuesta2 + $iind->respuesta4 + $iind->respuesta5 + $iind->respuesta7 +$iind->respuesta8 + $iind->respuesta10 + $iind->respuesta11 + $iind->respuesta13 + $iind->respuesta14;
	          		$tot_iind = $tot_iind/10;
	          		$totalProm += round(($tot_iind/5)*100);
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($iindarr));
				}else{
					$tot =($tot_iind/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'I. Ind','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($iindarr));				

				$newTot =0;
		     	if(sizeof($iindarr)>=1) {	
		     		$newTot = ($tot/100) * sizeof($iindarr);
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($iindarr));
		     	}
		     	$totalProm =0;
		     	$newTot =0;		     	
				//T Aguas 10
				$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (10) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$taguasarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($taguasarr as $taguas) {
					$tot_tAguas +=$taguas->respuesta1 + $taguas->respuesta2 + $taguas->respuesta4 + $taguas->respuesta5 + $taguas->respuesta7 +$taguas->respuesta8 + $taguas->respuesta10 + $taguas->respuesta11 + $taguas->respuesta13 + $taguas->respuesta14;
		          	$tot_tAguas = $tot_tAguas/10;
		          	$totalProm += round(($tot_tAguas/5)*100);
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($taguasarr));
				}else{
					$tot =($tot_tAguas/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'T Aguas','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($taguasarr));	

				$newTot =0;
		     	if(sizeof($taguasarr)>=1) {	
		     		$newTot = ($tot/100) * sizeof($taguasarr);
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($taguasarr));
		     	}
		     	$totalProm =0;
		     	$newTot =0;
				//REFINACION 4
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (4) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$refinacionarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($refinacionarr as $refinacion) {
					$tot_refinacion +=$refinacion->respuesta1 + $refinacion->respuesta2 + $refinacion->respuesta4 + $refinacion->respuesta5 + $refinacion->respuesta7 +$refinacion->respuesta8 + $refinacion->respuesta10 + $refinacion->respuesta11 + $refinacion->respuesta13 + $refinacion->respuesta14;
		          	$tot_refinacion = $tot_refinacion/10;
		          	$totalProm += round(($tot_refinacion/5)*100);		          	
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($refinacionarr));
				}else{
					$tot =($tot_refinacion/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'I. Refinacion','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($refinacionarr));

				$newTot =0;
		     	if(sizeof($refinacionarr)>=1) {	
		     		$newTot = ($tot/100) * sizeof($refinacionarr);		     		
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($refinacionarr));
		     	}
		     	$totalProm =0;
				$newTot =0;

				//I. Privada 5
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (5) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$iprivadaarr = $db->loadObjectlist();
	     		$totalProm =0;
				foreach ($iprivadaarr as $iprivada) {
					$tot_iprivada +=$iprivada->respuesta1 + $iprivada->respuesta2 + $iprivada->respuesta4 + $iprivada->respuesta5 + $iprivada->respuesta7 +$iprivada->respuesta8 + $iprivada->respuesta10 + $iprivada->respuesta11 + $iprivada->respuesta13 + $iprivada->respuesta14;
					$tot_iprivada = $tot_iprivada/10;
		          	$totalProm += round(($tot_iprivada/5)*100);						
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($iprivadaarr));
				}else{
					$tot =($tot_iprivada/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'I. Privada','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($iprivadaarr));

				$newTot =0;
		     	if(sizeof($iprivadaarr)>=1) {	
		     		$newTot = ($tot/100) * sizeof($iprivadaarr);		     		
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($iprivadaarr));
		     	}
		     	$totalProm =0;
		     	$newTot =0;

		     	// DIRECCION DUCTOS
				//DUCTOS 8
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (3) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$ductoss = $db->loadObjectlist();
	     		
				$totalProm =0;
				foreach ($ductoss as $ductos) {
					$tot_ductos +=$ductos->respuesta1 + $ductos->respuesta2 + $ductos->respuesta4 + $ductos->respuesta5 + $ductos->respuesta7 +$ductos->respuesta8 + $ductos->respuesta10 + $ductos->respuesta11 + $ductos->respuesta13 + $ductos->respuesta14;
					$tot_ductos = $tot_ductos/10;
					$totalProm += round(($tot_ductos/5)*100);
		     	}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($ductoss));
				}else{
					$tot =($tot_ductos/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'Ductos','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($ductoss));

				$newTot =0;
		     	if(sizeof($ductoss)>=1) {	
		     		$newTot = ($tot/100) * sizeof($ductoss);		     		
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($ductoss));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

				//DUBA. 9
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (1) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$dubii = $db->loadObjectlist();
     		
				$totalProm =0;
				foreach ($dubii as $duva) {
					$tot_duba +=$duva->respuesta1 + $duva->respuesta2 + $duva->respuesta4 + $duva->respuesta5 + $duva->respuesta7 +$duva->respuesta8 + $duva->respuesta10 + $duva->respuesta11 + $duva->respuesta13 + $duva->respuesta14;
		          	$tot_duba = $tot_duba/10;
		     		$totalProm += round(($tot_duba/5)*100); 
		     		//$tot_duba=0;
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($dubii));
				}else{
					$tot =($tot_duba/5)*100;
				}
				$bodyAditivos[] = array('div' => 'Duba','anio'=>date("Y", strtotime($value)) .' T'.$i,'promedio' => sprintf('%.2f', $tot), 'number_ittem' => sizeof($dubii));

				$newTot =0;
		     	if(sizeof($dubii)>=1) {	
		     		$newTot = ($tot/100) * sizeof($dubii);	
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($dubii));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

				//TRAZADORES 10
				$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (7) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$trazadores = $db->loadObjectlist();
	     		
				$totalProm =0;
				foreach ($trazadores as $trazado) {
					$tot_trazadora +=$trazado->respuesta1 + $trazado->respuesta2 + $trazado->respuesta4 + $trazado->respuesta5 + $trazado->respuesta7 +$trazado->respuesta8 + $trazado->respuesta10 + $trazado->respuesta11 + $trazado->respuesta13 + $trazado->respuesta14;
		          	$tot_trazadora = $tot_trazadora/10;	
		          	$totalProm += round(($tot_trazadora/5)*100); 
	     		}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($trazadores));
				}else{
					$tot =($tot_trazadora/5)*100;
				}

	     		$bodyAditivos[] = array('div' => 'Trazadores','anio'=>date("Y", strtotime($value)) .' T'.$i,'promedio' => sprintf('%.2f', $tot), 'number_ittem' => sizeof($trazadores));

				$newTot =0;
		     	if(sizeof($trazadores)>=1) {	
		     		$newTot = ($tot/100) * sizeof($trazadores);	
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($trazadores));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

	     		//DIRECCION PETROLEO
				//PEP. SUR 11
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (11) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$pepsur = $db->loadObjectlist();
				$totalProm =0;

				foreach ($pepsur as $psur) {
					$tot_pepsur +=$psur->respuesta1 + $psur->respuesta2 + $psur->respuesta4 + $psur->respuesta5 + $psur->respuesta7 +$psur->respuesta8 + $psur->respuesta10 + $psur->respuesta11 + $psur->respuesta13 + $psur->respuesta14;
	          		$tot_pepsur = $tot_pepsur/10;
					$totalProm += round(($tot_pepsur/5)*100);
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($pepsur));
				}else{
					$tot =($tot_pepsur/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'PEP. SUR','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($pepsur));

				$newTot =0;
		     	if(sizeof($pepsur)>=1) {	
		     		$newTot = ($tot/100) * sizeof($pepsur);
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($pepsur));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

				//P.NORTE. 2
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (2) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$pnte = $db->loadObjectlist();
				$totalProm =0;

				foreach ($pnte as $pnt) {
					$tot_pnte +=$pnt->respuesta1 + $pnt->respuesta2 + $pnt->respuesta4 + $pnt->respuesta5 + $pnt->respuesta7 +$pnt->respuesta8 + $pnt->respuesta10 + $pnt->respuesta11 + $pnt->respuesta13 + $pnt->respuesta14;
	          		$tot_pnte = $tot_pnte/10;
					$totalProm += round(($tot_pnte/5)*100);	          		
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($pnte));
				}else{
					$tot =($tot_pnte/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'P.Nte','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($pnte));

				$newTot =0;
		     	if(sizeof($pnte)>=1) {	
		     		$newTot = ($tot/100) * sizeof($pnte);
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($pnte));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

				//PRODUC DE POZOS 12
				$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (12) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$ppozo = $db->loadObjectlist();
				$totalProm =0;

				foreach ($ppozo as $podoz) {
					$tot_prodpozos +=$podoz->respuesta1 + $podoz->respuesta2 + $podoz->respuesta4 + $podoz->respuesta5 + $podoz->respuesta7 +$podoz->respuesta8 + $podoz->respuesta10 + $podoz->respuesta11 + $podoz->respuesta13 + $podoz->respuesta14;
		          	$tot_prodpozos = $tot_prodpozos/10;
		          	$totalProm += round(($tot_prodpozos/5)*100);				
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($ppozo));
				}else{
					$tot =($tot_prodpozos/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'Produc. de Pozos','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($ppozo));

				$newTot =0;
		     	if(sizeof($ppozo)>=1) {	
		     		$newTot = ($tot/100) * sizeof($ppozo);
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($ppozo));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

				//REMEDIACIONES 6
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (6) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$remedia = $db->loadObjectlist();
				$totalProm =0;

				foreach ($remedia as $remed) {
					$tot_remedioaciones +=$remed->respuesta1 + $remed->respuesta2 + $remed->respuesta4 + $remed->respuesta5 + $remed->respuesta7 +$remed->respuesta8 + $remed->respuesta10 + $remed->respuesta11 + $remed->respuesta13 + $remed->respuesta14;
		          	$tot_remedioaciones = $tot_remedioaciones/10;	
		          	$totalProm += round(($tot_remedioaciones/5)*100);		          				
				}
	     		$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($remedia));
				}else{
					$tot =($tot_remedioaciones/5)*100;
				}
		     	$bodyAditivos[] = array('div' => 'Remediaciones','anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($remedia));				     		

				$newTot =0;
		     	if(sizeof($remedia)>=1) {	
		     		$newTot = ($tot/100) * sizeof($remedia);
		     		$trimestres_anios[] = array('trim' => $i,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'actual_promedio' => sprintf('%.2f', $tot),'new_promedio' => $newTot,'number_ittem' => sizeof($remedia));
		     	}
	     		$totalProm =0;
		     	$newTot =0;

	     		//***************************************************//
     		   
    //  		    $tot_aditivos = 0;
    //  			$tot_iind = 0;
    //  			$tot_refinacion = 0;
    //  			$tot_iprivada = 0;    			
    //  			$tot_tAguas = 0;
				// $tot_ductos = 0;
				// $tot_duba = 0;
				// $tot_trazadora = 0;
				// $tot_pepsur = 0;
				// $tot_pnte = 0;
				// $tot_prodpozos = 0;
				// $tot_remedioaciones = 0;
			}	//FIN FOR PARA LOS 4 TRIMESTRES
			
			if (sizeof($trimestres_anios)>=1) {
	     		foreach ($trimestres_anios as $keyy) {
	     			
	     			if ($keyy['trim'] == 1) {
		     			$totales +=$keyy['new_promedio'];
		     			$trimms = $keyy['trim'];
		     			$num_ittemm += $keyy['number_ittem'];
	     			}

	     			if ($keyy['trim'] == 2) {
		     			$totales2 +=$keyy['new_promedio'];
		     			$trimms2 = $keyy['trim'];
		     			$num_ittemm2 += $keyy['number_ittem'];
	     			}
	     			if ($keyy['trim'] == 3) {
		     			$totales3 +=$keyy['new_promedio'];
		     			$trimms3 = $keyy['trim'];
		     			$num_ittemm3 += $keyy['number_ittem'];
	     			}

	     			if ($keyy['trim'] == 4) {
		     			$totales4 +=$keyy['new_promedio'];
		     			$trimms4 = $keyy['trim'];
		     			$num_ittemm4 += $keyy['number_ittem'];
	     			}

		     	}

		     	if ($num_ittemm>=1) {
				    $subt = sprintf('%.2f', $totales)/$num_ittemm;
		     	}else{
		     		$subt = 0;
		     		$trimms = 1;
		     	}
		     	if ($num_ittemm2>=1) {
					$subt2 = sprintf('%.2f', $totales2)/$num_ittemm2;
		     	}else{
		     		$subt2 = 0;
		     		$trimms2 = 2;		     		
		     	}
		     	if ($num_ittemm3>=2) {
					$subt3 = sprintf('%.2f', $totales3)/$num_ittemm3;
		     	}else{
		     		$subt3 = 0;
		     		$trimms3 = 3;		     		
		     	}
		     	if ($num_ittemm4>=2) {
					$subt4 = sprintf('%.2f', $totales4)/$num_ittemm4;
		     	}else{
		     		$subt4 = 0;
		     		$trimms4 = 4;		     		
		     	}


				$newAdit[] = array('antrim' =>date("Y", strtotime($value)) .' T'.$trimms,'new_promedio' => sprintf('%.2f', $totales), 'num_ittemm' => $num_ittemm, 'tot_prom' => sprintf('%.2f', $subt * 100));

				$newAdit[] = array('antrim' =>date("Y", strtotime($value)) .' T'.$trimms2,'new_promedio' => sprintf('%.2f', $totales2), 'num_ittemm' => $num_ittemm2, 'tot_prom' => sprintf('%.2f', $subt2 * 100));
				
				$newAdit[] = array('antrim' =>date("Y", strtotime($value)) .' T'.$trimms3,'new_promedio' => sprintf('%.2f', $totales3), 'num_ittemm' => $num_ittemm3, 'tot_prom' => sprintf('%.2f', $subt3 * 100));

				$newAdit[] = array('antrim' =>date("Y", strtotime($value)) .' T'.$trimms4,'new_promedio' => sprintf('%.2f', $totales4), 'num_ittemm' => $num_ittemm4, 'tot_prom' => sprintf('%.2f', $subt4 * 100));										

				$totales =0;
				$trimms =0;
				$num_ittemm =0;
				$subt =0;

				$totales2 =0;
				$trimms2 =0;
				$num_ittemm2=0;
				$subt2 =0;

				$totales3 =0;
				$trimms3 =0;
				$num_ittemm3=0;
				$subt3 =0;

				$totales4 =0;
				$trimms4 =0;
				$num_ittemm4=0;
				$subt4 =0;

     		}else{
     			$newAdit[] = array('antrim' =>date("Y", strtotime($value)) .' T 1 - 4','new_promedio' => sprintf('%.2f', 0), 'num_ittemm' => 0, 'tot_prom' => sprintf('%.2f', 0));
     		}
     	}
    } 	

///$newAdit[] = array('antrim' =>date("Y", strtotime($value)) .' T'.$i,'new_promedio' => sprintf('%.2f', 0), 'num_ittemm' => 0, 'tot_prom' => sprintf('%.2f', 0));	

	// $jsondata['body0'] = $bodyAditivos;
	// $jsondata['body1'] = $trimestres_anios;
	$jsondata['body0'] = $newAdit;
	

	echo json_encode($jsondata);
	unset($an);
	//unset($newAdit);
 ?>