<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

    $an = $_GET['anio'];
 // $trim = $_GET['trim'];

////////////////////////////////////////////////////////////////////////////Obtener datos globales por 3 años atras

    $anios = array('2013-01-15','2014-01-15','2015-01-15','2016-01-15','2017-01-15','2018-01-15','2019-01-15','2020-01-15','2021-01-15','2022-01-15','2023-01-15','2024-01-15','2025-01-15','2026-01-15','2027-01-15','2028-01-15','2029-01-15','2030-01-15');
    $indFin =array_search($an,$anios,true)+1;
    $indIni = $indFin-3;

	$nom_div = "";
	$id_div = "";	
    $id_3 = "Ductos";
    $id_1 = "Duva";
    $id_7 = "Trazadora";
    


    foreach ($anios as $key => $value) {
     	//$glob_3[] = $key;
     	
     	if ($key >= $indIni && $key < $indFin) {
     		
     		for ($i=1; $i <=4 ; $i++) {
     			$tot_aditivos = 0;
     			$tot_iind = 0;
     			$tot_refinacion = 0;
     			$tot_iprivada = 0;
				$tot_iprivada = 0;     			
     			$tot_tAguas = 0;
				

				$encabezado[] = array('anio'=>date("Y", strtotime($value)) .' T'.$i);//Crear encabezado
     		
     			//DUCTOS 8
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (3) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$aditii = $db->loadObjectlist();
	     		
				$totalProm =0;
				foreach ($aditii as $aditivos) {
					$tot_aditivos +=$aditivos->respuesta1 + $aditivos->respuesta2 + $aditivos->respuesta4 + $aditivos->respuesta5 + $aditivos->respuesta7 +$aditivos->respuesta8 + $aditivos->respuesta10 + $aditivos->respuesta11 + $aditivos->respuesta13 + $aditivos->respuesta14;
					$tot_aditivos = $tot_aditivos/10;
					$totalProm += round(($tot_aditivos/5)*100);
		     	}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($aditii));
				}else{
					$tot =($tot_aditivos/5)*100;
				}
		     	$bodyDuctos[] = array('an' =>$value, 'trim' => $i, 'div' => 3,'anio'=>date("Y", strtotime($value)) .' T'.$i, 'promedio' => sprintf('%.2f', $tot),'number_ittem' => sizeof($aditii));

				//DUVA. 9
	     		$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (1) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$iind = $db->loadObjectlist();
     		
				$totalProm =0;
				foreach ($iind as $duva) {
					$tot_iind +=$duva->respuesta1 + $duva->respuesta2 + $duva->respuesta4 + $duva->respuesta5 + $duva->respuesta7 +$duva->respuesta8 + $duva->respuesta10 + $duva->respuesta11 + $duva->respuesta13 + $duva->respuesta14;
		          	$tot_iind = $tot_iind/10;
		     		$totalProm += round(($tot_iind/5)*100); 
		     		//$tot_iind=0;
				}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($iind));
				}else{
					$tot =($tot_iind/5)*100;
				}
				$bodyDuva[] = array('an' =>$value, 'trim' => $i, 'div' => 1,'anio'=>date("Y", strtotime($value)) .' T'.$i,'promedio' => sprintf('%.2f', $tot), 'number_ittem' => sizeof($iind));

				//TRAZADORA 10
				$db->setQuery("select r.id as id_respuesta, cd.id as id_div, cd.nombre as nom_div, r.trimestre, r.estatus, r.respuesta1 , r.respuesta2 , r.respuesta4, r.respuesta5, r.respuesta7, r.respuesta8 ,  r.respuesta10 , r.respuesta11 , r.respuesta13, r.respuesta14 from respuestas r 
						inner join contratos c
						on c.id = r.id_contrato
						inner join cat_division cd 
						on cd.id = c.id_division where cd.id in (7) and r.anio_trimestre = '".$value."' and r.trimestre = ".$i." and r.estatus = 'P';"); 
				$tiiaguas = $db->loadObjectlist();
	     		
				$totalProm =0;
				foreach ($tiiaguas as $taguas) {
					$tot_tAguas +=$taguas->respuesta1 + $taguas->respuesta2 + $taguas->respuesta4 + $taguas->respuesta5 + $taguas->respuesta7 +$taguas->respuesta8 + $taguas->respuesta10 + $taguas->respuesta11 + $taguas->respuesta13 + $taguas->respuesta14;
		          	$tot_tAguas = $tot_tAguas/10;
		          	$totalProm += round(($tot_iind/5)*100); 	
	     		}
				$tot = 0;
				if ($totalProm>0) {
					$tot =($totalProm/sizeof($tiiaguas));
				}else{
					$tot =($tot_tAguas/5)*100;
				}

	     		$bodyTrazadora[] = array('an' =>$value, 'trim' => $i, 'div' => 7,'anio'=>date("Y", strtotime($value)) .' T'.$i,'promedio' => sprintf('%.2f', $tot), 'number_ittem' => sizeof($tiiaguas));
	     		//$bodyTrazadora[] = array('promedio' => round(($tot_tAguas/5)*100));

			}

     	}
    }

    
	$jsondata['encabezado'] = $encabezado;
	$jsondata['body0'] = $bodyDuctos;
	$jsondata['body1'] = $bodyDuva;
	$jsondata['body2'] = $bodyTrazadora;
	
	echo json_encode($jsondata);
	unset($an);

 ?>