<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 		$db = DataBase::connect();

 		$an = $_GET['anio'];
 		//Consultar datos por año 
		$db->setQuery("select count(seg.estatus) as tot, seg.estatus  from seguimiento as seg where id_respuesta in(select id from respuestas where estatus = 'P' and anio_trimestre = '".$an."') group by seg.estatus"); 
		
		$registros = $db->loadObjectlist();

 		//Consultar datos por seccion y folios e estatus en proceso
		$db->setQuery("select count(s.id)as tot, cd.nombre,cd.id as id_cdiv , s.estatus, r.id as id_resp
			from respuestas r 
			inner join seguimiento s on s.id_respuesta = r.id
			inner join contratos c on c.id = r.id_contrato 
			inner join cat_division cd on cd.id = c.id_division
			where r.anio_trimestre = '".$an."' and r.estatus = 'P' and s.estatus ='EN PROCESO' group by id_cdiv;"); 

		$divisi = $db->loadObjectlist();

 		//arreglo de secciones preguntas
		$regSection = array('respuesta3', 'respuesta6', 'respuesta9', 'respuesta12', 'respuesta15');
		
		foreach ($regSection as $reg) { 
			$seccion = "";
			if ($reg=="respuesta3") {
				$seccion = "Servicio";
			}
			if ($reg=="respuesta6") {
				$seccion = "Equipos e instalaciones";
			}
			if ($reg=="respuesta9") {
				$seccion = "Factor humano";
			}			
			if ($reg=="respuesta12") {
				$seccion = "Gestión";
			}			
			if ($reg=="respuesta15") {
				$seccion = "Imagen";
			}									
			$db->setQuery("select count(s.id) as tot, cd.nombre, s.estatus
				from respuestas r 
				inner join seguimiento s on s.id_respuesta = r.id
				LEFT JOIN contratos c on c.id = r.id_contrato 
				LEFT JOIN cat_division cd on cd.id = c.id_division
				where r.anio_trimestre = '".$an."' and ".$reg." != '' and r.estatus = 'P' and s.estatus ='EN PROCESO' group by cd.nombre;"); 
				$dato = $db->loadObject();
				if (sizeof($dato)==1) {
					$topicos[] = array('label'=>$seccion ,'value' => $dato->tot, 'nombre' =>$dato->nombre, 'estatus' =>$dato->estatus);
				}else{
					$topicos[] = array('label'=>$seccion ,'value' => 0, 'nombre' => 'NINGUNO', 'estatus' =>'EN PROCESO');					
				}

	 	}


	// $jsondata['data'] = $rows;
	$jsondata['anio'] = array("anio"=>$an);	
	$jsondata['nivel_a_pet'] = $registros;
	$jsondata['nivel_a_division'] = $divisi;
	$jsondata['nivel_a_topicos'] = $topicos;	

	echo json_encode($jsondata);


 ?>