<?php
	error_reporting(0);
	session_start();  
	if (empty($_SESSION['user'])) {
	   header("location: ./logout/");
	} 
 
  	$idCuest = $_GET['idCuest'];

   	require_once('./db.class.php');
 	
	
	$db = DataBase::connect();// inicio obtener las preguntas de los cuestionarios
    $db->setQuery("select id_usuario, fecha_inicio, fecha_termino, u.nombre as nom_user, u.correo, e.nombre, cc.nombre as nomccostos from estatus_encuestas 
		inner join usuarios u
		on u.numero_empleado = estatus_encuestas.id_usuario
		inner join empresa e
		on e.id = u.id_empresa
        inner join ccostos cc
        on u.numero_empleado = cc.numero_empleado
		where estatus_encuestas.id_cuestionario = ".$idCuest." order by u.nombre");
    $rows = $db->loadObjectList();
    if($rows){
    	$stat ='';

		foreach($rows as $registro){
			//$str .= "<p>" . $registro->id_department . " " . $registro->department . "</p>" ;
			//$str[] = array($registro->id_usuario,$registro->fecha_inicio,$registro->fecha_termino,$registro->nom_user);
			if ($registro->fecha_termino) {
	    		$stat = '<span class="label label-success">Completado</span>';
	    	} else {
	    		$stat = '<span class="label label-danger">Incompleto</span>';
	    	}

			$str[] = array('num_empl' => $registro->id_usuario,'nom_user' => $registro->nom_user,'correo'=>$registro->correo,'empresa'=>utf8_encode($registro->nombre),'stat'=>$stat, 'nomccostos'=>utf8_encode($registro->nomccostos));

			$jsondata['data'] = $str;
		}
    }

    echo json_encode($jsondata);


?>