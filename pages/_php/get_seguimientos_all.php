
<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

    require_once('./db.class.php');
    $db = DataBase::connect();
    include "../mcript.php";

////////////////////////////////////////////////////////////////////////////Obtener encuestas respondidas candidatas a un seguimiento

    
    //List seguimientos
    $db->setQuery('SELECT r.fecha,r.id, r.id_contrato, r.trimestre, r.anio_trimestre, c.num_contrato,cc.nombre as cliente_nombre, cu.nombre as ubicacion_nombre, seg.estatus, cd.nombre as division_nombre, seg.folio
      FROM respuestas r
      inner join contratos c
      on c.id = r.id_contrato
      LEFT JOIN cat_clientes cc on cc.id = c.id_cliente
      LEFT JOIN cat_ubicaciones cu on cu.id = c.id_ubicacion
      LEFT JOIN seguimiento seg on seg.id_respuesta = r.id
      LEFT JOIN cat_division cd on cd.id = c.id_division
      where r.estatus = "P" and r.respuesta3 != "" or r.respuesta6 != "" or r.respuesta9 != "" or r.respuesta12 != "" or r.respuesta15 != "" order by fecha desc;'); 
    $listaSeguimientos = $db->loadObjectlist();
        

    foreach ($listaSeguimientos as $lseg) {
        
        $db->setQuery('SELECT DATEDIFF(NOW(), "'.$lseg->fecha.'") AS postdate'); 
        $fech = $db->loadObject();

        $rows[] = array('division'=>$lseg->division_nombre,'proyecto'=>'<h6>'.$lseg->num_contrato.'</h6>','cliente_nombre' => '<h6>'.$lseg->cliente_nombre.'</h6>','trimestre' => '<h6>'.$lseg->trimestre.'</h6>','anio' => '<h6>'.date("Y", strtotime($lseg->anio_trimestre)).'</h6>', 'postdate' => '<h6>'.$fech->postdate.'</h6>', 'ubicacion_nombre' =>'<h6>'.$lseg->ubicacion_nombre.'</h6>','folio' => $lseg->folio,'estatus'=>'<a href="seguir.php?seg='.urlencode($encriptar($lseg->id)).'" target="_blank">'.getEstatGen($lseg->estatus).'</a>');
    }

    $jsondata['data'] = $rows;
    echo json_encode($jsondata);


function getEstatGen($dato){


    $color_ms ='';
    if($dato == "EN PROCESO"){
        $color_ms ='label-info';
    }
    if($dato == "CANCELADO"){
        $color_ms ='label-danger';
    }
    if($dato == "CERRADO"){
        $color_ms ='label-success';
    }
    if ($dato == "") {
        $dato = "NUEVO";
        $color_ms = 'label-default';
    }                                                
    return '<span class="label '.$color_ms.'">'.$dato.'</span>'; 

}
 ?>