<?php
	error_reporting(0);
	session_start();  
	if (empty($_SESSION['user'])) {
	   header("location: ./logout/");
	} 
 
  	require_once('./db.class.php');
  	$db = DataBase::connect();
	$db->setQuery("SELECT id, nombre FROM cat_division where estatus = 1;");
	$rows = $db->loadObjectList();

	echo '<option value="">Seleccione una división</option>';
	foreach ($rows as $row) {
		echo '<option value="'. $row->id.'">'. $row->nombre .'</option>';
	}

?>