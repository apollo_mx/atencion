<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

    $an = $_GET['an'];
  //$trim = $_GET['trim'];

//////////////////////////////////////////////////////////////////////////////Obtener datos globales por 3 años atras

    $anios = array('2013-01-15','2014-01-15','2015-01-15','2016-01-15','2017-01-15','2018-01-15','2019-01-15','2020-01-15','2021-01-15','2022-01-15','2023-01-15','2024-01-15','2025-01-15','2026-01-15','2027-01-15','2028-01-15','2029-01-15','2030-01-15');
    $indFin =array_search($an,$anios,true)+1;
    $indIni = $indFin-3;

    //$glob_3 = $indIni ." - ". $indFin;
    //$aniosL[] = array(array_slice($anios,$indIni,3,true));
    //$glob_3[] = array('lapso'=>date("Y", strtotime($anios[$indIni])) .' - '.date("Y", strtotime($an)));


    foreach ($anios as $key => $value) {
     	//$glob_3[] = $key;
     	if ($key >= $indIni && $key < $indFin) {
     		//$glob_3[] = $value;

     		for ($i=1; $i <=4 ; $i++) {
     			$db->setQuery("select count(cd.id)as tot from respuestas r 
				inner join contratos c
				on c.id = r.id_contrato
				inner join cat_division cd 
				on cd.id = c.id_division where r.anio_trimestre = '".$value."' and r.trimestre = ".$i.";"); 
				$rg_3 = $db->loadObject();

				$db->setQuery("select count(cd.id)as tot, r.estatus from respuestas r 
				inner join contratos c
				on c.id = r.id_contrato
				inner join cat_division cd 
				on cd.id = c.id_division where r.estatus ='NA' and r.anio_trimestre = '".$value."' and r.trimestre = ".$i.";"); 
				$rna_3 = $db->loadObject();

				$db->setQuery("select count(cd.id)as tot, r.estatus from respuestas r 
				inner join contratos c
				on c.id = r.id_contrato
				inner join cat_division cd 
				on cd.id = c.id_division where r.estatus ='NP' and r.anio_trimestre = '".$value."' and r.trimestre = ".$i.";"); 
				$rnp_3 = $db->loadObject(); 

				$db->setQuery("select count(cd.id)as tot, r.estatus from respuestas r 
				inner join contratos c
				on c.id = r.id_contrato
				inner join cat_division cd 
				on cd.id = c.id_division where r.estatus ='P' and r.anio_trimestre = '".$value."' and r.trimestre = ".$i.";"); 
				$rp_3 = $db->loadObject();        


				$fporcen_3 = $rg_3->tot-$rna_3->tot;
				$valop_3 = is_numeric(round(($rp_3->tot/$fporcen_3)*100,0));
				
				// echo $valop_3;
				if ($valop_3==true) {
				$valop_3=round(($rp_3->tot/$fporcen_3)*100,0);
				}

				if (is_nan($valop_3)) {
					$valop_3 = '0'; 
				}
				//echo $valop_3;
				$glob_3[] = array('anio'=>date("Y", strtotime($value)) .' T'.$i,'trimestre'=>$i,'tot_global' => $rg_3->tot, 'tot_na' => $rna_3->tot, 'tot_np' => $rnp_3->tot, 'tot_p' => $rp_3->tot, 'porcentaje' => $valop_3);
				// echo "<br>";

     		}

			//$glob_3[] = array('tot_global' => $rg_3->tot, 'tot_na' => $rna_3->tot, 'tot_np' => $rnp_3->tot, 'tot_p' => $rp_3->tot, 'porcentaje' => $valop_3);

     	}
    }

    
	$jsondata['combinados_global'] = $glob_3;
	echo json_encode($jsondata);
	unset($an);

 ?>