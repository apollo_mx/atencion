<?php
error_reporting(0);
  session_start();  
  if (empty($_SESSION['user'])) {
       header("location: ./logout/");
  } 

   	require_once('./db.class.php');
 	$db = DataBase::connect();

  $an = $_GET['an'];
  $trim = $_GET['trim'];

//////////////////////////////////////////////////////////////////////////////////////////////////////// genera los datos para los detalles de las tablas
    $db->setQuery("select r.id as id_respuesta, cd.id as id_div, r.estatus from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_division cd 
on cd.id = c.id_division where r.anio_trimestre = '".$an."' and r.trimestre = ".$trim." order by id_div;"); 
    $rows = $db->loadObjectList();
    
    #id_div, division, estatus
    if($rows){
      foreach ($rows as $row) {

          $arr[] = array('id_resp' => $row->id_respuesta, 'id_div' => $row->id_div, 'estatus'=> $row->estatus);
  		  // $datos = 0;
      }
    }else{
        $arr[] = array('id_resp' => 0, 'id_div' => 0, 'estatus'=> 0);
    }


    $db->setQuery("select id, nombre from cat_division;"); 
    $rows1 = $db->loadObjectList();
    
    #id_div, division, estatus
    if($rows1){
      foreach ($rows1 as $row1) {
          $arr1[] = array('id_div' => $row1->id, 'division' => utf8_encode($row1->nombre));
      }
    }else{
        $arr1[] = array('id_div' => 0, 'division' => 0);
    }

    //FUNCION QUE AGREGAR LOS ESTADOS QUE TRAE CADA CONSULTA DE FORMA AGRUPADA, DIVISION Y SUS ESTADOS QUE TRAE
    foreach ($arr1 as $cat_div) {
        $NA = 0;
        $NP = 0;
        $P = 0;
        

      foreach ($arr as $registros) { // Agrego datos cuando la division es igual al resultado de los datos
        if ($cat_div['id_div'] == $registros['id_div']) { 
          if ($registros['estatus'] == 'NA') {
            $NA +=1;
          }
          if ($registros['estatus'] == 'NP') {
            $NP +=1;
          }          
          if ($registros['estatus'] == 'P') {
            $P +=1;
          }          
                    
          $estados[]= array('estatus' => $registros['estatus']);

        }
      }
      if (sizeof($estados)>=1) { //agrego datos solamente cuando tiene algo el estatus 
        
        $all = sizeof($estados);
        $tporcen = $all-$NA;

        $arr2[] = array('id_division' => $cat_div['id_div'], 'nombre' => $cat_div['division'], 'TOTAL' => sizeof($estados), 'NA' => $NA,'NP' => $NP,'P' => $P, 'porcentaje' => (($P/$tporcen)*100)); 
      }

    unset($estados); //limpio registro para poder agregarlo
    }

  if (sizeof($arr2) ==0) { //si está vacio agrego esos valores por default
        $arr2[] = array('id_division' => 0, 'nombre' => 'NINGUNO', 'TOTAL' => 0, 'NA' => 0,'NP' => 0,'P' => 0,'porcentaje'=>'0%');
  }
//////////////////////////////////////////////////////////////////////////fin genera datos para detalle de las tablas.

//////////////////////////////////////////////////////////////////////////genera los datos para obterner valores globales
    $db->setQuery("select count(cd.id)as tot from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_division cd 
on cd.id = c.id_division where r.anio_trimestre = '".$an."' and r.trimestre = ".$trim.";"); 
    $rg = $db->loadObject();

    $db->setQuery("select count(cd.id)as tot, r.estatus from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_division cd 
on cd.id = c.id_division where r.estatus ='NA' and r.anio_trimestre = '".$an."' and r.trimestre = ".$trim.";"); 
    $rna = $db->loadObject();
    
    $db->setQuery("select count(cd.id)as tot, r.estatus from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_division cd 
on cd.id = c.id_division where r.estatus ='NP' and r.anio_trimestre = '".$an."' and r.trimestre = ".$trim.";"); 
    $rnp = $db->loadObject(); 

    $db->setQuery("select count(cd.id)as tot, r.estatus from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_division cd 
on cd.id = c.id_division where r.estatus ='P' and r.anio_trimestre = '".$an."' and r.trimestre = ".$trim.";"); 
    $rp = $db->loadObject();        


    $fporcen = $rg->tot-$rna->tot;
    $valop = is_numeric(round(($rp->tot/$fporcen)*100,0));
    if ($valop==true) {
      $valop=round(($rp->tot/$fporcen)*100,0);
    }

    if ($valop =="NAN") {
      $valop = '0'; 
    }
    
    $glob[] = array('tot_global' => $rg->tot, 'tot_na' => $rna->tot, 'tot_np' => $rnp->tot, 'tot_p' => $rp->tot, 'porcentaje' => $valop);
//////////////////////////////////////////////////////////////////////////////fin genera datos globales

  $jsondata['glob'] = $glob;
  $jsondata['combinados'] = $arr2;



  echo json_encode($jsondata);

  unset($an);
  unset($trim);
?>

