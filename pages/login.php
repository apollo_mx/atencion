<?php 

/*session_start();
$row = array('id_empleado'=> 13062, 'id_rol' => 1,'nombre' => 'Delfino Ruiz Salinas');
$_SESSION['user'] = $row;
*/

if (isset($_SESSION['user'])) {
    header('Location: ./index.php');
}

//session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Sistema de encuestas - Iniciar Sesión</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
           .form-signin-heading{margin-bottom: 10px;text-align: center;}
           #icon{width: 130px;
    margin: 20px 0 10px 0;}
        </style>
    </head>
    <body>

        <div class="container">
            <div class="row">

                <!-- <img src="./img/ecml2.PNG" class="img-thumbnail" alt="Responsive image"> -->
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default" style="margin-top: 15%;">
                        <div class="panel-heading" style="background-color: #fff;">
<!--                             <img src="../img/Logo-png-blanco.png" alt="..." style="width: 100%;"> -->
                            <img src="./img/logoweb.png" id="icon" alt="Apollo Icon">                           
                        </div><br>
                        <h2 class="form-signin-heading">Iniciar Sesión</h2>
                        <div class="panel-body">
                           
                                <fieldset>
                                    <form id="formLogin" name="formLogin" class="formLogin">
                                        <div class="form-group">

                                            <input class="form-control" id="idEmpleado" placeholder="Usuario" name="numero_empleado" type="text" autofocus><br>
                                            <input class="form-control" id="idPasswd" placeholder="Contraseña" name="pwd" type="password" autofocus><br>
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <a href="#" id="enviard" class="btn btn-lg btn-success btn-block click_on_enterkey" style="background-color: #AC182D; border-color:#AC182D;">Iniciar sesión</a>
                                    </form>
                                </fieldset>
                            
                        </div>
                        <br>

                        <div class="panel-footer">
<!--                             ¿No tienes una cuenta?
                            <a href="registrate.php">Regístrate</a> -->
                            <div class="clearfix">&nbsp;</div>
                            <a href="olvido.php">¿Olvidaste tu contraseña?</a>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>        
        <script type="text/javascript">
            $(document).ready(function(){
               

                $(document).keypress(function(event){
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if(keycode == '13'){
                       $("#enviard").click();
                    }
                });


                $("#enviard").click(function() {

                    var idEmpleado = $("#idEmpleado").val(); 
                    var idPasswd = $("#idPasswd").val();                     
                    
                    //console.log(idEmpleado);
                    
                    $.ajax({
                        type: 'POST',
                        url: './_php/login/index.php',
                        dataType: 'json',
                        data: {idEmpleado:idEmpleado, idPasswd:idPasswd},
                        complete: function (xhr, textStatus) {
                        //called when complete
                        },
                        success: function(data) {
                            //console.log(data);
                            if (data.res == 'ok') {
                                    window.location = './index.php';
                                }else{
                                    bootbox.alert({
                                        size: "small",
                                        title: "Alerta",
                                        message: "<label>Ocurrio un error al intentar iniciar sesion, usuario y/o contraseña incorrecto</label>",
                                        callback: function(){ location.reload(); }
                                    });  
                            }    
                        }
                            
                    });


                });

               


            });//fin jquery



    </script>
    </body>
</html>
