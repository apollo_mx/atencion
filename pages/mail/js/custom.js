/* HTML document is loaded. DOM is ready.
-------------------------------------------*/
$(function(){
    
    
   
    $('#enviar-form').click(function(){

        nam = $("#fullname").val();
        em = $("#email").val();
        msj = $("#message").val();
        bandera = "";
        if (nam == "") {
            $("#fullname").focus();
            $("#respuesta").html("Nombre requerido.");
            bandera = 1;
            return false;
        }
        if (em == "") {
            $("#email").focus();
            $("#respuesta").html("Correo requerido.");
            bandera = 1;
            return false;
        }

        if (msj == "") {
            $("#message").focus();
            $("#respuesta").html("Mensaje requerido.");
            bandera = 1;
            return false;
        }
        if (bandera != 1) {
            $("#respuesta").html("Tu mensaje a sido enviado con exito, nuestro equipo se pondra en contacto contigo.");

            $.post( "./mail/test_mail.php", { name_: nam, email_:em, message_: $.trim(msj) }, function(data) {
                $("#respuesta").html("Tu mensaje a sido enviado con exito, nuestro equipo se pondra en contacto contigo.");
            }, "json");

            var explode = function(){
              $("#fullname").val("");
              $("#email").val("");
              $("#message").val("");
            };
            setTimeout(explode, 3000);

        }
    });



    /* start typed element */
    //http://stackoverflow.com/questions/24874797/select-div-title-text-and-make-array-with-jquery
    var subElementArray = $.map($('.sub-element'), function(el) { return $(el).text(); });    
    $(".element").typed({
        strings: subElementArray,
        typeSpeed: 30,
        contentType: 'html',
        showCursor: false,
        loop: true,
        loopCount: true,
    });
    /* end typed element */

    /* Smooth scroll and Scroll spy (https://github.com/ChrisWojcik/single-page-nav)    
    ---------------------------------------------------------------------------------*/ 
    $('.templatemo-nav').singlePageNav({
        offset: $(".templatemo-nav").height(),
        filter: ':not(.external)',
        updateHash: false
    });

    /* start navigation top js */
    $(window).scroll(function(){
        if($(this).scrollTop()>58){
            $(".templatemo-nav").addClass("sticky");
        }
        else{
            $(".templatemo-nav").removeClass("sticky");
        }
    });
    
    /* Hide mobile menu after clicking on a link
    -----------------------------------------------*/
    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });
    /* end navigation top js */

    $('body').bind('touchstart', function() {});

    /* wow
    -----------------*/
    new WOW().init();
});

/* start preloader */
$(window).load(function(){
	$('.preloader').fadeOut(1000); // set duration in brackets    
});
/* end preloader */
