<?php 

error_reporting(0);
//https://www.youtube.com/watch?v=JVnDHYS_3Ag

    session_start();  
    if (empty($_SESSION['user'])) {
       header("location: ./logout/");
    } 
    
    include "./mcript.php";
    
    $id_respuesta = $desencriptar($_GET['seg']);

    $codEmpleadoResponde = 0;

    // echo $id_respuesta;    
    // echo '<br>';
    // echo $codEmpleadoResponde;    

    require_once('./_php/db.class.php');

    //Seleccionar todos los datos relacionadps a la respuesta de un cuestionario y un contrato
    $db = DataBase::connect();
    $db->setQuery("select r.id as id_respuesta, c.num_contrato,c.descripcion,cd.nombre as division,r.anio_trimestre,r.trimestre, r.estatus, crep.nombre,crep.correo,crep.puesto from respuestas r 
inner join contratos c
on c.id = r.id_contrato
inner join cat_representantes crep
on c.id_representante = crep.id 
inner join cat_division cd 
on cd.id = c.id_division where r.id =".$id_respuesta);
    $rowe = $db->loadObject();


    $date = date_create($rowe->anio_trimestre); 
    $anFormat = date_format($date,"Y");

    //Seleccionar las respuestas de peticiones que se realizaron
    $db->setQuery("SELECT respuesta3, respuesta6, respuesta9,respuesta12,respuesta15 FROM respuestas where id =".$id_respuesta); 
	$listPeticiones = $db->loadObject();
	

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de atención al cliente - Seguimiento</title>        
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

<!--         <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
 -->        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        
        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->
        <link href="../js/dist/css/jquery.dm-uploader.min.css" rel="stylesheet">
        <link href="../css/styles.css" rel="stylesheet">


        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="../css/jquery.chosen.css" rel="stylesheet" type="text/css">
  
        <style type="text/css">
           .titulo{
            color: #fff;
            background-color: #cf203d;
            text-align: center;
            font-size: 20px !important;
           }
           label{
            font-size: 13px;
            color: #969696;
           }
           .subtit{
            font-weight: 700;
            color: #002e5b;
            font-size: 14px;
           }
            .subtit1 {
                font-weight: 700;
                color: #000;
/*                font-size: 14px;*/
                text-align: right;
                display: block;
            }           
           .checkbox, .radio {
                margin-top: 5px;
                margin-bottom: 5px;
                padding-left: 20px;
            }

            blockquote {
        
                border-left-color: #f0ad4e;
            }            
            .recived_msg{
                background: #ebebeb none repeat scroll 0 0;
                border-radius: 3px;
                color: #646464;
                font-size: 14px;
                margin: 0;
                padding: 5px 10px 5px 12px;
                width: 100%;
                display: block;
            }
            .time_date{
                color: #747474;
                display: block;
                font-size: 10px;
                margin: 8px 0 0;
                text-align: right;                
            }
            .incoming_msg_img{
                width: 35px;
            }
            .autor{
                font-size: 12px;
                line-height: 1.5;
                color: var(--color-text-primary);
                font-weight: 600;
                white-space: nowrap;                
            }
           
            #toc_container {
                background: #f9f9f9;
                border: 1px solid #aaa;
/*                padding: 10px;*/
                margin-bottom: 1em;
                width: auto;
                display: table;
                font-size: 95%;
            }
            #mail_container {
                background: #f9f9f9;
                border: 1px solid #aaa;
/*                padding: 10px;*/
                margin-bottom: 1em;
                width: auto;
                display: table;
                font-size: 95%;
            }            
       </style>
    </head>
    <body class="bg-light">
        
	    <main role="main">

            <div class="panel-body">

                <!-- Button trigger modal -->
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Reasignar Colaborador</h4>
                            </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label>Reasignar Colaborador</label>                 
                                        <select id="colab_reasigna" name="colab_reasigna" data-placeholder="Selecciona un Colaborador" class="chosen-select" tabindex="-1" style="display: none; width: 350px;">
                                        </select>
                                    </div>         
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>            
        <?php if ($id_respuesta != ""){ //Si el id de seguimiento es correcto ?>
<div class="container">
            <div id="loading" class="col-md-6" style="text-align: center;"> <img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
                <br><br>

                <div class="col-md-10 col-md-offset-1">
                    

                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="col-md-6" style="font-weight: 700; color: #002e5b; font-size: 14px; text-align: left;">
                                                <img src="/atencion/img/logo.png" style="width: 160px; height: px;" alt="APOLLO">
                                            </div>
                                            <div class="col-md-6" style="font-weight: 700; color: #002e5b; font-size: 14px; text-align: right;">
                                            <?php 
                                                //Verificar Si ya se inició un seguimiento
                                                $db->setQuery("SELECT * FROM seguimiento where id_respuesta =".$id_respuesta); 
                                                $seguimiento_general = $db->loadObject();

                                                //echo sizeof($seguimiento_general);
                                                if ($seguimiento_general == 1) {
                                            ?>
                                            <h1><small>Folio:</small><small id="folioSeguimiento"><?php echo $seguimiento_general->folio; ?></small></h1>

                                            <?php }else{ ?>
                                                <h1><small>Folio:</small><small id="folioSeguimiento"></small></h1>                                                    
                                                <script>
                                                    var folioSeg =Math.round(new Date().getTime()/1000);
                                                    document.getElementById("folioSeguimiento").innerHTML = folioSeg;
                                                </script>

                                            <?php
                                                }

                                            ?>                                  

                                            </div>  
                                        </td>                                       
                                    </tr>
                                </tbody>
                            </table>
                        </div>                  
                        <hr>                            
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Contrato</th>
                                        <td><?php echo $rowe->num_contrato; ?></td>
                                        <th>Representante</th>
                                        <td><?php echo ($rowe->nombre); ?></td>   
                                    </tr>
                                    <tr>
                                        <th>Descripcion</th>
                                        <td><?php echo utf8_encode($rowe->descripcion); ?></td>
                                        <th>Correo</th>
                                        <td><?php echo utf8_encode($rowe->correo); ?></td>                                                                              
                                    </tr>
                                    <tr>
                                        <th>División</th>
                                        <td><?php echo utf8_encode($rowe->division); ?></td>    
                                        <th>Puesto</th>
                                        <td><?php echo $rowe->puesto; ?></td>                                                                               
                                    </tr>                                   
                                    <tr>
                                        <th>Año y Trimestre</th>
                                        <td><?php echo $anFormat .",Trimestre". $rowe->trimestre; ?></td>                                                               
                                        <th>Estatus</th>
                                        <td>
                                            <?php if ($seguimiento_general == 1) { 
                                                $color_ms ='';
                                                if($seguimiento_general->estatus == "EN PROCESO"){
                                                    $color_ms ='label-info';
                                                }
                                                if($seguimiento_general->estatus == "CANCELADO"){
                                                    $color_ms ='label-danger';
                                                }
                                                if($seguimiento_general->estatus == "CERRADO"){
                                                    $color_ms ='label-success';
                                                }                                                
                                                echo '<span class="label '.$color_ms.'">'.$seguimiento_general->estatus.'</span>'; 
                                            } ?>
                                            <?php if ($seguimiento_general < 1) { echo '<span class="label label-default">NUEVO</span>'; } ?>

                                        </td>                                                                       

                                    </tr>                                                                           
                                </tbody>
                            </table>                            
                        </div>
                        <?php 
                            if ($seguimiento_general == 1){
                                
                            }else{
                        ?>
<!--                         se carga cuando aun no hay una asignacion -->
                        <div class="col-md-12">   
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sección</th>
                                        <th style="text-align: center;"><span>Solicitud y/o Petición</span></th>
                                        <th>Asignar a:</th>
                                        <th>Cancelar un Seguimiento</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($listPeticiones->respuesta3) { ?>                                    
                                    <tr>
                                        <td>Servicio</td>
                                        <td><?php echo $listPeticiones->respuesta3; ?></td>
                                        <td>
                                            <div class="form-group">
                                                <label id="empl3">Colaborador</label>                
                                                <select id="clnt3" name="clnt3" data-placeholder="Selecciona un Colaborador" multiple class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>

                                            </div>
                                        </td>
                                        <td>
                                            <input type="button" id="canc3" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <?php if ($listPeticiones->respuesta6) { ?>
                                    <tr>
                                        <td>Equipos e instalaciones</td>
                                        <td><?php echo $listPeticiones->respuesta6; ?></td>
                                        <td>
                                            <div class="form-group">  
                                                <label id="empl6">Colaborador</label>               
                                                <select id="clnt6" name="clnt6" multiple data-placeholder="Selecciona un Colaborador" class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                          
                                        </td>
                                        <td>
                                            <input type="button" id="canc6" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>                               
                                    <?php } ?>
                                    <?php if ($listPeticiones->respuesta9) { ?>                                                                          
                                    <tr>
                                        <td>Factor humano</td>
                                        <td><?php echo $listPeticiones->respuesta9; ?></td>
                                        <td>
                                            <div class="form-group"> 
                                                <label id="empl9">Colaborador</label>                
                                                <select id="clnt9" name="clnt9" multiple data-placeholder="Selecciona un Colaborador" class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                          
                                        </td>
                                        <td>
                                            <input type="button" id="canc9" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>                                    
                                    <?php } ?>                                                                  
                                    <?php if ($listPeticiones->respuesta12) { ?>                                    
                                    <tr>
                                        <td>Gestión</td>
                                        <td><?php echo $listPeticiones->respuesta12; ?></td>
                                        <td>
                                            <div class="form-group">
                                                <label id="empl12">Colaborador</label>                
                                                <select id="clnt12" name="clnt12" data-placeholder="Selecciona un Colaborador" multiple class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                          
                                        </td>
                                        <td>
                                            <input type="button" id="canc12" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>
                                    <?php } ?>                                      
                                    <?php if ($listPeticiones->respuesta15) { ?>
                                    <tr>
                                        <td>Imagen</td>
                                        <td><?php echo $listPeticiones->respuesta15; ?></td>
                                        <td>
                                            <div class="form-group">
                                                <label id="empl15">Colaborador</label>                 
                                                <select id="clnt15" name="clnt15" data-placeholder="Selecciona un Colaborador" multiple class="chosen-select" tabindex="-1" style="display: none;">
                                                </select>
                                            </div>                                                                                             
                                        </td>
                                        <td>
                                            <input type="button" id="canc15" class="btn btn-danger" value="Cancelar">
                                        </td>
                                    </tr>
                                    <?php } ?>                                                                                                                                                      
                                </tbody>
                            </table>
                        </div>
                        <form role="form" id="f_encuesta">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="col-md-6"></div>  
                                <div class="col-md-12" style="text-align: right;">
                                    <input id="enviarDatos" type="button" class="btn btn-primary" value="Guardar">
                                </div>  
                            </div>
                        </form>
                        
                        <?php } ?>

                        
                        <?php if ($seguimiento_general == 1):   //Seccion que se muestra cuando ya está generado un folio
                             $db->setQuery("SELECT * FROM atencion.detalle_seguimiento where folio_seguimiento =".$seguimiento_general->folio); 
                            $seguimiento_detalle = $db->loadObjectlist();
                            //print_r($seguimiento_detalle);                            # code...
                            include './_php/cise_get_UserId.php'; 
                        ?>

                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div style="text-align: center;">
                                        <div class="col-md-10">
                                            Detalles
                                        </div>
                                     <?php $tot = 0; ?>
                                    <?php foreach ($seguimiento_detalle as $key => $value): ?>
                                        <?php
                                            if ($value->estatus == "en proceso" || $value->estatus == "asignado" || $value->estatus == "validacion") {
                                                $tot=1;
                                            }
                                         ?>
                                    <?php endforeach ?>
                                     <?php if ($tot==0): ?>
                                        <?php if ($seguimiento_general->estatus !="CANCELADO"): ?>
                                            <div class="col-md-2">
                                              <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-gear"></i> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#" onclick="cerrarSeguimiento(<?php echo $seguimiento_general->folio; ?>);"><i class="fa fa-check-circle fa-fw"></i> Cerrar Seguimiento</a>
                                                    </li>
                                                </ul>
                                              </div>
                                            </div>                                           
                                        <?php endif ?>
                                     <?php endif ?>
                                    <br><br>
                                    </div>
                                </div>
                                <div class="panel-body">
                               
                                <div class="row">
        
                                    <ul class="nav nav-tabs">
                                    <?php function getSeccion($secc){
                                        switch ($secc) {
                                            case 'respuesta3':
                                                return 'Servicio';
                                                break;
                                            case 'respuesta6':
                                                return 'Equipos e instalaciones';
                                                break; 
                                            case 'respuesta9':
                                                return 'Factor humano';
                                                break;
                                            case 'respuesta12':
                                                return 'Gestión';
                                                break;       
                                            case 'respuesta15':
                                                return 'Imagen';
                                                break;
                                            default:
                                                return 'No';
                                                break;
                                        }
                                    } ?>                             
                                    <?php foreach ($listPeticiones as $key => $val_peticiones): ?>
                                        <?php if ($val_peticiones): ?>
                                            <li><a data-toggle="pill" href="#<?php echo $key; ?>" onClick="que_preg(<?php echo "'".$key."'"; ?>);"><?php echo getSeccion($key); ?></a></li>
                                        
                                        <?php endif ?>

                                    <?php endforeach ?>
                                  <?php $page = "back"; ?>

                                  </ul>
                                  <div class="tab-content">
                                    <div id="respuesta3" class="tab-pane fade">
                                      <?php include './_php/serv_tab.php'; ?>
                                    </div>
                                    <div id="respuesta6" class="tab-pane fade">
                                      <?php include './_php/equi_e_ins_tab.php'; ?>
                                    </div>
                                    <div id="respuesta9" class="tab-pane fade">
                                      <?php include './_php/fac_hum_tab.php'; ?>
                                    </div>
                                    <div id="respuesta12" class="tab-pane fade">
                                      <?php include './_php/gest_tab.php'; ?>
                                    </div>
                                    <div id="respuesta15" class="tab-pane fade">
                                      <?php include './_php/imag_tab.php'; ?>
                                    </div>                                    
                                  </div>
                                </div>


                                </div>

                            </div>
                        </div>                                  
                        <?php endif ?>
                                            
                </div>

            </div> 
            <?php 

            //ID de tab seguimiento que se está dando
            $db->setQuery("SELECT id as aler_id, num_pregunta as aler_numpreg, cod_empleado as aler_cod_emp, estatus as alert_estatus FROM atencion.detalle_seguimiento where folio_seguimiento = '".$seguimiento_general->folio."' and alerta_reasignacion = 1;");
            $reg_notifi = $db->loadObjectlist();

            ?>

            <?php if (sizeof($reg_notifi)>=1): ?>
            <ul class="dropdown-menu dropdown-alerts pull-right" id="toc_container" style="top: 25%;">
                <li>
                    <a class="text-center" href="#">
                        <strong>Solicitud de reasignación&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
                <li class="divider"></li>
                <?php foreach ($reg_notifi as $key => $value): ?>
                    <li><a href="#" onclick="reasigna(<?php echo $value->aler_id; ?>);"><div><i class="fa fa-bell-o fa-fw" style="color:red;"></i> 
                        <span class="autor"><?php echo getNameabreb($value->aler_cod_emp); ?></span><span class="pull-right text-muted small" style="margin: 5px 0 0 0;"><?php echo getSeccion("respuesta".$value->aler_numpreg); ?></span></div></a></li>
                <?php endforeach ?>

            </ul>                
            <?php endif ?>
            <?php 
            //ID de tab seguimiento que se está dando
                $db->setQuery("select * from atencion.detalle_seguimiento where folio_seguimiento = '".$seguimiento_general->folio."' and tracking != '' and estatus != 'CERRADO';");
                $reenv_mail = $db->loadObjectlist();
            ?>
            <?php if (sizeof($reenv_mail)>=1): ?>
            <ul class="dropdown-menu dropdown-alerts pull-left" id="mail_container" style="top: 25%;">
                <li>
                    <a class="text-center" href="#">
                        <strong>Reenviar link de seguimiento</strong>
                        <i class="fa fa-angle-left"></i>
                    </a>
                </li>
                <li class="divider"></li>
                <?php foreach ($reenv_mail as $key => $value): ?>
                    
                    <li><a href="#" onclick="sendMailjs(<?php echo $value->id; ?>);"><div><i class="fa fa-envelope fa-fw" style="color:red;"></i> 
                        <span class="autor"><?php echo getNameabreb($value->cod_empleado); ?></span><span class="pull-right text-muted small" style="margin: 5px 0 0 0;"><?php echo getSeccion("respuesta".$value->num_pregunta); ?></span></div></a>
                    </li>
                <?php endforeach ?>
            </ul>
            <?php endif ?>
               
        <?php }else{  //Si el id de seguimiento no  correcto  ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-4">
                    <h1>Folio de seguimiento incorrecto.</h1>
                </div>
            </div>
        <?php } ?>
			
            <br>
	    </main>
<!-- File item template -->
    <script type="text/html" id="files-template" class="files-template">
      <li class="media">
        <div class="media-body mb-1">
          <p class="mb-2">
            <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
          </p>
          <div class="progress mb-2">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
              role="progressbar"
              style="width: 0%" 
              aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
            </div>
          </div>
          <hr class="mt-1 mb-1" />
        </div>
      </li>
    </script>

    <!-- Debug item template -->
    <script type="text/html" id="debug-template" class="debug-template">
      <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
    </script>
    </body>
<!-- jQuery -->
    <script src="../js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../js/metisMenu.min.js"></script>
    <script src="../js/dataTables/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../js/startmin.js"></script>
    <script src="../js/bootbox.min.js"></script>
    <script src="../js/chosen.jquery.min.js"></script>

    <script src="../js/dist/js/jquery.dm-uploader.min.js"></script>
    <script src="../js/demo-ui.js"></script>
    <script src="../js/demo-config.js"></script>

    <script type="text/javascript">
	    var num_pr = 0;
        //console.log("forma global " + num_pr);

        function que_preg(pregunta){
            num_pr = pregunta;
            //console.log("dentro de funcion click tab" + num_pr);
        }
        $(document).ready(function(){
  
           
            // Estatus 1 asignado
            // Estatus 2 cerrado
            // Estatus 3 cancelado
            

            $('#loading').hide(); //initially hide the loading icon

            $(document).ajaxStart(function(){
                $('#loading').show();
                //console.log('shown');
              });
            $(document).ajaxStop(function(){
                $('#loading').hide();
                //console.log('hidden');
            });


            $('#clnt3').load('./_php/cise_get_allUser.php', function() {
                //$(this).chosen();
                $(this).chosen({
                    no_results_text: "¡No se ha encontrado nada!",
                    width: "50%",
                    max_selected_options: 3,
                  });

            });
            $('#clnt6').load('./_php/cise_get_allUser.php', function() {
                $(this).chosen({
                    no_results_text: "¡No se ha encontrado nada!",
                    width: "50%",
                    max_selected_options: 3,                    
                });
            });
            $('#clnt9').load('./_php/cise_get_allUser.php', function() {
                $(this).chosen({
                    no_results_text: "¡No se ha encontrado nada!",
                    width: "50%",
                    max_selected_options: 3,                    
                });
            });
            $('#clnt12').load('./_php/cise_get_allUser.php', function() {
                $(this).chosen({
                    no_results_text: "¡No se ha encontrado nada!",
                    width: "50%",
                    max_selected_options: 3,                    
                });
            });
            $('#clnt15').load('./_php/cise_get_allUser.php', function() {
                $(this).chosen({
                    no_results_text: "¡No se ha encontrado nada!",
                    width: "50%",
                    max_selected_options: 3,                    
                });
            });                                                

            $('#colab_reasigna').load('./_php/cise_get_allUser.php', function() {
                $(this).chosen({
                    no_results_text: "¡No se ha encontrado nada!",
                    width: "50%",
                    max_selected_options: 1,                    
                });
            }); 

                                               

            var resultreg = [];
            var id_respuesta =  '<?php echo $id_respuesta; ?>';


            if ($('#clnt3').length == 1) {

                var dataAsignacion3 = {numPreg: null,estatus: null,codEmpleado: null};

                // Cuando seleccionan una persona asignada 
                $('#clnt3').on('change', function() {

                    dataAsignacion3.numPreg = 3;
                    dataAsignacion3.estatus = 'asignado';
                    dataAsignacion3.codEmpleado = $(this).val();
                });
                // Cuando seleccionan la opcion de cancelacion, setear los datos
                $('#canc3').on('click', function() {                                        
                    $(this).attr('disabled',true);                   
                    $('#clnt3').prop('disabled', true).trigger("liszt:updated");

                    dataAsignacion3.numPreg = 3;                    
                    dataAsignacion3.estatus = 'cancelado';
                    dataAsignacion3.codEmpleado = 0;
                });
                function preg3Ok(){
                    if (dataAsignacion3.estatus == null) {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: "<label>Los campos de seleccion son requeridos, a menos que quieras cancelar</label>",
                            callback: function(){
                                $('#empl3').css('color','red');
                               clearCboincomplete();  
                            }
                        });
                        return 0;
                    }else{
                        $('#empl3').css('color','#969696');
                        resultreg.push(dataAsignacion3);
                        // console.log(resultreg);
                        return 1;                        
                    }
                }
            }

            if ($('#clnt6').length == 1) {
                var dataAsignacion6 = {numPreg: null,estatus: null,codEmpleado: null};
                
                // Cuando seleccionan una persona asignada 
                $('#clnt6').on('change', function() {
                    dataAsignacion6.numPreg= 6;                    
                    dataAsignacion6.estatus = 'asignado';
                    dataAsignacion6.codEmpleado = $(this).val();

                });

               // Cuando seleccionan la opcion de cancelacion, setear los datos
                $('#canc6').on('click', function() {                                        
                    $(this).attr('disabled',true);                   
                    $('#clnt6').prop('disabled', true).trigger("liszt:updated");
                    dataAsignacion6.numPreg= 6;
                    dataAsignacion6.estatus = 'cancelado';
                    dataAsignacion6.codEmpleado = 0;

                });
                function preg6Ok(){
                    if (dataAsignacion6.estatus == null) {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: "<label>Los campos de seleccion son requeridos o selecciona cancelar la asignacion.</label>",
                            callback: function(){
                                $('#empl6').css('color','red');
                                clearCboincomplete(); 
                            }
                        });
                        return 0;                       
                    }else{
                        $('#empl6').css('color','#969696');
                        resultreg.push(dataAsignacion6);
                        // console.log(resultreg);
                        return 1;                        
                    }
                }                
            }

            if ($('#clnt9').length == 1) {
                var dataAsignacion9 = {numPreg: null,estatus: null,codEmpleado: null};
                
                // Cuando seleccionan una persona asignada 
                $('#clnt9').on('change', function() {
                    dataAsignacion9.numPreg= 9;                    
                    dataAsignacion9.estatus = 'asignado';
                    dataAsignacion9.codEmpleado = $(this).val();
                });

               // Cuando seleccionan la opcion de cancelacion, setear los datos
                $('#canc9').on('click', function() {                                        
                    $(this).attr('disabled',true);                   
                    $('#clnt9').prop('disabled', true).trigger("liszt:updated");
                    dataAsignacion9.numPreg= 9;                    
                    dataAsignacion9.estatus = 'cancelado';
                    dataAsignacion9.codEmpleado = 0;
                });
                function preg9Ok(){
                    if (dataAsignacion9.estatus == null) {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: "<label>Los campos de seleccion son requeridos o selecciona cancelar la asignacion.</label>",
                            callback: function(){
                                $('#empl9').css('color','red');
                                clearCboincomplete(); 
                            }
                        });
                        return 0;                       
                    }else{
                        $('#empl9').css('color','#969696');
                        resultreg.push(dataAsignacion9);
                        // console.log(resultreg);
                        return 1;                        
                    }
                } 
            }

            if ($('#clnt12').length == 1) {
                var dataAsignacion12 = {numPreg: null,estatus: null,codEmpleado: null};
                
                // Cuando seleccionan una persona asignada 
                $('#clnt12').on('change', function() {
                    dataAsignacion12.numPreg= 12;                    
                    dataAsignacion12.estatus = 'asignado';
                    dataAsignacion12.codEmpleado = $(this).val();
                });

               // Cuando seleccionan la opcion de cancelacion, setear los datos
                $('#canc12').on('click', function() {                                        
                    $(this).attr('disabled',true);                   
                    $('#clnt12').prop('disabled', true).trigger("liszt:updated");
                    dataAsignacion12.numPreg= 12;                    
                    dataAsignacion12.estatus = 'cancelado';
                    dataAsignacion12.codEmpleado = 0;
                });
                function preg12Ok(){
                    if (dataAsignacion12.estatus == null) {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: "<label>Los campos de seleccion son requeridos o selecciona cancelar la asignacion.</label>",
                            callback: function(){
                                $('#empl12').css('color','red');
                                clearCboincomplete();        
                            }
                        });
                        return 0;                       
                    }else{
                        $('#empl12').css('color','#969696');
                        resultreg.push(dataAsignacion12);
                        return 1;
                    }
                }
            }

            if ($('#clnt15').length == 1) {
               var dataAsignacion15 = {numPreg: null,estatus: null,codEmpleado: null};
                
                // Cuando seleccionan una persona asignada 
                $('#clnt15').on('change', function() {
                    dataAsignacion15.numPreg= 15;                    
                    dataAsignacion15.estatus = 'asignado';
                    dataAsignacion15.codEmpleado = $(this).val();
                });

               // Cuando seleccionan la opcion de cancelacion, setear los datos
                $('#canc15').on('click', function() {                                        
                    $(this).attr('disabled',true);                   
                    $('#clnt15').prop('disabled', true).trigger("liszt:updated");
                    dataAsignacion15.numPreg= 15;                    
                    dataAsignacion15.estatus = 'cancelado';
                    dataAsignacion15.codEmpleado = 0;
                });
                function preg15Ok(){
                    if (dataAsignacion15.estatus == null) {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: "<label>Los campos de seleccion son requeridos o selecciona cancelar la asignacion.</label>",
                            callback: function(){
                                $('#empl15').css('color','red');
                                clearCboincomplete(); 
                            }
                        });
                        return 0;                       
                    }else{
                        $('#empl15').css('color','#969696');
                        resultreg.push(dataAsignacion15);
                        // console.log(resultreg);
                        return 1;                        
                    }
                }
            }

            function clearCboincomplete(){
                resultreg.pop();
                // dataAsignacion6
                // dataAsignacion9
                // dataAsignacion12
                // dataAsignacion15
            }

            //FUNCION QUE EXTRAE TODOS LOS DATOS
            $('#enviarDatos').on('click', function() {

                var checkin = [];
                //checkin.length = 0;
                if ($('#clnt3').length == 1) { 
                    if (preg3Ok() == 1) {
                        // return false;
                        checkin.push(1);
                    }else{
                        checkin.push(0);
                    }
                }
                if ($('#clnt6').length == 1) {
                    if (preg6Ok() == 1) {
                        checkin.push(1);
                    }else{
                        checkin.push(0);
                    }
                }
                if ($('#clnt9').length == 1) { 
                    if (preg9Ok() == 1) {
                        checkin.push(1);
                    }                 else{
                        checkin.push(0);
                    }  
                }
                if ($('#clnt12').length == 1) {
                    if (preg12Ok() == 1) {
                        checkin.push(1);
                    }  else{
                        checkin.push(0);
                    }
                }
                if ($('#clnt15').length == 1) {
                    if (preg15Ok() == 1) {
                        checkin.push(1);
                    }else{
                        checkin.push(0);
                    }
                }

                //checkin.find(0);                                                             
                //console.log(checkin.indexOf(0));
                if (checkin.indexOf(0) == -1) {
                    // console.log(resultreg);
                    // console.log($('#folioSeguimiento').text());
                    // console.log(id_respuesta);

                    var estatus_seg ="CANCELADO";
                    $.each( resultreg, function( index, value ){
                        //sum += value;==
                        if (value.estatus == "asignado") {
                            estatus_seg = "EN PROCESO";
                        }

                    });

                    //console.log(resultreg);     
                    // console.log($('#folioSeguimiento').text());
                    // console.log(id_respuesta);
                    // console.log(estatus_seg);
                    console.log(resultreg);


                    bootbox.confirm({ 
                        size: "small",
                        title: 'Crear Seguimiento',
                        message: "¿Deseas continuar?",
                        buttons: {
                            confirm: {
                                label: 'Continuar',
                                className: 'btn-primary'
                            },
                            cancel: {
                                label: 'Cancelar',
                                className: 'btn-secondary'
                            }
                        },
                        callback: function(result){ 
                        /* result is a boolean; true = OK, false = Cancel*/ 
                            if (result === true) {
                                //var datastring = $("#formAColaborador").serializeArray(); 

                                $.ajax({
                                    type: 'POST',
                                    url: './_php/insertSeguimiento.php',
                                    dataType: 'json',
                                    data: {folio_seguimiento:$('#folioSeguimiento').text(),id_respuesta:id_respuesta,estatus_seg:estatus_seg,resultreg:resultreg},
                                     complete: function (xhr, textStatus) {
                                        //called when complete
                                        console.log(textStatus);
                                    },
                                    success: function(data) {
                                         //console.log(data);
                                        bootbox.alert({
                                            size: "small",
                                            title: "Alerta",
                                            message: "<label>"+data['data']+"</label>",
                                            callback: function(){
                                                location.reload(1);
                                            }
                                        });                                   
                                    }
                                });//fin ajax                         
                            }
                        }
                        
                    });

                }else{
                    result.length =0;
                    return false;
                }

            });


    	});
    
    function sendNota(note,num_preg){   //INserta una nota de texto

        var nota = $('#'+note).val();
        var dataNota = {folio: $('#folioSeguimiento').text(), num_preg: num_preg,nota:null, codEmpleado:'<?php echo $codEmpleadoResponde; ?>'};
        if ($.trim(nota).length >=10) {
            dataNota.nota = nota; 

            $.ajax({
                type: 'POST',
                url: './_php/insertNota_texto_admn.php',
                dataType: 'json',
                data: {dataNota:dataNota},
                 complete: function (xhr, textStatus) {
                    //called when complete
                    //console.log(textStatus);
                },
                success: function(data) {
                    // console.log(data)
                    bootbox.alert({
                        size: "small",
                        title: "Alerta",
                        message: "<label>"+data['data']+"</label>",
                        callback: function(){
                            location.reload(1);
                        }
                    });                                   
                }
            });//fin ajax   

        }else{
            bootbox.alert({
                size: "small",
                title: "Alerta",
                message: "<label>Campo Nota, Requiere mínimo 10 caracteres.</label>",
                callback: function(){
                }
            });    
        }
    }

    function sendFile(rutaImg){   //INserta un archivo
        // console.log("dentro de funcion insertar archivo" + num_pr);
        var dataFile = {folio: $('#folioSeguimiento').text(), num_preg: num_pr,urlFile:null, codEmpleado:'<?php echo $codEmpleadoResponde; ?>'};
        dataFile.urlFile = rutaImg; 

        $.ajax({
            type: 'POST',
            url: './_php/insertNota_archivo.php',
            dataType: 'json',
            data: {dataFile:dataFile},
             complete: function (xhr, textStatus) {
                //called when complete
                //console.log(textStatus);
            },
            success: function(data) {
                // console.log(data);
                bootbox.alert({
                    size: "small",
                    title: "Alerta",
                    message: "<label>"+data['data']+"</label>",
                    callback: function(){
                        location.reload(1);
                    }
                });                                   
            }
        });//fin ajax   

    }

    function cerrarSeguimiento(folio){
        bootbox.confirm({ 
            size: "small",
            title: 'Cerrar Seguimiento',
            message: "¿Deseas continuar?",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-secondary'
                }
            },
            callback: function(result){ 
            /* result is a boolean; true = OK, false = Cancel*/ 
                if (result === true) {
                    //var datastring = $("#formAcliente").serializeArray(); 

                    $.ajax({
                        type: 'POST',
                        url: './_php/cerrarSeguimiento.php',
                        dataType: 'json',
                        data: {folio:folio},
                         complete: function (xhr, textStatus) {
                            //called when complete
                            //console.log(textStatus);
                        },
                        success: function(data) {
                            //console.log(data);
                            bootbox.alert({
                                size: "small",
                                title: "Alerta",
                                message: "<label>"+data['data']+"</label>",
                                callback: function(){
                                    location.reload(1);
                                }
                            });                                   
                        }
                    });//fin ajax                         
                }
            }
            
        });
    }

    function sendMailjs(idDet){
        bootbox.confirm({ 
            size: "small",
            title: 'Reenviar link de seguimiento',
            message: "¿Deseas continuar?",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-secondary'
                }
            },
            callback: function(result){ 
            /* result is a boolean; true = OK, false = Cancel*/ 
                if (result === true) {
                    //var datastring = $("#formAcliente").serializeArray(); 
                    //console.log(idDet);
                    $.ajax({
                        type: 'POST',
                        url: './_php/envioUrlSeguimiento.php',
                        dataType: 'json',
                        data: {idDet:idDet},
                         complete: function (xhr, textStatus) {
                            //called when complete
                            //console.log(textStatus);
                        },
                        success: function(data) {
                            //console.log(data);
                            bootbox.alert({
                                size: "small",
                                title: "Alerta",
                                message: "<label>"+data['data']+"</label>",
                                callback: function(){
                                    location.reload(1);
                                }
                            });                                   
                        }
                    });//fin ajax                         
                }
            }
            
        });
    }

    function reasigna(idDet){

        $('#myModal').modal('show');
       // console.log("id Detalle" + idDet);

        $('#colab_reasigna').on('change', function() {
            newColab = $('#colab_reasigna').val();
                bootbox.confirm({ 
                    size: "small",
                    title: 'Reasignar',
                    message: "¿Deseas continuar?",
                    buttons: {
                        confirm: {
                            label: 'Continuar',
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-secondary'
                        }
                    },
                    callback: function(result){ 
                    /* result is a boolean; true = OK, false = Cancel*/ 
                        if (result === true) {
                            //var datastring = $("#formAcliente").serializeArray(); 
                           // console.log(newColab);
                            //console.log(idDet);
                            $.ajax({
                                type: 'POST',
                                url: './_php/reasignarSeguimiento.php',
                                dataType: 'json',
                                data: {idDet:idDet,newColab:newColab},
                                 complete: function (xhr, textStatus) {
                                    //called when complete
                                    console.log(textStatus);
                                },
                                success: function(data) {
                                    //console.log(data);
                                    bootbox.alert({
                                        size: "small",
                                        title: "Alerta",
                                        message: "<label>"+data['data']+"</label>",
                                        callback: function(){
                                            location.reload(1);
                                        }
                                    });                                   
                                }
                            });//fin ajax                         
                        }
                    }
                    
                });                 

        }); 
    }

    function aprobarAsignacion(opt,idDetalle){

        bootbox.confirm({ 
            size: "small",
            title: 'Estas seguro que deseas continuar',
            message: "¿Deseas continuar?",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-secondary'
                }
            },
            callback: function(result){ 
            /* result is a boolean; true = OK, false = Cancel*/ 
                if (result === true) {
                    //var datastring = $("#formAcliente").serializeArray(); 
                    // console.log(idDetalle);
                    // console.log(opt);
                    $.ajax({
                        type: 'POST',
                        url: './_php/cerrarAsignacion.php',
                        dataType: 'json',
                        data: {idDetalle:idDetalle,opt:opt},
                         complete: function (xhr, textStatus) {
                            //called when complete
                            //console.log(textStatus);
                        },
                        success: function(data) {
                            // console.log(data)
                            bootbox.alert({
                                size: "small",
                                title: "Alerta",
                                message: "<label>"+data['data']+"</label>",
                                callback: function(){
                                    location.reload(1);
                                }
                            });                                   
                        }
                    });//fin ajax                         
                }
            }
            
        });
    }


    

    </script>
</html>
