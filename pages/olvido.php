<?php 

/*session_start();
$row = array('id_empleado'=> 13062, 'id_rol' => 1,'nombre' => 'Delfino Ruiz Salinas');
$_SESSION['user'] = $row;
*/

if (isset($_SESSION['user'])) {
    header('Location: ./index.php');
}

//session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>¿Olvidaste tu contraseña?</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
           .form-signin-heading{margin-bottom: 10px;text-align: center;}
           @media only screen and (max-device-width : 640px) {
            #loadimg{width: 340px;margin: 46% 0 0 -52%;}
            /* Styles */
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div id="loading" class="col-md-4" style="text-align: center;"> <img src="loading.gif" id="loadimg" style="opacity: 0.5; position: absolute; z-index: 1;"> </div>
            <div class="row">
                <!-- <img src="./img/ecml2.PNG" class="img-thumbnail" alt="Responsive image"> -->
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default" style="margin-top: 32%;">
  <!--                       <div class="panel-heading text-center" style="background-color: #104D73;">
                            <img src="../img/Logo-png-blanco.png" alt="..." style="width: 100%;">                            
                        </div><br> -->
                        <h2 class="form-signin-heading">¿Olvidaste tu contraseña?</h2>
                        <div class="panel-body">
                            <p>Escribe tu email de recuperación y enviaremos tu contraseña</p>
                                <fieldset>
                                    <form id="formLogin" name="formLogin" class="formLogin">
                                        <div class="form-group">

                                            <input class="form-control" id="correo" placeholder="Correo" name="correo" type="email" autofocus><br>
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <a href="#" id="enviard" class="btn btn-lg btn-success btn-block click_on_enterkey" style="background-color: #AC182D; border-color:#AC182D;">Recuperar contraseña</a>
                                    </form>
                                </fieldset>
                            
                        </div>
                        <br>

                        <div class="panel-footer">
                            <a href="login.php">Iniciar Sesión</a>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>       
        <script type="text/javascript">
            $(document).ready(function(){
                
                
                $('#loading').hide(); //initially hide the loading icon

                $(document).ajaxStart(function(){
                    $('#loading').show();
                    //console.log('shown');
                  });
                $(document).ajaxStop(function(){
                    $('#loading').hide();
                    //console.log('hidden');
                }); 

                $(document).keypress(function(event){
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if(keycode == '13'){
                       $("#enviard").click();
                    }
                });


                $("#enviard").click(function() {

                    var correo = $("#correo").val(); 
                  
                    if (correo == "") {
                        bootbox.alert({
                            size: "small",
                            title: "Alerta",
                            message: '<label class="label label-danger"> El campo correo es requerido </label>',
                            callback: function(){  }
                        });   
                    }else{
                        $.ajax({
                            type: 'GET',
                            url: './_php/mail/recuperar_pwd.php',
                            dataType: 'json',
                            data: {correo:correo},
                            complete: function (xhr, textStatus) {
                            //called when complete
                            //console.log(textStatus);
                            },
                            success: function(data) {
                                //console.log(data.data);
                                bootbox.alert({
                                    size: "small",
                                    title: "Alerta",
                                    message: "<label>"+data.data+"</label>",
                                    callback: function(){ }
                                });  
                            }    
                        });
                    }
                    //console.log(correo);
                });

               


            });//fin jquery


    </script>
    </body>
</html>
