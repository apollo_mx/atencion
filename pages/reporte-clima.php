<?php 

//$_SESSION['numero_empleado'] = '13062';

session_start(); 

if (empty($_SESSION['user'])) {
     header("location: ./login.php");
}
if ($_SESSION['user']['id_rol'] !="2") {
	header("location: ./login.php");
}

//session_destroy();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Reporte | Encuesta de Clima Organizacional</title>        

        <!-- Bootstrap Core CSS -->

        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="../css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
<!--         <link href="../css/morris.css" rel="stylesheet"> -->
   <link rel='stylesheet' href='https://pierresh.github.io/morris.js/css/morris.css' crossorigin='anonymous'>

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
     

   <!-- <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet"> -->
<!--         <link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet">         -->
    

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <style type="text/css">
            .donut-legend > span {
              display: inline-block;
              margin-right: 25px;
              margin-bottom: 10px;
              font-size: 13px;
            }
            .donut-legend > span:last-child {
              margin-right: 0;
            }
            .donut-legend > span > i {
              display: inline-block;
              width: 15px;
              height: 15px;
              margin-right: 7px;
              margin-top: -3px;
              vertical-align: middle;
              border-radius: 1px;
            }

        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header" style="background-color: #104D73;">
                    <!-- <a class="navbar-brand" href="index.php">Apollo</a> -->
                    <a href="index.php"><img src="../img/Logo-png-blanco-sm.png" alt="..." style="width: 100%;"></a>
                </div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <?php print_r($_SESSION['user']['nombre']); ?>
                                </div>
                                <!-- /input-group -->
                            </li>

                            <!-- Main navigation Menu-->
                            <?php 
                                require_once('./menu/menu.php'); 
                                showMenu('encli',$_SESSION['user']['id_rol']);
                            ?>
                            <!-- /Main navigation -->
                        </ul>
                    </div>

                <img src="./img/ecml1.gif" class="img-thumbnail" alt="Responsive image">
                </div>
            </nav>

            <div id="page-wrapper">
            <!-- <div id="loading" class="col-md-4" style="text-align: center;"> 
            	<img id="loadimg" src="loading.gif" style="opacity: 0.5; position: absolute; z-index: 1;"> 
            </div> -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header" style="color: #AC182D;" id="exa">Reporte | Encuesta de Clima Organizacional </h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-hashtag fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="tot"></div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                                
                                    <div class="panel-footer">
                                        <span class="pull-left">TOTAL DE ENCUESTAS</span>
                                        <span class="pull-right"></span>

                                        <div class="clearfix"></div>
                                    </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-hashtag fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="enc-fin"></div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                                
                                    <div class="panel-footer">
                                        <span class="pull-left">FINALIZADAS</span>
                                        <span class="pull-right"></span>

                                        <div class="clearfix"></div>
                                    </div>
                                
                            </div>
                        </div>                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-hashtag fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="enc-nofin"></div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                                
                                    <div class="panel-footer">
                                        <span class="pull-left">NO FINALIZADAS</span>
                                        <span class="pull-right"></span>

                                        <div class="clearfix"></div>
                                    </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-list-alt fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="enc-nofin"></div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="./getDetalleEnc_user.php?idCuest=1" target="_blank">
                                    <div class="panel-footer">
                                        <span class="pull-left">ENCUESTA POR USUARIO</span>
                                        <span class="pull-right"></span>

                                        <div class="clearfix"></div>
                                    </div>
                               </a> 
                            </div>
                        </div>
                    </div>
                    <div class="row">

						<div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Grafica de encuestas recibidas.
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="dt-buttons"> 
                                        <button class="dt-button buttons-pdf buttons-html5 exp" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExportLine();"><span>PDF</span></button>
                                    </div>
                                    <div id="totenc">
                                        <h2 style="text-align: center;"><small>PROGRESO DE ENCUESTAS</small></h2>
                                        <div id="total-encuestas"></div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-6 -->
						<div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Detalle de todos las encuestas.
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
<!--                                     <div id="por-empresa"></div> -->
                                   <table id="datatable_example" class="display nowrap table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>#User</th>
                                            <th>Usuario</th>
                                            <th>Correo</th>
                                            <th>Empresa</th>
                                            <th>Estatus</th>
                                            <th>Centro de Costos</th>
                                        </tr>
                                    </thead>
                                    </table>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                      
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Encuestas por empresa.
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="dt-buttons"> 
                                        <button class="dt-button buttons-pdf buttons-html5 exp" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExport('grafDona');"><span>PDF</span></button>
                                    </div>

                                    <div id="grafDona">
                                        <h2 style="text-align: center;"><small>PORCENTAJE POR EMPRESA</small></h2>
                                        <div id="por-empresa2"></div>
                                        <div id="legend" class="donut-legend"></div>
                                    </div>

                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-6 -->
 <!-- /.col-lg-6 -->
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Reporte por pregunta y respuesta.
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <div class="dt-buttons"> 
                                    <button class="dt-button buttons-pdf buttons-html5 exp" tabindex="0" aria-controls="datatable_example" type="button" onclick="pdfExport('rep-preg-cont');"><span>PDF</span></button>
                                </div>
<!--                                     <div id="g_1"></div> -->
                                    <div class="table" id="rep-preg-cont">
                                        <h2 style="text-align: center;"><small>DETALLE POR PREGUNTA Y OPCIONES</small></h2>
                                        <table class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Preguntas</th>
                                                    <th colspan="5">Resultados</th>
<!--                                                     <th>Casi siempre</th>
                                                    <th>Algunas veces</th>
                                                    <th>Nunca</th> -->
                                                </tr>
                                            </thead>
                                            <tbody id="rep-preg"></tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-6 -->                        

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
<!--         <script src="../js/raphael.min.js"></script> -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js' crossorigin='anonymous'></script>

        <script src='https://pierresh.github.io/morris.js/js/morris.js' crossorigin='anonymous'></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>


        <!-- DataTables JavaScript -->
<!--         <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script> -->

        <script src="../js/scriptdown/jquery.dataTables.min.js"></script>
        <script src="../js/scriptdown/dataTables.buttons.min.js"></script>
        <script src="../js/scriptdown/buttons.flash.min.js"></script>
        <script src="../js/scriptdown/jszip.min.js"></script>
        <script src="../js/scriptdown/pdfmake.min.js"></script>
        <script src="../js/scriptdown/vfs_fonts.js"></script>
        <script src="../js/scriptdown/buttons.html5.min.js"></script>
        <script src="../js/scriptdown/buttons.print.min.js"></script>

        <script src="../js/dist/html2pdf.bundle.min.js"></script>
                                                        
        <script type="text/javascript">
        	$(document).ready(function(){

        		// getAllquestion
        	$.ajax({
                type: 'POST',
                url: './_php/getAllquestion.php',
                dataType: 'json',
                data: {idQuiestion:1},
                complete: function (xhr, textStatus) {
                },
                success: function(data) {
                    //console.log(data.por_empresa);
                    $("#tot").text(data.totales[0]); 
                    $("#enc-fin").text(data.finalizados[0]); 
                    $("#enc-nofin").text(data.no_finalizados[0]); 
                	
                	Morris.Area({ //grafica lineal
				        element: 'total-encuestas',
				        data: data.allquest,
				        xkey: 'fecha_inicio',
				        ykeys: ['value'],
				        labels: ['#encuestas'],
				        pointSize: 4,
				        fillOpacity: 0.2,
                        xLabelAngle: '40',
    					resize: true
				    });

                    var color_array = ['#CF203D', '#104D73', '#0E2C42', '#113651', '#263746'];
                    // Morris.Donut({    //grafica de dona
                    //     element: 'por-empresa2',
                    //     data: data.por_empresa,
                    //     // donutType: 'pie',
                    //     colors: color_array,
                    //     dataLabels: true,
                    //     showPercentage: true,
                    //     // resize:true,
                    // }); 

                    var browsersChart = Morris.Donut({
                        element: 'por-empresa2',
                        // donutType: 'pie',
                        data   : data.por_empresa,
                        colors: color_array,
                        showPercentage: true,
                        dataLabels: true,
                      });


                    // data.por_empresa.forEach(function(label, i){
                    //     var legendItem = $('<span></span>').text(label['value']+" "+label['label']).prepend('<i>&nbsp;</i>');
                    //     legendItem.find('i').css('backgroundColor', '#CF203D');
                    //     $('#legend').append(legendItem)
                    // })
                    
                    browsersChart.options.data.forEach(function(label, i){
                        var legendItem = $('<span></span><br>').text(label['label'] + " ( " +label['value'] + " )").prepend('<i>&nbsp;</i>');
                        legendItem.find('i').css('backgroundColor', browsersChart.options.colors[i]);
                        $('#legend').append(legendItem)
                      })

 
                }
            });//fin ajax      
         

                $.ajax({
                    type: 'POST',
                    url: './_php/getRespuestas.php',
                    dataType: 'json',
                    data: {idCuest:1},
                     complete: function (xhr, textStatus) {
                        //called when complete
                    },
                    success: function(data) {
                        //console.log(data['data']);

                        $.each(data['data'], function( key, val ) {
                            //console.log(val.datos);
                                var opt_cuest ="";
                             $('#rep-preg').append('<tr><td>'+val.id_preg+'.- '+val.pregunta+'</td><td colspan="5">'+val.opciones+'</td></tr>');
                        }); 
                    }
                });//fin ajax                             
			     

                $.ajax({
                    type: 'POST',
                    url: './_php/getDetalleEnc.php',
                    dataType: 'json',
                    data: {idCuest:1},
                     complete: function (xhr, textStatus) {
                        //called when complete
                    },
                    success: function(data) {
                        console.log(data);
 
                    }
                });//fin ajax  
                 // todas-encusta s
                 //datatables 

                var table = $('#datatable_example').dataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'pdf'
                    ],
                    bProcessing: true,
                    "scrollY": 300,
                    "scrollX": true,
                    // scrollCollapse: true,
                    paging: false,
                    responsive: true,
                    "language": {
                        "search": "Filtro:"
                    },
                    ajax: {
                        "url":"./_php/getDetalleEnc.php?idCuest=1"
                    },
                    columns: [
                        { data: 'num_empl' },
                        { data: 'nom_user' },
                        { data: 'correo' },
                        { data: 'empresa' },
                        { data: 'stat' },
                        { data: 'nomccostos' },                                             
                      ]
                    });

                // var table = $('#usuario-encuesta').dataTable({
                //     dom: 'Bfrtip',
                //     buttons: [
                //         'excel', 'pdf'
                //     ],
                //     bProcessing: true,
                //     "scrollY": 300,
                //     "scrollX": true,
                //     // scrollCollapse: true,
                //     paging: false,
                //     responsive: true,
                //     "language": {
                //         "search": "Filtro:"
                //     },
                //     ajax: {
                //         "url":"./_php/getDetalleEnc_user.php?idCuest=1"
                //     },
                //     columns: [{ data: 'preg1' },{ data: 'preg2' },{ data: 'preg3' },{ data: 'preg4' },{ data: 'preg5' },{ data: 'preg6' },{ data: 'preg7' },{ data: 'preg8' },{ data: 'preg9' },{ data: 'preg10' },{ data: 'preg11' },{ data: 'preg12' },{ data: 'preg13' },{ data: 'preg14' },{ data: 'preg15' },{ data: 'preg16' },{ data: 'preg17' },{ data: 'preg18' },{ data: 'preg19' },{ data: 'preg20' },{ data: 'preg21' },{ data: 'preg22' },{ data: 'preg23' },{ data: 'preg24' },{ data: 'preg25' },{ data: 'preg26' },{ data: 'preg27' },{ data: 'preg28' },{ data: 'preg29' },{ data: 'preg30' },{ data: 'preg31' },{ data: 'preg32' },{ data: 'preg33' },{ data: 'preg34' },{ data: 'preg35' },{ data: 'preg36' },{ data: 'preg37' },{ data: 'preg38' },{ data: 'preg39' },{ data: 'preg40' },{ data: 'preg41' },{ data: 'preg42' },{ data: 'preg43' },{ data: 'preg44' },{ data: 'preg45' },{ data: 'preg46' },{ data: 'preg47' },{ data: 'preg48' },{ data: 'preg49' },{ data: 'preg50' },{ data: 'preg51' },{ data: 'preg52' },
                //       ]
                //     });
                

                // $('#usuario-encuesta').DataTable({
                //     dom: 'Bfrtip',
                //     buttons: [
                //         'excel', 'pdf'
                //     ],
                //     "language": {
                //         "search": "Filtro:",
                //         "zeroRecords": " "
                //     },
                //    //  bProcessing: true,
                //     "scrollY": 300,
                //     "scrollX": true,
                //     paging: false,
                //     responsive: true,
                // });

                //  $.ajax({
                //     type: 'GET',
                //     url: './_php/getDetalleEnc_user.php?idCuest=1',
                //     dataType: 'json',
                //     // data: {idCuest:1},
                //     complete: function (xhr, textStatus) {
                //         //called when complete
                //     },
                //     success: function(data) {
                //         console.log(data);
                //         //$('#registrosU').()
                //         // console.log(data.data[0]['nom_user']);
                //         // console.log(data.aaData);
                        

                //         //$('#registrosU').append('<tr><td>'+data.data[0]['nom_user']+'</td>');
                //         var opt_cuest ="";

                //         $.each(data.aaData, function( key, val ) {
                //             //console.log(val);
                //             opt_cuest +='<td>'+val+'</td>';
                          
                //         });
                //         $('#registrosU').append('<tr><td>'+data.data[0]['nom_user']+'</td>'+opt_cuest+'</tr>');
                //     }
                // });//fin ajax  


        	});//fin jquery

    function pdfExport(nameId){
        //console.log(nameId);
        var element = document.getElementById(nameId);
        var opt = {
          margin:       1,
          filename:     'reporte-encuesta',
          image:        { type: 'jpeg', quality: 0.98 },
          html2canvas:  { scale: 1 },
          jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
        };
        html2pdf(element, opt);
    }

    function pdfExportLine(){
        //console.log(nameId);
        var element = document.getElementById('totenc');
        var opt = {
          margin:       0.1,
          filename:     'reporte-encuesta',
          image:        { type: 'jpeg', quality: 0.98 },
          html2canvas:  { scale: 1 },
          jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
        };
        html2pdf(element, opt);
    }
    


    // setTimeout(function(){ 
    //     $(".exp").show();
       
    // }, 5000);

        </script>

    </body>
</html>