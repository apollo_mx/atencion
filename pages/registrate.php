<?php 

/*session_start();
$row = array('id_empleado'=> 13062, 'id_rol' => 1,'nombre' => 'Delfino Ruiz Salinas');
$_SESSION['user'] = $row;
*/

if (isset($_SESSION['user'])) {
    header('Location: ./index.php');
}

//session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema de encuestas - Iniciar Sesión</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
           .form-signin-heading{margin-bottom: 10px;text-align: center;}
        </style>
    </head>
    <body style="background-image: url('./img/ecml3.png');background-size: cover; background-repeat: no-repeat; height: 100%;">

        <div class="container">
            <div class="row">
                <!-- <img src="./img/ecml2.PNG" class="img-thumbnail" alt="Responsive image"> -->
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default" style="margin-top: 5%;">
  <!--                       <div class="panel-heading text-center" style="background-color: #104D73;">
                            <img src="../img/Logo-png-blanco.png" alt="..." style="width: 100%;">                            
                        </div><br> -->
                        <h2 class="form-signin-heading">Crea una cuenta</h2>
                        <div class="panel-body">
                            <form id="formCcuenta">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" id="nombre_empleado" placeholder="* Nombre completo" name="nombre_empleado" type="text" autofocus><br>
                                        <input class="form-control" id="idEmpleado" placeholder="* Número de empleado" name="numero_empleado" type="text" autofocus><br>
                                        <input class="form-control" id="idPasswd" placeholder="* Contraseña" name="pwd" type="text" autofocus><br>
                                        <input class="form-control" id="correo" placeholder="* Correo para recuperar contraseña" name="correo" type="text" autofocus><br>
                                        
                                        <div class="form-group">
                                        <label for="exampleFormControlSelect1">Selecciona la Empresa a la que perteneces:</label>
                                            <select class="form-control" id="empresa" name="empresa">
                                              <option value="0">-- Seleccionar --</option>
                                              <option value="1">Especialización Profesional de Personal</option>
                                              <option value="2">Producción,&nbsp; Mantenimiento y Servicios&nbsp; Aéreos&nbsp;</option>
                                              <option value="3">Clear Strategy</option>
                                              <option value="4">Investigación, Desarrollo y Control de Calidad</option>
                                              <option value="5">Labormex Servicios Químicos S.A. de C.V.</option>                                              
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <a href="#" id="crearCuenta" class="btn btn-lg btn-success btn-block" style="background-color: #AC182D; border-color:#AC182D;">Crea tu cuenta</a>
                                </fieldset>
                            </form>
                        </div>
                        <br>

                        <div class="panel-footer">
                            ¿Ya estás registrado ? <a href="login.php">Ingresa</a>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
        <script src="../js/bootbox.min.js"></script>                
        <script type="text/javascript">
            $(document).ready(function(){
                
                $(document).keypress(function(event){
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if(keycode == '13'){
                       $("#crearCuenta").click();
                    }
                });

                $("#crearCuenta").click(function() {


                    var datastring = $("#formCcuenta").serializeArray();
                    // 0 nombre_empleado // 1 numero_empleado // 2 pwd // 3 "correo" // 4 "empresa"
                    if (datastring[0]['value'] == "" || datastring[1]['value'] == "" || datastring[2]['value'] == "" || datastring[3]['value'] == "" || datastring[4]['value'] == 0) {
                            bootbox.alert({
                                size: "small",
                                title: "Alerta",
                                message: '<label class="label label-danger"> Todos los campos son requeridos</label>',
                                callback: function(){  }
                            });                                    
                        return false;
                    }else{
                        $.ajax({
                            type: 'POST',
                            url: './_php/crearnewUser.php',
                            dataType: 'json',
                            data: {datastring:datastring},
                             complete: function (xhr, textStatus) {
                                //called when complete
                            },
                            success: function(data) {
                                //console.log(data);
                                bootbox.alert({
                                    size: "small",
                                    title: "Alerta",
                                    message: "<label>"+data['data']+"</label>",
                                    callback: function(){  }
                                });                                    
                            }
                        });                           
                    }
                });

               


            });//fin jquery


    </script>
    </body>
</html>
